package model.database.dao.interfaces;

import model.database.dao.DAO;
import model.entity.Question;

import java.util.List;

/**
 * Provides additional methods for accessing Question objects in database
 */
public interface QuestionDAO extends DAO<Question> {

    /**
     * get list of question of specified user
     * @param userId id of user which questions needed
     * @return list of questions where userId equals supplied userId
     */
    List<Question> getUserQuestions(Integer userId);

    /**
     * get all question that were not moderated
     * @return list of question where approved equals {@code null}
     */
    List<Question> getQuestionsNullApproved();

    /**
     * retrieve questions from db where userId not equals to supplied
     *
     * @param isApproved value of approved
     * @param lang value og lang
     *@param amount amount of questions to retrieve
     * @param userId id of user which questions do not needed to be retrieved   @return list of questions which size equals or less then amount
     */
    List<Question> getRandomQuestionsWith(boolean isApproved, String lang, int amount, Integer userId);
}
