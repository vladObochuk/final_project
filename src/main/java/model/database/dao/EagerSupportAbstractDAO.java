package model.database.dao;

import model.database.DomainObject;
import model.database.exception.RepositoryException;
import org.apache.log4j.Logger;

import java.sql.*;

public abstract class EagerSupportAbstractDAO<T extends DomainObject> extends AbstractDAO<T> {
    private static Logger logger = Logger.getLogger(EagerSupportAbstractDAO.class);

    public EagerSupportAbstractDAO(Connection connection) {
        super(connection);
    }

    /**
     * Insert this object and all subobjects
     *
     * @param elem not {@code null} entity
     * @return 1
     * @throws RepositoryException is {@code SQLException} occurred
     */
    @Override
    public int insert(T elem) throws RepositoryException {
        super.insert(elem);
        try (PreparedStatement statement = connection.prepareStatement(getInsertSubEntitiesQuery(), Statement.RETURN_GENERATED_KEYS)) {

            prepareBatchForSubEntitiesInsert(statement, elem);
            int[] rowsAffected = statement.executeBatch();

            ResultSet generatedKeys = statement.getGeneratedKeys();
            parseGeneratedKeys(generatedKeys, rowsAffected, elem);
            return 1;
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while insert entity into '" + getTableName() + "` table";
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    /**
     * get SQL query to insert objects of {@code T} type
     *
     * @return not {@code null} String object
     */
    protected abstract String getInsertSubEntitiesQuery();

    /**
     * Set to prepared statement all objects, according to return value of
     * {@link EagerSupportAbstractDAO#getInsertSubEntitiesQuery()}
     *
     * @param statement not {@code null} statement created with return value of
     *                  {@link EagerSupportAbstractDAO#getInsertSubEntitiesQuery()}
     * @param entity    not {@code null}
     * @throws SQLException
     */
    protected abstract void prepareBatchForSubEntitiesInsert(PreparedStatement statement, T entity) throws SQLException;

    /**
     * Set id's for subobjects
     *
     * @param generatedKeys generated id's for subobjects
     * @param rowsAffected  amount of affected rows foreach subobject
     * @param entity        not {@code null}
     * @throws SQLException
     */
    protected abstract void parseGeneratedKeys(ResultSet generatedKeys, int[] rowsAffected, T entity) throws SQLException;

}
