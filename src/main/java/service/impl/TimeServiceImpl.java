package service.impl;

import model.entity.Config;
import org.apache.log4j.Logger;
import service.TimeService;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Date;

public class TimeServiceImpl implements TimeService {
    private static Logger logger = Logger.getLogger(TimeServiceImpl.class);

    public void setupTimeForAnswer(HttpSession session, Config config) {
        Date startTime = (Date) session.getAttribute("startTime");
        Date endTime = (Date) session.getAttribute("endTime");
        if (startTime == null) {
            logger.trace("Setting up time to answer");
            startTime = new Date();
            session.setAttribute("startTime", startTime);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startTime);
            calendar.add(Calendar.SECOND, config.getTimeToReply());
            endTime = calendar.getTime();
            session.setAttribute("endTime", endTime);
        }

        Date now = new Date();
        long secondsLeft = secondsBetween(now, endTime);
        session.setAttribute("timeLeft", secondsLeft);
    }

    @Override
    public void addTime(HttpSession session) {
        logger.trace("Adding time to answer");
        final int ADDITIONAL_TIME = 30;
        Date endTime = (Date) session.getAttribute("endTime");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(endTime);
        calendar.add(Calendar.SECOND, ADDITIONAL_TIME);
        endTime = calendar.getTime();
        session.setAttribute("endTime", endTime);

        Date now = new Date();
        long secondsLeft = secondsBetween(now, endTime);
        session.setAttribute("timeLeft", secondsLeft);
    }

    private long secondsBetween(Date start, Date finish) {
        return ((finish.getTime() - start.getTime()) / 1_000);
    }

    public void removeSessionAttributes(HttpSession session) {
        logger.debug("Removing game session attributes");
        session.removeAttribute("startTime");
        session.removeAttribute("endTime");
        session.removeAttribute("timeLeft");
    }

    @Override
    public boolean isTimeToAnswerPassed(HttpSession session) {
        Date finishTime = (Date) session.getAttribute("endTime");
        Date now = new Date();
        return now.after(finishTime);
    }
}
