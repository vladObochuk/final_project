package service.impl;

import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.AnswerOptionDAO;
import model.database.dao.interfaces.QuestionDAO;
import model.entity.AnswerOption;
import model.entity.Question;
import model.entity.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import service.QuestionService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestQuestionServiceImpl {
    @Mock
    DAOFactory daoFactory;

    @Mock
    QuestionDAO questionDAO;

    @Mock
    AnswerOptionDAO answerOptionDAO;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private QuestionService questionService;

    @Before
    public void setUp() throws Exception {
        questionService = new QuestionServiceImpl(daoFactory);
        when(daoFactory.createQuestionDAO()).thenReturn(questionDAO);
        when(daoFactory.createAnswerOptionDAO()).thenReturn(answerOptionDAO);
    }

    @Test
    public void testAddQuestion(){

        Question question = Question.newBuilder().id(5).build();
        AnswerOption correctAnswer = new AnswerOption();
        AnswerOption single = new AnswerOption("text");
        List<AnswerOption> answerOptions = Arrays.asList(single, single, single);

        questionService.addQuestion(question, correctAnswer, answerOptions);

        assertEquals(question.getId(), correctAnswer.getQuestionId());
        for (AnswerOption answerOption : answerOptions) {
            assertEquals(question.getId(), answerOption.getQuestionId());
        }

        verify(questionDAO, times(1)).insert(question);
        verify(answerOptionDAO, times(4)).insert(anyObject());

    }

    @Test
    public void testConfirmQuestion(){
        Question question = Question.newBuilder().id(8).build();
        AnswerOption correctAnswer = new AnswerOption();
        AnswerOption single = new AnswerOption("text");
        List<AnswerOption> answerOptions = Arrays.asList(single, single, single);

        questionService.confirmQuestion(question, correctAnswer, answerOptions);

        assertEquals(question.getId(), correctAnswer.getQuestionId());
        for (AnswerOption answerOption : answerOptions) {
            assertEquals(question.getId(), answerOption.getQuestionId());
        }

        verify(questionDAO, times(1)).delete(anyInt());
        verify(questionDAO, times(1)).insert(question);
        verify(answerOptionDAO, times(4)).insert(anyObject());
    }

    @Test
    public void testGetQuestionsOfUser(){
        List<Question> questionsExpected = Arrays.asList(new Question(), new Question());
        when(questionDAO.getUserQuestions(anyInt())).thenReturn(questionsExpected);

        int userId = 2;
        List<Question> result = questionService.getQuestionsOfUser(userId);

        assertEquals(questionsExpected, result);
        verify(questionDAO, times(1)).getUserQuestions(userId);
    }

    @Test
    public void  testGetNotModeratedQuestions(){
        List<Question> questionsExpected = Arrays.asList(new Question(), new Question());
        when(questionDAO.getQuestionsNullApproved()).thenReturn(questionsExpected);

        List<Question> result = questionService.getNotModeratedQuestions();

        assertEquals(questionsExpected, result);
        verify(questionDAO, times(1)).getQuestionsNullApproved();
    }
    @Test
    public void testGetQuestionById(){
        Question questionExpected = new Question();
        when(questionDAO.getById(anyInt())).thenReturn(Optional.of(questionExpected));

        int questionId = 5;
        Question result = questionService.getQuestionById(questionId);
        assertEquals(questionExpected, result);

        verify(questionDAO, times(1)).getById(questionId);
    }

    @Test
    public void testGetRandomQuestions(){
        List<Question> questionsExpected = Arrays.asList(new Question(), new Question());
        when(questionDAO.getRandomQuestionsWith(anyBoolean(), anyString(), anyInt(), anyInt()))
                .thenReturn(questionsExpected);

        String lang = "en";
        int amount = 5;
        User user = User.newBuilder().id(88).build();
        List<Question> result = questionService.getRandomQuestions(lang, amount, user);

        assertEquals(questionsExpected, result);

        verify(questionDAO, times(1))
                .getRandomQuestionsWith(true,lang , amount, user.getId());
    }

    @Test
    public void testGetQuestionAnswers(){
        List<AnswerOption> expectedOptions = Arrays.asList(new AnswerOption(), new AnswerOption(), new AnswerOption());

        when(answerOptionDAO.getQuestionAnswers(anyInt())).thenReturn(expectedOptions);

        int questionId = 879;
        List<AnswerOption> result = questionService.getQuestionAnswers(questionId);
        assertEquals(expectedOptions, result);

        verify(answerOptionDAO, times(1)).getQuestionAnswers(questionId);
    }

    @Test
    public void testGetQuestionAnswer(){
        AnswerOption expected = new AnswerOption();
        when(answerOptionDAO.getById(anyInt())).thenReturn(Optional.of(expected));

        Question question = Question.newBuilder().rightAnswerId(8).build();
        AnswerOption result = questionService.getQuestionAnswer(question);
        assertEquals(expected, result);

        verify(answerOptionDAO, times(1)).getById(question.getRightAnswerId());
    }

    @Test
    public void testGetCorrectnessProbability(){
        List<AnswerOption> answerOptions = Arrays.asList(new AnswerOption(1), new AnswerOption(2), new AnswerOption(3));
        Question question = Question.newBuilder().id(1).rightAnswerId(3).build();

        Map<AnswerOption, Double> result = questionService.getCorrectnessProbability(answerOptions, question);


        double maxMean = 100 / answerOptions.size();
        double minMean = 100 / (answerOptions.size() * 2);
        double mean = result.values().stream().mapToDouble(value -> value).average().getAsDouble();
        assertTrue(mean < maxMean);
        assertTrue(mean > minMean);
    }
}