package model.database.dao.concrete.mysql;


import model.entity.Answer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLAnswerDAOTest {
    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    private Answer source = Answer.newBuilder()
            .id(15856)
            .chosenAnswerId(5)
            .gameId(8)
            .questionId(155)
            .build();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MySQLAnswerDAO mySQLAnswerDAO;

    @Before
    public void setUp() throws SQLException {
        mySQLAnswerDAO = new MySQLAnswerDAO(connection);
    }

    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLAnswerDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLAnswerDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLAnswerDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLAnswerDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLAnswerDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLAnswerDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLAnswerDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLAnswerDAO.getInsertQuery().isEmpty());
    }

    @Test
    public void testMapEntities() throws SQLException{
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("id")).thenReturn(source.getId());
        when(resultSet.getInt("game_id")).thenReturn(source.getGameID());
        when(resultSet.getInt("question_id")).thenReturn(source.getQuestionId());
        when(resultSet.getInt("chosen_answer_id")).thenReturn(source.getChosenAnswerId());

        List<Answer> answers = mySQLAnswerDAO.mapEntities(resultSet);

        assertNotNull(answers);
        assertEquals(2, answers.size());
        for (Answer answer : answers) {
            assertEquals(source.getId(), answer.getId());
            assertEquals(source.getChosenAnswerId(), answer.getChosenAnswerId());
            assertEquals(source.getGameID(), answer.getGameID());
            assertEquals(source.getQuestionId(), answer.getQuestionId());
        }

        verify(resultSet, times(3)).next();
        verify(resultSet, times(8)).getInt(anyString());
    }

    @Test
    public void testPrepareStatementForMerge() throws SQLException{

        mySQLAnswerDAO.prepareStatementForMerge(preparedStatement, source);

        verify(preparedStatement, atLeast(1)).setInt(anyInt(), eq(source.getId()));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getChosenAnswerId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getGameID()), eq(Types.INTEGER));
        verify(preparedStatement,atLeast(2))
                .setObject(anyInt(), eq(source.getQuestionId()), eq(Types.INTEGER));
    }

    @Test
    public void testPrepareStatementForInsert() throws SQLException{
        mySQLAnswerDAO.prepareStatementForInsert(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getChosenAnswerId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getGameID()), eq(Types.INTEGER));
        verify(preparedStatement,atLeast(1))
                .setObject(anyInt(), eq(source.getQuestionId()), eq(Types.INTEGER));
    }

}
