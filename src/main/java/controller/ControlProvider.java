package controller;

import controller.command.Command;
import controller.command.manager.CommandManager;
import controller.utils.CSRFSecurity;
import controller.utils.HttpMethod;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class that process user request consuming list of applicable commands and using it to write response
 */
public class ControlProvider {
    private static Logger logger = Logger.getLogger(ControlProvider.class);

    /**
     * process user request with one of Command in manager
     *
     * @param manager  not {@code null} object containing list of applicable commands
     * @param request  not {@code null}
     * @param response not {@code null}
     * @param method   not {@code null}, equals to the method in request
     * @throws ServletException when some of response or request objects throws it
     * @throws IOException
     */
    public static void process(CommandManager manager, HttpServletRequest request, HttpServletResponse response, HttpMethod method)
            throws ServletException, IOException {

        if (CSRFSecurity.isSafe(request, method)) {
            String action = getAction(request);

            Command command = manager.get(action);
            String page = command.process(request, response, method);

            returnPage(page, request, response, method);
        }
    }


    /**
     * Retrieve action parameter
     *
     * @param request not {@code null}
     * @return String that equals parameter value in request, nullable
     */
    private static String getAction(HttpServletRequest request) {
        return request.getParameter("action");
    }


    /**
     * Process writing response
     *
     * @param page     not {@code null}
     * @param request  not {@code null}
     * @param response not {@code null}
     * @param method   equals to the method in request
     * @throws ServletException
     * @throws IOException
     */
    private static void returnPage(String page, HttpServletRequest request, HttpServletResponse response, HttpMethod method)
            throws ServletException, IOException {
        if (method == HttpMethod.GET) {
            forwardToPage(page, request, response);
        } else {
            redirectToPage(page, response);
        }
    }

    /**
     * Process redirect to specified page
     *
     * @param page     not {@code null} path to witch redirect
     * @param response not {@code null}
     * @throws ServletException
     * @throws IOException
     */
    private static void redirectToPage(String page, HttpServletResponse response) throws ServletException, IOException {
        logger.trace("Redirecting to " + page);
        response.sendRedirect(response.encodeRedirectURL(page));
    }

    /**
     * Process forwarding to specified page
     *
     * @param page     not {@code null} path to jsp page
     * @param request  not {@code null}
     * @param response not {@code null}
     * @throws ServletException
     * @throws IOException
     */
    private static void forwardToPage(String page, HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.trace("Forwarding to " + page);
        RequestDispatcher dispatcher = request.getRequestDispatcher(page);
        dispatcher.forward(request, response);
    }
}
