package model.database.dao.concrete.mysql;

import model.database.dao.AbstractDAO;
import model.database.dao.interfaces.GameDAO;
import model.entity.Game;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLGameDAO extends AbstractDAO<Game> implements GameDAO {
    private final static String TABLE_NAME = "games";

    public MySQLGameDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT * FROM " + TABLE_NAME;
        String INSERT = "INSERT INTO " + TABLE_NAME + " (user_id, configuration_id) VALUES (?, ?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, user_id, configuration_id) VALUES (?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE user_id = ?, configuration_id = ?";
    }

    @Override
    protected List<Game> mapEntities(ResultSet resultSet) throws SQLException {
        List<Game> games = new ArrayList<>();
        while (resultSet.next()) {
            Game game = new Game();
            game.setId(resultSet.getInt("id"));
            game.setUserId(resultSet.getInt("user_id"));
            game.setConfigurationId(resultSet.getInt("configuration_id"));
            games.add(game);
        }
        return games;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }

    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, Game elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setObject(2, elem.getUserId(), Types.INTEGER);
        statement.setObject(3, elem.getConfigurationId(), Types.INTEGER);

        statement.setObject(4, elem.getUserId(), Types.INTEGER);
        statement.setObject(5, elem.getConfigurationId(), Types.INTEGER);
    }

    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Game elem) throws SQLException {
        statement.setObject(1, elem.getUserId(), Types.INTEGER);
        statement.setObject(2, elem.getConfigurationId(), Types.INTEGER);
    }
}
