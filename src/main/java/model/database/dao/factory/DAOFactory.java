package model.database.dao.factory;

import model.database.dao.ErrorCodes;
import model.database.dao.interfaces.*;

import java.sql.Connection;

/**
 * Interface that describe creation of DAOs
 */
public interface DAOFactory {
    /**
     * get connection to database
     * @return not {@code null} connection to database
     */
    Connection getConnection();

    AnswerDAO createAnswerDAO();

    AnswerOptionDAO createAnswerOptionDAO();

    ConfigDAO createConfigDAO();

    GameDAO createGameDAO();

    HintTypeDAO createHintTypeDAO();

    QuestionDAO createQuestionDAO();

   UserDAO createUserDAO();

    ErrorCodes getErrorCodes();

}
