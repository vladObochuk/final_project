package controller.command.impl.admin;

import constants.Alert;
import constants.AlertMessages;
import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import exception.NullRequestParameterException;
import model.entity.User;
import org.apache.catalina.CredentialHandler;
import org.apache.catalina.Globals;
import service.UserService;
import service.factory.ServiceFactory;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ModifyUserCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            Integer userId = CommandUtils.retrieveIntegerValue(request, "id");
            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            UserService userService = serviceFactory.createUserService();
            User user = userService.getUser(userId);
            request.setAttribute("roles", User.Role.values());
            request.setAttribute("user", user);
        } catch (NullRequestParameterException | NumberFormatException e){
            request.getSession().setAttribute(Alert.ERROR, AlertMessages.CANT_FIND_USER);
        }

        return PagePath.EDIT_USER;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        try {
            User modifications = retrieveUser(request);

            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            UserService userService = serviceFactory.createUserService();
            ServletContext context = request.getServletContext();
            CredentialHandler credentialHandler = (CredentialHandler) context.getAttribute(Globals.CREDENTIAL_HANDLER);
            userService.modifyUser(modifications, credentialHandler);

            session.setAttribute(Alert.SUCCESS, AlertMessages.USER_EDIT_SUCCESS);
        } catch (NullRequestParameterException | NumberFormatException e){
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_GET_PARAMETER);
        }
        return request.getContextPath() + RedirectLink.ADMIN_LIST_MODERATORS;
    }

    private User retrieveUser(HttpServletRequest request) throws NumberFormatException{
        Integer userId = CommandUtils.retrieveIntegerValue(request, "userId");
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        String role = request.getParameter("role");

        return User.newBuilder()
                .id(userId)
                .username(userName)
                .password(password)
                .role(role)
                .build();
    }


}
