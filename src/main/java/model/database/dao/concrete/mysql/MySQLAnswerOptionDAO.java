package model.database.dao.concrete.mysql;

import model.database.dao.AbstractDAO;
import model.database.dao.interfaces.AnswerOptionDAO;
import model.database.exception.RepositoryException;
import model.entity.AnswerOption;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLAnswerOptionDAO extends AbstractDAO<AnswerOption> implements AnswerOptionDAO {
    private static Logger logger = Logger.getLogger(MySQLAnswerOptionDAO.class);
    private final static String TABLE_NAME = "answer_options";

    public MySQLAnswerOptionDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT * FROM " + TABLE_NAME;
        String INSERT = "INSERT INTO " + TABLE_NAME + " (text, question_id) VALUES (?, ?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, text, question_id) VALUES (?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE text = ?, question_id = ?";
        String SELECT_QUESTION_OPTIONS = SELECT + " WHERE question_id=?";
    }

    @Override
    protected List<AnswerOption> mapEntities(ResultSet resultSet) throws SQLException {
        List<AnswerOption> answerOptions = new ArrayList<>();
        while (resultSet.next()) {
            AnswerOption answerOption = new AnswerOption();
            answerOption.setId(resultSet.getInt("id"));
            answerOption.setText(resultSet.getString("text"));
            answerOption.setQuestionId(resultSet.getInt("question_id"));
            answerOptions.add(answerOption);
        }
        return answerOptions;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }


    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }


    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, AnswerOption elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setString(2, elem.getText());
        statement.setObject(3, elem.getQuestionId(), Types.INTEGER);

        statement.setString(4, elem.getText());
        statement.setObject(5, elem.getQuestionId(), Types.INTEGER);
    }


    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }


    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, AnswerOption elem) throws SQLException {
        statement.setString(1, elem.getText());
        statement.setObject(2, elem.getQuestionId(), Types.INTEGER);
    }


    @Override
    public List<AnswerOption> getQuestionAnswers(Integer questionId) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.SELECT_QUESTION_OPTIONS)) {
            statement.setObject(1, questionId);
            ResultSet resultSet = statement.executeQuery();
            return mapEntities(resultSet);
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while retrieving answerOptions of question with id=" + questionId;
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

}
