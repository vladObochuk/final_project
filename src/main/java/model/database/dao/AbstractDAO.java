package model.database.dao;


import model.database.DomainObject;
import model.database.exception.RepositoryException;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.List;
import java.util.Optional;

/**
 * Realizes all operations of {@link DAO} interface. Require realization of methods: get sql queries of CRUD operations,
 * mapping objects from {@link ResultSet} and preparing statement for insert and merge operations
 *
 * @param <T>
 */
public abstract class AbstractDAO<T extends DomainObject> implements DAO<T> {
    private static Logger logger = Logger.getLogger(AbstractDAO.class);
    protected Connection connection;

    public AbstractDAO(Connection connection) {
        this.connection = connection;
    }

    public List<T> getAll() throws RepositoryException {
        List<T> result;
        try (Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(getSelectQuery());
            result = mapEntities(resultSet);
            return result;
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while retrieving all entities from `" + getTableName() + "` table";
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    public Optional<T> getById(Integer id) throws RepositoryException {
        List<T> elemList;
        final String SELECT_BY_ID_QUERY = getSelectQuery() + " WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(SELECT_BY_ID_QUERY)) {

            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            elemList = mapEntities(resultSet);

            return elemList.stream().findFirst();
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while retrieving entity from `" + getTableName() +
                    "` table with id=" + id;
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    public boolean delete(Integer id) throws RepositoryException {
        final String DELETE_QUERY = "DELETE FROM " + getTableName() + " WHERE id = ?";
        try (PreparedStatement statement = connection.prepareStatement(DELETE_QUERY)) {

            statement.setInt(1, id);
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Can't delete entity from table `" + getTableName() + "` with id=" + id;
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }


    public int merge(T elem) throws RepositoryException {
        if (elem.getId() == null)
            return insert(elem);

        final String MERGE_QUERY = getMergeQuery();
        try (PreparedStatement statement = connection.prepareStatement(MERGE_QUERY)) {

            prepareStatementForMerge(statement, elem);
            return statement.executeUpdate();
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while merge entity from `" + getTableName() +
                    "` table, with id=" + elem.getId();
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }


    public int insert(T elem) throws RepositoryException {
        final String INSERT_QUERY = getInsertQuery();
        try (PreparedStatement statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS)) {

            prepareStatementForInsert(statement, elem);
            int rowsAffected = statement.executeUpdate();

            ResultSet generatedKey = statement.getGeneratedKeys();
            if (generatedKey.next())
                elem.setId(generatedKey.getInt(1));
            return rowsAffected;
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while insert entity into '" + getTableName() + "` table";
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    /**
     * maps object fields from resultSet to object
     *
     * @param resultSet result of execution select query
     * @return list of maped object, not null
     */
    protected abstract List<T> mapEntities(ResultSet resultSet) throws SQLException;

    /**
     * @return not null name of table that represents entities of type {@code T}
     */
    protected abstract String getTableName();

    protected abstract String getSelectQuery();

    /**
     * @return not null query to merge entity of {@code T} type
     */
    protected abstract String getMergeQuery();

    /**
     * set all params for merge query {@link AbstractDAO#getMergeQuery()}
     *
     * @param statement result of preparing statement with {@link AbstractDAO#getMergeQuery()} query
     */
    protected abstract void prepareStatementForMerge(PreparedStatement statement, T elem) throws SQLException;

    /**
     * @return query to insert entity of type {@code T}
     */
    protected abstract String getInsertQuery();

    /**
     * set all params for insert query {@link AbstractDAO#getInsertQuery()}
     *
     * @param statement result of preparing statement with {@link AbstractDAO#getInsertQuery()}
     */
    protected abstract void prepareStatementForInsert(PreparedStatement statement, T elem) throws SQLException;
}