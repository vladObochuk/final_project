package controller.command.manager.factory;

import controller.command.manager.CommandManager;
import controller.command.manager.impl.admin.AdminCommandManager;
import controller.command.manager.impl.moderator.ModeratorCommandManager;
import controller.command.manager.impl.unauthorized.UnauthorizedCommandManager;
import controller.command.manager.impl.user.UserCommandManager;
import exception.ApplicationException;
import org.apache.log4j.Logger;

public class CommandManagerFactory {
    private static Logger logger = Logger.getLogger(CommandManagerFactory.class);

    public static CommandManager getCommandManager(CommandManagerType type) {
        switch (type) {
            case USER:
                return UserCommandManager.getInstance();
            case ADMIN:
                return AdminCommandManager.getInstance();
            case MODERATOR:
                return ModeratorCommandManager.getInstance();
            case UNAUTHORIZED:
                return UnauthorizedCommandManager.getInstance();
            default:
                logger.error("");
                throw new ApplicationException("");
        }
    }
}
