<%@ tag body-content="empty" %>
<%@ attribute name="items" required="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
    <c:when test="${empty param.page}">
        <fmt:formatNumber var="page" value="1" scope="page" type="number"/>
    </c:when>

    <c:otherwise>
        <fmt:formatNumber var="page" value="${param.page}" scope="page" type="number"/>
    </c:otherwise>
</c:choose>

<c:set var="pageURL" value="${requestScope['javax.servlet.forward.request_uri']}?action=${param.action}"/>

<ul class="pagination justify-content-center">
    <c:if test="${page eq 1}">
        <c:set var="previousDisabled" value="disabled"/>
    </c:if>
    <li class="page-item ${previousDisabled}">
        <a class="page-link" href="${pageURL}&page=${page - 1}" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
        </a>
    </li>

    <c:forEach items="${items}" varStatus="loop" step="${pageSize}">
        <c:choose>
            <c:when test="${page eq loop.count}">
                <c:set var="pageActive" value="active"/>
            </c:when>

            <c:otherwise>
                <c:remove var="pageActive"/>
            </c:otherwise>
        </c:choose>
        <li class="page-item ${pageActive}">
            <a class="page-link" href="${pageURL}&page=${loop.count}">${loop.count}</a>
        </li>
        <c:set var="pageAmount" value="${loop.count}"/>
    </c:forEach>

    <c:if test="${page eq pageAmount}">
        <c:set var="nextDisabled" value="disabled"/>
    </c:if>
    <li class="page-item ${nextDisabled}">
        <a class="page-link" href="${pageURL}&page=${page + 1}" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
        </a>
    </li>
</ul>