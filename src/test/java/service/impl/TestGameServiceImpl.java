package service.impl;

import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.AnswerDAO;
import model.database.dao.interfaces.ConfigDAO;
import model.database.dao.interfaces.GameDAO;
import model.database.dao.interfaces.HintTypeDAO;
import model.entity.*;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import service.GameService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestGameServiceImpl {
    @Mock
    DAOFactory daoFactory;

    @Mock
    HintTypeDAO hintTypeDAO;

    @Mock
    ConfigDAO configDAO;

    @Mock
    AnswerDAO answerDAO;

    @Mock
    GameDAO gameDAO;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private GameService gameService;

    @Before
    public void setUp() throws Exception {
        gameService = new GameServiceImpl(daoFactory);
    }

    @Test
    public void testGetAllAvailableHints(){
        List<HintType> dummy = Arrays.asList(new HintType(1), new HintType(2));
        when(daoFactory.createHintTypeDAO()).thenReturn(hintTypeDAO);
        when(hintTypeDAO.getAll()).thenReturn(dummy);

        List<HintType> result = gameService.getAllAvailableHints();
        assertEquals(dummy, result);

        verify(daoFactory, times(1)).createHintTypeDAO();
        verify(hintTypeDAO, times(1)).getAll();
    }

    @Test
    public void testGetLastConfiguration(){
        User user = User.newBuilder().id(100).build();
        Config dummy = Config.newBuilder().userId(user.getId()).build();
        when(daoFactory.createConfigDAO()).thenReturn(configDAO);
        when(configDAO.getLastUserConfiguration(anyInt())).thenReturn(Optional.of(dummy));

        Config config = gameService.getLastConfigurationOrDefault(user);

        assertEquals(dummy, config);
        verify(daoFactory, times(1)).createConfigDAO();
        verify(configDAO, times(1)).getLastUserConfiguration(anyInt());
    }

    @Test
    public void testGetDefaultConfiguration(){
        User user = User.newBuilder().id(100).build();
        when(daoFactory.createConfigDAO()).thenReturn(configDAO);
        when(configDAO.getLastUserConfiguration(anyInt())).thenReturn(Optional.empty());

        Config config = gameService.getLastConfigurationOrDefault(user);

        assertEquals(GameService.DEFAULT_CONFIG, config);
        verify(daoFactory, times(1)).createConfigDAO();
        verify(configDAO, times(1)).getLastUserConfiguration(anyInt());
    }

    @Test
    public void testAddConfiguration(){
        Config config = new Config();
        when(daoFactory.createConfigDAO()).thenReturn(configDAO);

        gameService.addConfiguration(config);

        verify(daoFactory, times(1)).createConfigDAO();
        verify(configDAO, times(1)).insert(config);
    }

    @Test
    public void testAddAnswer(){
        Answer answer = new Answer();
        when(daoFactory.createAnswerDAO()).thenReturn(answerDAO);

        gameService.addAnswer(answer);

        verify(daoFactory, times(1)).createAnswerDAO();
        verify(answerDAO, times(1)).insert(answer);
    }

    @Test
    public void testAddGame(){
        Game game = new Game();
        when(daoFactory.createGameDAO()).thenReturn(gameDAO);

        gameService.addGame(game);

        verify(daoFactory, times(1)).createGameDAO();
        verify(gameDAO, times(1)).insert(game);
    }
}