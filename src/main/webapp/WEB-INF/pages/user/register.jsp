<%@ include file="../jspf/header.jspf" %>

<c:if test="${not empty param.error}">
    <div class="alert alert-danger text-center">
        <strong>
            <fmtdt:message key="${param.error}" defaultVal="Error occured"/>
        </strong>
    </div>
</c:if>

<csrf:form action="" method="post" htmlClass="form-small">
    <div class="form-group">
        <label for="user">
            <fmt:message key="username.label"/>
        </label>
        <input id="user" type="text" name="username" size="30" aria-describedby="username-help" class="form-control"
               placeholder="<fmt:message key="username.placeholder"/>" required>
        <small id="username-help" class="form-text text-muted">
            <fmt:message key="username.hint"/>
        </small>
    </div>


    <div class="form-group">
        <label for="password">
            <fmt:message key="password.label"/>
        </label>
        <input id="password" type="password" name="password" size="30" aria-describedby="password-help"
               class="form-control"
               placeholder="<fmt:message key="password.placeholder"/>" required>
        <small id="password-help" class="form-text text-muted">
            <fmt:message key="password.hint"/>
        </small>
    </div>

    <button type="submit" class="btn btn-lg btn-primary btn-block">
        <fmt:message key="button.register"/>
    </button>
</csrf:form>

<%@ include file="../jspf/footer.jsp" %>
