package model.database.dao.concrete.mysql;

import model.database.dao.AbstractDAO;
import model.database.dao.interfaces.UserDAO;
import model.database.exception.RepositoryException;
import model.entity.User;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class MySQLUserDAO extends AbstractDAO<User> implements UserDAO {
    private static Logger logger = Logger.getLogger(MySQLUserDAO.class);
    private final static String TABLE_NAME = "users";

    public MySQLUserDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT * FROM " + TABLE_NAME;
        String INSERT = "INSERT INTO " + TABLE_NAME + " (username, password, role) VALUES (?, ?, ?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, username, password, role) VALUES (?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE username = ?, password = ?, role = ?";
        String SELECT_BY_USERNAME = SELECT + " WHERE username = ?";
        String SELECT_BY_ROLE = SELECT + " WHERE role=?";
    }

    @Override
    protected List<User> mapEntities(ResultSet resultSet) throws SQLException {
        List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            User user = new User();
            user.setId(resultSet.getInt("id"));
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setRole(resultSet.getString("role"));
            users.add(user);
        }
        return users;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }

    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, User elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setString(2, elem.getUsername());
        statement.setString(3, elem.getPassword());
        statement.setString(4, elem.getRole());

        statement.setString(5, elem.getUsername());
        statement.setString(6, elem.getPassword());
        statement.setString(7, elem.getRole());
    }

    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, User elem) throws SQLException {
        statement.setString(1, elem.getUsername());
        statement.setString(2, elem.getPassword());
        statement.setString(3, elem.getRole());
    }

    @Override
    public Optional<User> get(String username) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.SELECT_BY_USERNAME)) {

            statement.setString(1, username);
            ResultSet resultSet = statement.executeQuery();
            List<User> elemList = mapEntities(resultSet);

            return elemList.stream().findFirst();
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error getting user with username " + username;
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<User> getUsersWithRole(User.Role role) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.SELECT_BY_ROLE)) {

            statement.setString(1, role.name());
            ResultSet resultSet = statement.executeQuery();
            return mapEntities(resultSet);
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error getting users with role " + role;
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }
}