package model.database.dao.concrete.mysql;


import model.entity.Game;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLGameDAOTest {

    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MySQLGameDAO mySQLGameDAO;

    private Game source = Game.newBuilder()
            .id(8)
            .userId(85)
            .configurationId(621)
            .build();

    @Before
    public void setUp() throws SQLException {
        mySQLGameDAO = new MySQLGameDAO(connection);
    }

    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLGameDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLGameDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLGameDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLGameDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLGameDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLGameDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLGameDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLGameDAO.getInsertQuery().isEmpty());
    }

    @Test
    public void testMapEntities() throws SQLException{
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("id")).thenReturn(source.getId());
        when(resultSet.getInt("user_id")).thenReturn(source.getUserId());
        when(resultSet.getInt("configuration_id")).thenReturn(source.getConfigurationId());

        List<Game> games = mySQLGameDAO.mapEntities(resultSet);

        assertNotNull(games);
        assertEquals(2, games.size());
        for (Game game : games) {
            assertEquals(source.getConfigurationId(), game.getConfigurationId());
            assertEquals(source.getUserId(), game.getUserId());
            assertEquals(source.getId(), game.getId());
        }

        verify(resultSet, times(3)).next();
        verify(resultSet, times(6)).getInt(anyString());
    }

    @Test
    public void testPrepareStatementForMerge() throws SQLException{

        mySQLGameDAO.prepareStatementForMerge(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setInt(anyInt(), eq(source.getId()));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getConfigurationId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getUserId()), eq(Types.INTEGER));
    }

    @Test
    public void testPrepareStatementForInsert() throws SQLException{
        mySQLGameDAO.prepareStatementForInsert(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getConfigurationId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getUserId()), eq(Types.INTEGER));
    }

}
