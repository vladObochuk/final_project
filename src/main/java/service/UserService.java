package service;

import model.entity.User;
import org.apache.catalina.CredentialHandler;

import java.util.List;

public interface UserService {

    /**
     * Retrieve user by username
     *
     * @param username {@code not null}
     * @return user object with specified username
     */
    public User getUser(String username);

    /**
     * Retrieve user by id
     *
     * @param id not {@code null}
     * @return user object
     */
    public User getUser(Integer id);

    /**
     * Retrive all user with specified role
     *
     * @param role not {@code null}
     * @return list of user witch role equals to specified
     */
    public List<User> getUsersWithRole(User.Role role);

    /**
     * save user to database
     *
     * @param user              not {@code null}
     * @param credentialHandler not {@code null}
     */
    public void registerUser(User user, CredentialHandler credentialHandler);

    /**
     * Change user password
     *
     * @param user              not {@code null}
     * @param oldPassword       not {@code null}
     * @param newPassword       not {@code null}
     * @param credentialHandler not {@code null}
     */
    public void changePassword(User user, String oldPassword, String newPassword, CredentialHandler credentialHandler);

    /**
     * Save new user state to databse
     *
     * @param user not {@code null}
     */
    public void mergeUser(User user);

    /**
     * Remove user from database
     *
     * @param id not {@code null}
     */
    public void deleteUser(Integer id);

    /**
     * Change user by specified modifications. If password equals {@code null}, saves old password
     *
     * @param modified          modifications in user
     * @param credentialHandler not {@code null}
     */
    public void modifyUser(User modified, CredentialHandler credentialHandler);
}
