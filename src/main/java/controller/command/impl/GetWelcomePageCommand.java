package controller.command.impl;

import constants.PagePath;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetWelcomePageCommand extends AbstractCommand {

    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        return PagePath.WELCOME;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required welcome page with POST method ");
    }
}
