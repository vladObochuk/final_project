package validation;

import validation.annotation.*;

public class TestEntity {

    @Min(value = TestData.minValue, message = TestData.MinConstrain)
    @Max(value = TestData.maxValue, message = TestData.MaxConstrain)
    int value;

    @NotNull(value = TestData.NotNullConstrain)
    @MinLength(value = TestData.minLengthValue, message = TestData.MinLengthConstrain)
    @MaxLength(value = TestData.maxLengthValue, message = TestData.MaxLengthConstrain)
    @Regex(value = TestData.regexValue, message = TestData.RegexConstrain)
    String text;

    public TestEntity(int value, String text) {
        this.value = value;
        this.text = text;
    }
}
