package model.database.dao.concrete.mysql;

import model.database.dao.EagerSupportAbstractDAO;
import model.database.dao.interfaces.ConfigDAO;
import model.database.exception.RepositoryException;
import model.entity.Config;
import model.entity.HintType;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.*;

public class MySQLConfigDAO extends EagerSupportAbstractDAO<Config> implements ConfigDAO {
    private static Logger logger = Logger.getLogger(MySQLConfigDAO.class);
    private final static String TABLE_NAME = "configs";

    public MySQLConfigDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT c.id, c.questions_amount, c.time_for_reply, c.user_id, h.id AS hint_id, h.name AS hint_name FROM " +
                TABLE_NAME + " AS c " +
                "LEFT JOIN configs_hinttypes AS c_h " +
                "ON c.id =c_h.config_id " +
                "LEFT JOIN hint_types AS h " +
                "ON c_h.hint_type_id = h.id";
        String INSERT = "INSERT INTO " + TABLE_NAME + " (time_for_reply, questions_amount, user_id) VALUES (?, ?, ?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, time_for_reply, questions_amount, user_id) VALUES (?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE time_for_reply = ?, questions_amount = ?, user_id = ?";
        String INSERT_HINT_TYPES = "INSERT INTO configs_hinttypes (config_id, hint_type_id) VALUES (?, ?)";
        String SELECT_LAST_USER_CONFIG = SELECT + " WHERE c.id = (SELECT MAX(id) FROM " + TABLE_NAME + " WHERE user_id = ?)";
    }


    @Override
    protected List<Config> mapEntities(ResultSet resultSet) throws SQLException {
        List<Config> configs = new ArrayList<>();

        Config config = new Config();
        Set<HintType> hintTypes = new HashSet<>();
        while (resultSet.next()) {
            Integer id = resultSet.getInt("id");
            if (Objects.equals(id, config.getId())) {    // retrieve next hintType for that config
                hintTypes.add(retrieveHintType(resultSet));
            } else {
                hintTypes = new HashSet<>();    // retrieving next config
                config = retrieveConfig(resultSet);
                config.setHintTypes(hintTypes);
                configs.add(config);
                HintType hintType = retrieveHintType(resultSet);
                if (hintType.getName() != null)
                    hintTypes.add(hintType);
            }
        }
        return configs;
    }


    private Config retrieveConfig(ResultSet resultSet) throws SQLException {
        Config config = new Config();
        config.setId(resultSet.getInt("id"));
        config.setTimeToReply(resultSet.getInt("time_for_reply"));
        config.setQuestionsAmount(resultSet.getInt("questions_amount"));
        config.setUserId(resultSet.getInt("user_id"));
        return config;
    }


    private HintType retrieveHintType(ResultSet resultSet) throws SQLException {
        HintType hintType = new HintType();
        hintType.setId(resultSet.getInt("hint_id"));
        hintType.setName(resultSet.getString("hint_name"));
        return hintType;
    }


    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }


    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }


    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, Config elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setObject(2, elem.getTimeToReply(), Types.INTEGER);
        statement.setObject(3, elem.getQuestionsAmount(), Types.INTEGER);
        statement.setObject(4, elem.getUserId(), Types.INTEGER);

        statement.setObject(5, elem.getTimeToReply(), Types.INTEGER);
        statement.setObject(6, elem.getQuestionsAmount(), Types.INTEGER);
        statement.setObject(7, elem.getUserId(), Types.INTEGER);
    }


    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }


    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Config elem) throws SQLException {
        statement.setObject(1, elem.getTimeToReply(), Types.INTEGER);
        statement.setObject(2, elem.getQuestionsAmount(), Types.INTEGER);
        statement.setObject(3, elem.getUserId(), Types.INTEGER);
    }


    @Override
    protected String getInsertSubEntitiesQuery() {
        return Queries.INSERT_HINT_TYPES;
    }


    @Override
    protected void prepareBatchForSubEntitiesInsert(PreparedStatement statement, Config entity) throws SQLException {
        Set<HintType> hintTypes = entity.getHintTypes();
        for (HintType hintType : hintTypes) {
            statement.setObject(1, entity.getId(), Types.INTEGER);
            statement.setObject(2, hintType.getId(), Types.INTEGER);
            statement.addBatch();
        }
    }

    @Override
    protected void parseGeneratedKeys(ResultSet generatedKeys, int[] rowsAffected, Config entity) {
    }


    @Override
    public Optional<Config> getLastUserConfiguration(int userId) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.SELECT_LAST_USER_CONFIG)) {

            statement.setObject(1, userId, Types.INTEGER);
            ResultSet resultSet = statement.executeQuery();
            List<Config> elemList = mapEntities(resultSet);

            return elemList.stream().findFirst();
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error getting last configuration";
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

}
