package model.database.dao;

public interface ErrorCodes {
    int TOO_BIG_FIELDLENGTH();

    int DUP_UNIQUE();
}
