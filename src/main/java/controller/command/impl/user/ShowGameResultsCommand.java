package controller.command.impl.user;

import constants.PagePath;
import controller.command.AbstractCommand;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ShowGameResultsCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        return PagePath.GAME_RESULTS_PAGE;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        return null;
    }
}
