package exception;

public class WrongUserCredentialException extends ApplicationException {
    public WrongUserCredentialException(String message) {
        super(message);
    }
}
