package controller.command.impl.moderator;

import constants.Alert;
import constants.AlertMessages;
import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import exception.NullRequestParameterException;
import model.entity.AnswerOption;
import model.entity.Question;
import service.QuestionService;
import service.factory.ServiceFactory;
import validation.Validator;
import validation.exception.ValidationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ModerateQuestionCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        QuestionService questionService = serviceFactory.createQuestionService();

        try {
            Integer id = CommandUtils.retrieveIntegerValue(request, "questionId");
            Question question = questionService.getQuestionById(id);
            List<AnswerOption> answerOptions = questionService.getQuestionAnswers(question.getId());
            request.setAttribute("question", question);
            request.setAttribute("answerOptions", answerOptions);
        } catch (NumberFormatException | NullRequestParameterException e){
            HttpSession session = request.getSession();
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_GET_PARAMETER);
        }
        return PagePath.QUESTION_CONFIRMATION;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {

        HttpSession session = request.getSession();
        try {
            Question question = retrieveQuestion(request);
            Validator.validate(question);
            AnswerOption rightAnswer = retrieveRightAnswer(request);
            Validator.validate(rightAnswer);
            List<AnswerOption> answerOptions = retrieveAnswerOptions(request);
            Validator.validate(answerOptions);

            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            QuestionService service = serviceFactory.createQuestionService();
            service.confirmQuestion(question, rightAnswer, answerOptions);
            session.setAttribute(Alert.SUCCESS, AlertMessages.MODERATE_QUESTION_SUCCESS);
        } catch (NullRequestParameterException | NumberFormatException e){
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_POST_PARAMETER);
        } catch (ValidationException e){
            session.setAttribute(Alert.ERROR, e.getMessage());
        }


        return request.getContextPath() + RedirectLink.MODERATOR_LIST_QUESTIONS;
    }

    private Question retrieveQuestion(HttpServletRequest request) throws NumberFormatException{
        Integer questionId = CommandUtils.retrieveIntegerValue(request, "id");
        String questionText = request.getParameter("text");
        String hint = request.getParameter("hint");
        String language = request.getParameter("lang");
        Integer userId = CommandUtils.retrieveIntegerValue(request, "createdBy");
        boolean confirmed = request.getParameter("confirm") != null;


        return Question.newBuilder()
                .id(questionId)
                .text(questionText)
                .hint(hint)
                .lang(language)
                .userId(userId)
                .isAproved(confirmed)
                .build();
    }

    private List<AnswerOption> retrieveAnswerOptions(HttpServletRequest request){
        String[] answerOptionsText = request.getParameterValues("answerOption[]");
        List<AnswerOption> answerOptions = new ArrayList<>();
        if (answerOptionsText != null) {
            answerOptions = Arrays.stream(answerOptionsText)
                    .map(AnswerOption::new)
                    .collect(Collectors.toList());
        }
        return answerOptions;
    }

    private AnswerOption retrieveRightAnswer(HttpServletRequest request){
        String rightAnswerText = request.getParameter("rightAnswer");
        return new AnswerOption(rightAnswerText);
    }
}
