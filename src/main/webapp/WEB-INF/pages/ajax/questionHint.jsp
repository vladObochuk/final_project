<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<p class="text-info"><fmt:message key="question.hint.label"/>: <c:out value="${currentQuestion.hint}"/> </p>
