<%@ include file="../jspf/header.jspf"%>

<div class="jumbotron">
    <h1 class="display-4">
        <fmt:message key="user.edit"/>
    </h1>
    <hr>

    <csrf:form action="" method="post">
        <input type="hidden" name="userId" value="${user.id}">
        <div class="form-group">
            <div class="row col-sm-6 col-lg-4">
                <label for="username">
                    <fmt:message key="username.label"/>
                </label>
                <input id="username" type="text" name="username" value="${user.username}"
                       placeholder="<fmt:message key="username.placeholder"/>" class="form-control form-control-sm">
            </div>
        </div>


        <div class="form-group">
            <div class=" row col-sm-6 col-lg-4">
                <label for="password">
                    <fmt:message key="password.label"/>
                </label>
                <input id="password" name="password" type="password" aria-describedby="password-help"
                       class="form-control form-control-sm"
                       placeholder="<fmt:message key="password.placeholder"/>">
            </div>
            <small id="password-help" class="form-text text-muted">
                <fmt:message key="password.hint"/>
            </small>
        </div>

        <div class="form-group">
            <div class="row col-sm-6 col-lg-4">
                <label for="role">
                    <fmt:message key="role.label"/>
                </label>
                <select id="role" name="role" class="form-control form-control-sm">
                    <c:forEach items="${roles}" var="role">
                        <c:choose>
                            <c:when test="${role eq user.role}">
                                <option selected>${role}</option>
                            </c:when>
                            <c:otherwise><option>
                                    ${role}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">
            <fmt:message key="user.edit"/>
        </button>
    </csrf:form>
    <hr>
</div>

<%@ include file="../jspf/footer.jsp"%>
