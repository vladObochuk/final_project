package controller.command.impl.user;

import constants.Alert;
import constants.AlertMessages;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;
import controller.utils.CommandUtils;
import exception.WrongUserCredentialException;
import model.entity.User;
import org.apache.catalina.CredentialHandler;
import org.apache.catalina.Globals;
import service.UserService;
import service.factory.ServiceFactory;
import validation.Validator;
import validation.exception.ValidationException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ChangePasswordCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Requested changePassword with GET method");
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");
        String repeatedPassword = request.getParameter("passwordRepeat");

        HttpSession session = request.getSession();
        if (newPassword.equals(repeatedPassword)){
            processPasswordChange(oldPassword, newPassword, request);
        } else {
            session.setAttribute(Alert.ERROR, AlertMessages.PASSWORDS_DONT_MATCH);
        }

        return request.getContextPath() + RedirectLink.USER_CHANGE_DATA;
    }

    private void processPasswordChange(String oldPassword, String newPassword, HttpServletRequest request){
        HttpSession session = request.getSession();
        try {
            validateUserPasswordChange(newPassword, request);
            ServletContext context = request.getServletContext();
            CredentialHandler handler = (CredentialHandler) context.getAttribute(Globals.CREDENTIAL_HANDLER);

            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            UserService userService = serviceFactory.createUserService();
            User user = CommandUtils.getUser(request);
            userService.changePassword(user, oldPassword, newPassword, handler);
            session.setAttribute(Alert.SUCCESS, AlertMessages.PASSWORD_CHANGE_SUCCESS);
        } catch (WrongUserCredentialException e){
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_PASSWORD);
        } catch (ValidationException e){
            session.setAttribute(Alert.ERROR, e.getMessage());
        }
    }

    private void validateUserPasswordChange(String newPassword, HttpServletRequest request){
        User thisUser = CommandUtils.getUser(request);
        User changed = User.newBuilder()
                .username(thisUser.getUsername())
                .role(thisUser.getRole())
                .password(newPassword)
                .build();
        Validator.validate(changed);
    }
}
