package controller.utils;

import exception.ApplicationException;
import exception.NullRequestParameterException;
import model.entity.User;
import org.apache.log4j.Logger;
import service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Provides widely used operations by Command objects
 */
public class CommandUtils {
    private static Logger logger = Logger.getLogger(CommandUtils.class);

    /**
     * Retrieve user from request
     *
     * @param request not {@code null}
     * @return User from session attribute or {@code null} if it not exist there
     * @throws ApplicationException if session is not initialized
     */
    public static User getUser(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            return (User) session.getAttribute("model.entity.User");
        }
        final String MESSAGE = "User in session is not initialized";
        logger.fatal(MESSAGE);
        throw new ApplicationException(MESSAGE);
    }

    /**
     * Retrieve ServiceFactory from request
     *
     * @param request not {@code null}
     * @return {@code ServiceFactory} from request attribute, or {@code null} if it not exist
     * @throws ApplicationException if retrieved object has wrong type
     */
    public static ServiceFactory getServiceFactory(HttpServletRequest request) {
        try {
            return (ServiceFactory) request.getAttribute("service.factory.ServiceFactory");
        } catch (ClassCastException e) {
            final String MESSAGE = "Request attribute service.factory.ServiceFactory has wrong type";
            logger.fatal(MESSAGE, e);
            throw new ApplicationException(MESSAGE);
        }
    }

    /**
     * Retrieve integer value from request
     *
     * @param request       not {@code null}
     * @param parameterName not {@code null} name of parameter to be retrieved from request
     * @return value of parameter
     * @throws NumberFormatException         if in parameter not {@code Integer} value
     * @throws NullRequestParameterException if there are not parameter with specified name
     */
    public static Integer retrieveIntegerValue(HttpServletRequest request, String parameterName) {
        String value = request.getParameter(parameterName);
        if (value != null) {
            String trimmedValue = value.trim();
            return Integer.parseInt(trimmedValue);
        }
        throw new NullRequestParameterException(parameterName + " is null");
    }

    /**
     * Add to specified attribute 1. Attribute first seek in session then on request
     *
     * @param request       not {@code null}
     * @param attributeName {@code null} name of attribute
     */
    public static void incrementAttribute(HttpServletRequest request, String attributeName) {
        HttpSession session = request.getSession();
        Object attributeObject = session.getAttribute(attributeName);
        if (attributeObject == null) {
            attributeObject = request.getAttribute(attributeName);
        }
        Integer attributeValue = (Integer) attributeObject;
        session.setAttribute(attributeName, attributeValue + 1);
    }

}
