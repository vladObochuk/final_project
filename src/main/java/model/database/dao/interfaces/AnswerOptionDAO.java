package model.database.dao.interfaces;

import model.database.dao.DAO;
import model.entity.AnswerOption;

import java.util.List;

/**
 * Provides additional methods for accessing AnswerOption objects in database
 */
public interface AnswerOptionDAO extends DAO<AnswerOption> {
    /**
     * get all answer options of specified question
     * @param questionId id of question which answer options needed
     * @return {@code List} of {@code AnswerOption} with questionId equals specified questionId
     */
    List<AnswerOption> getQuestionAnswers(Integer questionId);
}
