package model.database.dao.concrete.mysql;

import model.database.dao.AbstractDAO;
import model.database.dao.interfaces.AnswerDAO;
import model.entity.Answer;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLAnswerDAO extends AbstractDAO<Answer> implements AnswerDAO {
    private final static String TABLE_NAME = "answers";

    public MySQLAnswerDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT * FROM " + TABLE_NAME;
        String INSERT = "INSERT INTO " + TABLE_NAME + " (game_id, question_id, chosen_answer_id) VALUES (?, ?, ?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, game_id, question_id, chosen_answer_id) VALUES (?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE game_id = ?, question_id = ?, chosen_answer_id = ?";
    }

    @Override
    protected List<Answer> mapEntities(ResultSet resultSet) throws SQLException {
        List<Answer> answers = new ArrayList<>();
        while (resultSet.next()) {
            Answer answer = new Answer();
            answer.setId(resultSet.getInt("id"));
            answer.setGameID(resultSet.getInt("game_id"));
            answer.setQuestionId(resultSet.getInt("question_id"));
            answer.setChosenAnswerId(resultSet.getInt("chosen_answer_id"));
            answers.add(answer);
        }
        return answers;
    }


    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }


    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }


    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, Answer elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setObject(2, elem.getGameID(), Types.INTEGER);
        statement.setObject(3, elem.getQuestionId(), Types.INTEGER);
        statement.setObject(4, elem.getChosenAnswerId(), Types.INTEGER);

        statement.setObject(5, elem.getGameID(), Types.INTEGER);
        statement.setObject(6, elem.getQuestionId(), Types.INTEGER);
        statement.setObject(7, elem.getChosenAnswerId(), Types.INTEGER);
    }


    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }


    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Answer elem) throws SQLException {
        statement.setObject(1, elem.getGameID(), Types.INTEGER);
        statement.setObject(2, elem.getQuestionId(), Types.INTEGER);
        statement.setObject(3, elem.getChosenAnswerId(), Types.INTEGER);
    }
}
