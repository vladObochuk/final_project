<%@ include file="../jspf/header.jspf" %>

<div class="text-center">
    <h1>
        <fmt:message key="game.start"/>
    </h1>
</div>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">
                    <fmt:message key="rules"/>
                </h3>
            </div>

            <div class="card-body">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <fmt:message key="rule1"/>
                    </li>
                    <li class="list-group-item">
                        <fmt:message key="rule2"/>
                    </li>
                    <li class="list-group-item">
                        <fmt:message key="rule3"/>
                    </li>
                    <li class="list-group-item">
                        <fmt:message key="rule4"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col">
        <div class="card">
            <div class="card-header">
                <h3 class="text-center">
                    <fmt:message key="config"/>
                </h3>
            </div>
            <div class="card-body">
                <p class="card-text">
                    <fmt:message key="config.time"/>: <c:out value="${config.timeToReply}"/>
                </p>
                <p class="card-text">
                    <fmt:message key="config.question.amount"/>: <c:out value="${config.questionsAmount}"/>
                </p>
                <p class="card-text">
                    <fmt:message key="hintTypes.label"/>
                </p>
                <c:choose>
                    <c:when test="${empty config.hintTypes}">
                        <p class="card-text">
                            <fmt:message key="none"/>
                        </p>
                    </c:when>

                    <c:otherwise>
                        <ul class="list-group list-group-flush">
                            <c:forEach items="${config.hintTypes}" var="hintType">
                                <li class="list-group-item">
                                    <fmtdt:message key="${hintType.name}" defaultVal=""/>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:otherwise>

                </c:choose>
            </div>
        </div>
    </div>
</div>

<csrf:form action="" method="post" htmlClass="text-center">
    <button type="submit" class="btn btn-lg btn-primary">
        <fmt:message key="game.start"/>
    </button>
</csrf:form>


<%@ include file="../jspf/footer.jsp" %>
