package model.database.dao.concrete.mysql;

import model.database.dao.AbstractDAO;
import model.database.dao.interfaces.HintTypeDAO;
import model.entity.HintType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MySQLHintTypeDAO extends AbstractDAO<HintType> implements HintTypeDAO {
    private final static String TABLE_NAME = "hint_types";

    public MySQLHintTypeDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT * FROM " + TABLE_NAME;
        String INSERT = "INSERT INTO " + TABLE_NAME + " (name) VALUES (?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, name) VALUES (?, ?)" +
                "ON DUPLICATE KEY UPDATE name = ?";
    }

    @Override
    protected List<HintType> mapEntities(ResultSet resultSet) throws SQLException {
        List<HintType> hintTypes = new ArrayList<>();
        while (resultSet.next()) {
            HintType hintType = new HintType();
            hintType.setId(resultSet.getInt("id"));
            hintType.setName(resultSet.getString("name"));
            hintTypes.add(hintType);
        }
        return hintTypes;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }

    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, HintType elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setString(2, elem.getName());

        statement.setString(3, elem.getName());
    }

    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, HintType elem) throws SQLException {
        statement.setString(1, elem.getName());
    }
}
