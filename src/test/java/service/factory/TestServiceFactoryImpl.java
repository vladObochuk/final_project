package service.factory;

import model.database.dao.factory.DAOFactory;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import service.GameService;
import service.QuestionService;
import service.TimeService;
import service.UserService;

import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class TestServiceFactoryImpl {
    @Mock
    DAOFactory daoFactory;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private ServiceFactory serviceFactory;

    @Before
    public void setUp(){
        serviceFactory = new ServiceFactoryImpl(daoFactory);
    }

    @Test
    public void testCreateGameService(){
        GameService gameService = serviceFactory.createGameService();
        assertNotNull(gameService);
    }

    @Test
    public void testCreateUserService(){
        UserService userService = serviceFactory.createUserService();
        assertNotNull(userService);
    }
    @Test
    public void testCreateQuestionService(){
        QuestionService questionService = serviceFactory.createQuestionService();
        assertNotNull(questionService);
    }
    @Test
    public void testCreateTimeService(){
        TimeService timeService = serviceFactory.createTimeService();
        assertNotNull(timeService);
    }

}
