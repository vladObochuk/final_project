package controller.command.manager.impl.user;

/**
 * Provides names of actions proper for ordinary user
 */
public interface UserAction {
    String VIEW_QUESTION_DETAILS = "questionDetails";
    String VIEW_ALL_QUESTIONS = "listQuestions";
    String EDIT_DATA = "changeData";
    String CHANGE_PASSWORD = "changePassword";
    String LOG_OUT = "logout";
    String ADD_QUESTION = "addQuestion";
    String CONFIGURE_GAME = "configure";
    String START_GAME = "startGame";
    String GAME = "game";
    String SHOW_RESULTS = "showResults";
    String GET_ADDITIONAL_TIME = "getTime";
    String GET_HINT = "showHint";
    String GET_PROBABILITY = "getProbability";
    String REMOVE_OPTIONS = "deleteOptions";
}
