package controller.command.impl.user;

import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SignOutCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required signOut on GET method");
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        session.invalidate();

        org.apache.tomcat.util.net.SSLSessionManager mgr =
                (org.apache.tomcat.util.net.SSLSessionManager)
                        request.getAttribute("javax.servlet.request.ssl_session_mgr");
        mgr.invalidateSession();

        response.setHeader("Connection", "close");
        return request.getContextPath();
    }
}
