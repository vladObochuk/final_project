package model.entity;

import model.database.DomainObject;

public class Game extends DomainObject {
    private Integer userId;
    private Integer configurationId;

    public static Builder  newBuilder(){
        return new Game().new Builder();
    }

    public class Builder {
        public Builder id(Integer id){
            Game.this.id = id;
            return this;
        }


        public Builder userId(Integer userId){
            Game.this.userId = userId;
            return this;
        }

        public Builder configurationId(Integer configurationId){
            Game.this.configurationId = configurationId;
            return this;
        }

        public Game build(){
            return Game.this;
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(Integer configurationId) {
        this.configurationId = configurationId;
    }
}
