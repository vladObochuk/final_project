<%@ include file="../jspf/header.jspf" %>

<csrf:form action="" method="post" htmlClass="form-medium">
    <div class="form-group">
        <label for="time">
            <fmt:message key="config.time"/>
        </label>
        <select id="time" name="time" class="form-control" required>
            <c:forEach items="${timeOptions}" var="timeOption">
                <c:choose>
                    <c:when test="${timeOption eq lastConfiguration.timeToReply}">
                        <option selected>${timeOption}</option>
                    </c:when>
                    <c:otherwise>
                        <option>${timeOption}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </div>

    <div class="form-group">
        <label for="questionAmount">
            <fmt:message key="config.question.amount"/>
        </label>
        <select id="questionAmount" name="questionAmount" class="form-control" required>
            <c:forEach items="${questionAmountOptions}" var="questionAmountOption">
                <c:choose>
                    <c:when test="${questionAmountOption eq lastConfiguration.questionsAmount}">
                        <option selected>${questionAmountOption}</option>
                    </c:when>
                    <c:otherwise>
                        <option>${questionAmountOption}</option>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
        </select>
    </div>


    <div class="form-group">
        <label>
            <fmt:message key="config.hint"/>
        </label>

        <c:forEach items="${hints}" var="hint">
            <c:set var="contains" value="${false}"/>
            <c:forEach items="${lastConfiguration.hintTypes}" var="previousHint">
                <c:if test="${hint eq previousHint}">
                    <c:set var="contains" value="${true}"/>
                </c:if>
            </c:forEach>

            <div class="form-check">
                <label class="form-check-label">
                    <c:choose>
                        <c:when test="${contains eq true}">
                            <input class="form-check-input" type="checkbox" name="hintType[]" value="${hint.id}" checked>
                        </c:when>
                        <c:otherwise>
                            <input class="form-check-input" type="checkbox" name="hintType[]" value="${hint.id}">
                        </c:otherwise>
                    </c:choose>
                    <fmtdt:message key="${hint.name}" defaultVal=""/>
                </label>
            </div>
        </c:forEach>
    </div>

    <button type="submit" class="btn btn-lg btn-primary">
        <fmt:message key="config.submit"/>
    </button>
</csrf:form>

<%@ include file="../jspf/footer.jsp" %>
