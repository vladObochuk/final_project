<%@ tag body-content="scriptless" %>
<%@ attribute name="action" required="true" rtexprvalue="true" %>
<%@ attribute name="method" required="true" rtexprvalue="true" %>
<%@ attribute name="htmlClass" required="false" rtexprvalue="true" %>

<form action="${action}" method="${method}" class="${htmlClass}">

    <jsp:doBody var="formBody"/>
    ${formBody}

    <input id="token" type="hidden" name="csrfToken" value="${sessionScope.csrfToken}">
</form>
