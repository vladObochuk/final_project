package validation;

public interface TestData {
    int minValue = 8;
    String MinConstrain = "Smaller then min";
    int maxValue = 120;
    String MaxConstrain = "Bigger then max";

    String NotNullConstrain = "Null";
    int minLengthValue = 1;
    String MinLengthConstrain = "Length smaller then min";
    int maxLengthValue = 5;
    String MaxLengthConstrain = "Length bigger then max";
    String regexValue = "\\p{L}+";
    String RegexConstrain = "Regex don`t match";
}
