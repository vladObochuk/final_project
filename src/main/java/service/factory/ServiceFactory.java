package service.factory;

import service.GameService;
import service.QuestionService;
import service.TimeService;
import service.UserService;

/**
 * Provides methods of creation services
 */
public interface ServiceFactory {

    /**
     * creates object of game service
     *
     * @return not {@code null} game service
     */
    GameService createGameService();

    /**
     * creates object of user service
     *
     * @return not {@code null} user service
     */
    UserService createUserService();

    /**
     * creates object of question service
     *
     * @return not {@code null} question service
     */
    QuestionService createQuestionService();

    /**
     * creates object of time service
     *
     * @return not {@code null} time service
     */
    TimeService createTimeService();
}
