package controller.command.impl.user;

import constants.Alert;
import constants.AlertMessages;
import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import model.entity.AnswerOption;
import model.entity.Question;
import model.entity.User;
import service.QuestionService;
import service.factory.ServiceFactory;
import validation.Validator;
import validation.exception.ValidationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class AddQuestionCommand extends AbstractCommand {

    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        return PagePath.ADD_QUESTION;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();

        try {
            Question question = retrieveQuestion(request);
            Validator.validate(question);
            AnswerOption rightAnswer = retrieveRightAnswer(request);
            Validator.validate(rightAnswer);
            List<AnswerOption> answerOptions = retrieveAnswerOptions(request);
            Validator.validate(answerOptions);

            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            QuestionService questionService = serviceFactory.createQuestionService();
            questionService.addQuestion(question, rightAnswer, answerOptions);
            session.setAttribute(Alert.SUCCESS, AlertMessages.ADD_QUESTION_SUCCESS);
        } catch (ValidationException e){
            session.setAttribute(Alert.ERROR, e.getMessage());
            return request.getContextPath() + RedirectLink.USER_ADD_QUESTION;
        } catch (Exception ignore){}

        return request.getContextPath() + RedirectLink.USER_LIST_QUESTIONS;
    }

    private Question retrieveQuestion(HttpServletRequest request) {
        String questionText = request.getParameter("text");
        String hint = request.getParameter("hint");
        String language = request.getParameter("lang");

        HttpSession session = request.getSession();
        User user = CommandUtils.getUser(request);
        Integer userId = user.getId();

        return Question.newBuilder()
                .text(questionText)
                .hint(hint)
                .lang(language)
                .userId(userId)
                .build();
    }

    private List<AnswerOption> retrieveAnswerOptions(HttpServletRequest request) {
        String[] answerOptionsText = request.getParameterValues("answerOption[]");
        List<AnswerOption> answerOptions = new ArrayList<>();
        if (answerOptionsText != null) {
            answerOptions = Arrays.stream(answerOptionsText)
                    .map(AnswerOption::new)
                    .collect(Collectors.toList());
        }
        return answerOptions;
    }

    private AnswerOption retrieveRightAnswer(HttpServletRequest request) {
        String rightAnswerText = request.getParameter("rightAnswer");
        return new AnswerOption(rightAnswerText);
    }
}
