package constants;

/**
 * Keys to resourceBundle messages on some validation errors
 */
public interface ValidatorMessages {
    String USERNAME_NULL = "user.username.null";
    String USERNAME_SMALL = "user.username.small";
    String USERNAME_BIG = "user.username.big";
    String USERNAME_WRONG = "user.username.regex";


    String PASSWORD_NULL = "user.password.null";
    String PASSWORD_SMALL = "user.password.small";


    String QUESTION_TEXT_NULL = "question.text.null";
    String QUESTION_TEXT_SMALL = "question.text.small";
    String QUESTION_TEXT_BIG = "question.text.big";

    String QUESTION_HINT_NULL = "question.hint.null";
    String QUESTION_HINT_BIG = "question.hint.big";

    String QUESTION_LANG_NULL = "question.lang.null";


    String OPTION_TEXT_NULL = "answerOption.text.null";
    String OPTION_TEXT_SMALL = "answerOption.text.small";
    String OPTION_TEXT_BIG = "answerOption.text.big";


    String CONFIG_TIME_LITTLE = "config.time.little";
    String CONFIG_TIME_ALOT = "config.time.lot";

    String CONFIG_QUESTION_FEW = "config.question.few";
    String CONFIG_QUESTION_ALOT = "config.question.lot";

}
