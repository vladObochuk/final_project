<%@ include file="../jspf/header.jspf" %>


<csrf:form action="" method="post" htmlClass="form-medium">
    <input type="hidden" name="id" value="${question.id}">
    <input type="hidden" name="createdBy" value="${question.userId}">

    <div class="form-group">
        <div class="row col-sm-6 col-lg-4">
            <label for="lang">
                <fmt:message key="question.lang"/>
            </label>
            <select id="lang" name="lang" class="form-control">

                <c:choose>
                    <c:when test="${question.language eq 'en'}">
                        <c:set var="engSelected" value="selected"/>
                    </c:when>

                    <c:otherwise>
                        <c:set var="ukSelected" value="selected"/>
                    </c:otherwise>
                </c:choose>

                <option value="en" ${engSelected}>
                    <fmt:message key="lang.en"/>
                </option>
                <option value="uk" ${ukSelected}>
                    <fmt:message key="lang.uk"/>
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="text">
            <fmt:message key="question.text.label"/>
        </label>
        <textarea id="text" placeholder="<fmt:message key="question.text.placeholder"/> "
                  name="text" cols="25" rows="6" maxlength="768" class="form-control"
                  required><c:out value="${question.text}"/></textarea>
    </div>

    <c:forEach items="${answerOptions}" var="answerOption">
        <c:if test="${question.rightAnswerId eq answerOption.id}">
            <c:set var="rightAnswer" value="${answerOption}" scope="request"/>
        </c:if>
    </c:forEach>
    <div class="form-group">
        <label for="rightAnswer">
            <fmt:message key="question.answer.label"/>
        </label>
        <input id="rightAnswer" name="rightAnswer" type="text" class="form-control"
               placeholder="<fmt:message key="question.answer.placeholder"/> "
               value="<c:out value="${rightAnswer.text}"/>" required>
    </div>

    <div class="form-group">
        <div class="input_fields_wrap">
            <button type="button" class="add_field_button btn btn-success btn-sm">
                <fmt:message key="question.appendOption.label"/>
            </button>
            <c:forEach items="${answerOptions}" var="answerOption">
                <c:if test="${answerOption.id ne rightAnswer.id}">
                    <div class="input-group mt-3">
                        <input type="text" name="answerOption[]" class="form-control input-sm"
                               value="<c:out value="${answerOption.text}"/>"/>
                        <a href="#" class="remove_field btn btn-outline-secondary input-group-append"><span
                                class="fa fa-remove"></span> </a>
                    </div>
                </c:if>
            </c:forEach>
        </div>
    </div>


    <div class="form-group">
        <label for="hint">
            <fmt:message key="question.hint.label"/>
        </label>
        <input id="hint" name="hint" type="text"
               placeholder="<fmt:message key="question.hint.placeholder"/> " class="form-control"
               value="<c:out value="${question.hint}"/>" required>
    </div>

    <div class="row">
        <div class="col-6 text-center">
            <button type="submit" name="confirm" class="btn btn-success">
                <fmt:message key="question.button.confirm"/>
            </button>
        </div>
        <div class="col-6 text-left">
            <button type="submit" name="reject" class="btn btn-danger">
                <fmt:message key="question.button.reject"/>
            </button>
        </div>
    </div>

</csrf:form>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/addField.js"></script>

<%@ include file="../jspf/footer.jsp" %>