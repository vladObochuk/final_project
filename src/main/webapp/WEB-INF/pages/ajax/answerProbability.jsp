<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${probabilityMap}" var="entry" varStatus="loop">
    <li class="list-group-item">
        <div class="form-check">
            <input id="radio${loop.index}" class="form-check-input" name="chosenAnswerId"
                   type="radio"
                   value="${entry.key.id}">
            <label for="radio${loop.index}" class="form-check-label">
                <c:out value="${entry.key.text}"/>
            </label>
            <fn:formatNumber value="${entry.value}" type="number" maxFractionDigits="2" var="probability"/>
            <span class="badge badge-info">${probability}</span>
        </div>
    </li>
</c:forEach>
