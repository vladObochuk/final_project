package model.database.dao.concrete.mysql;

import model.database.exception.RepositoryException;
import model.entity.Question;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLQuestionDAOTest {

    @Mock
    Connection connection;

    @Mock
    Statement statement;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MySQLQuestionDAO mySQLQuestionDAO;

    private Question source = Question.newBuilder()
            .id(555)
            .hint("HINT")
            .lang("en")
            .isAproved(true)
            .rightAnswerId(8952)
            .userId(2)
            .text("BIIIG TEXT")
            .build();

    @Before
    public void setUp() throws SQLException {
        mySQLQuestionDAO = new MySQLQuestionDAO(connection);
    }

    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLQuestionDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLQuestionDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLQuestionDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLQuestionDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLQuestionDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLQuestionDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLQuestionDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLQuestionDAO.getInsertQuery().isEmpty());
    }

    @Test
    public void testMapEntities() throws SQLException{

        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("id")).thenReturn(source.getId());
        when(resultSet.getInt("right_answer_id")).thenReturn(source.getRightAnswerId());
        when(resultSet.getInt("user_id")).thenReturn(source.getUserId());
        when(resultSet.getBoolean("approved")).thenReturn(source.getApproved());
        when(resultSet.getString("hint")).thenReturn(source.getHint());
        when(resultSet.getString("text")).thenReturn(source.getText());
        when(resultSet.getString("lang")).thenReturn(source.getLanguage());

        List<Question> questions = mySQLQuestionDAO.mapEntities(resultSet);

        assertNotNull(questions);
        assertEquals(2, questions.size());
        for (Question question: questions) {
            assertEquals(source.getId(), question.getId());
            assertEquals(source.getApproved(), question.getApproved());
            assertEquals(source.getHint(), question.getHint());
            assertEquals(source.getRightAnswerId(), question.getRightAnswerId());
            assertEquals(source.getText(), question.getText());
            assertEquals(source.getUserId(), question.getUserId());
            assertEquals(source.getLanguage(), question.getLanguage());
        }

        verify(resultSet, times(3)).next();
        verify(resultSet, times(6)).getInt(anyString());
        verify(resultSet, times(2)).getBoolean(anyString());
        verify(resultSet, times(6)).getString(anyString());
    }

    @Test
    public void testPrepareStatementForMerge() throws SQLException{
        mySQLQuestionDAO.prepareStatementForMerge(preparedStatement, source);

        verify(preparedStatement, atLeast(1)).setInt(anyInt(), eq(source.getId()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getText()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getHint()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getLanguage()));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getRightAnswerId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getUserId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getApproved()), eq(Types.BOOLEAN));
    }

    @Test
    public void testPrepareStatementForInsert() throws SQLException{
        mySQLQuestionDAO.prepareStatementForInsert(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getText()));
        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getHint()));
        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getLanguage()));
        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getRightAnswerId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getUserId()), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getApproved()), eq(Types.BOOLEAN));
    }

    @Test
    public void testGetQuestionsNullApproved() throws SQLException{
        Boolean isApproved = false;
        String lang = "en";
        Integer userId = 5;
        Integer amount = 1;

        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getBoolean("approved")).thenReturn(isApproved);
        when(resultSet.getString("lang")).thenReturn(lang);
        when(resultSet.getInt("user_id")).thenReturn(userId);

        List<Question> questions = mySQLQuestionDAO.getRandomQuestionsWith(isApproved, lang, amount, userId);
        assertEquals(amount, (Integer) questions.size());
        for (Question question : questions) {
            assertEquals(isApproved, question.getApproved());
            assertEquals(lang, question.getLanguage());
            assertEquals(userId, question.getUserId());
        }

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeQuery();
        verify(preparedStatement, times(1)).close();
        verify(preparedStatement, atLeast(1)).setObject(anyInt(), eq(isApproved), eq(Types.BOOLEAN));
        verify(preparedStatement, atLeast(1)).setString(anyInt(), eq(lang));
        verify(preparedStatement, atLeast(1)).setObject(anyInt(), eq(userId), eq(Types.INTEGER));
        verify(preparedStatement, atLeast(1)).setObject(anyInt(), eq(amount), eq(Types.INTEGER));

    }

    @Test(expected = RepositoryException.class)
    public void testGetQuestionsNullApprovedException() throws SQLException{
        when(connection.createStatement()).thenThrow(new SQLException());

        mySQLQuestionDAO.getQuestionsNullApproved();
    }

    @Test
    public void getRandomQuestionsWith() throws SQLException{
        when(connection.createStatement()).thenReturn(statement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        List<Question> questions = mySQLQuestionDAO.getQuestionsNullApproved();
        assertEquals(2, questions.size());

        verify(connection, times(1)).createStatement();
        verify(statement, times(1)).executeQuery(anyString());
        verify(resultSet, times(3)).next();
        verify(statement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void getRandomQuestionsWithException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        mySQLQuestionDAO.getRandomQuestionsWith(false, "", 5, 5);
    }

}
