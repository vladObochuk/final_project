package controller.command.manager.impl.unauthorized;

/**
 * Provides names of actions proper for unauthorized user
 */
public interface UnauthorizedAction {
    String REGISTER = "register";
}
