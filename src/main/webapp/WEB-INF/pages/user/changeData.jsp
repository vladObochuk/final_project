<%@ include file="../jspf/header.jspf" %>

<c:url value="/user.do" var="changePasswordURL">
    <c:param name="action" value="changePassword"/>
</c:url>

<div class="jumbotron">
    <h1 class="display-4">
        <fmt:message key="changeData.password"/>
    </h1>
    <hr>

    <csrf:form action="${changePasswordURL}" method="post">
        <div class="form-group">
            <div class="row col-sm-6 col-lg-4">
                <label for="oldPassword">
                    <fmt:message key="changeData.password.old.label"/>
                </label>
                <input id="oldPassword" name="oldPassword" type="password" class="form-control form-control-sm"
                       placeholder="<fmt:message key="changeData.password.old.placeholder"/>" required>
            </div>
        </div>


        <div class="form-group">
            <div class=" row col-sm-6 col-lg-4">
                <label for="newPassword">
                    <fmt:message key="changeData.password.new.label"/>
                </label>
                <input id="newPassword" name="newPassword" type="password" aria-describedby="password-help"
                       class="form-control form-control-sm"
                       placeholder="<fmt:message key="changeData.password.new.placeHolder"/>" required>
            </div>
            <small id="password-help" class="form-text text-muted">
                <fmt:message key="password.hint"/>
            </small>
        </div>

        <div class="form-group">
            <div class="row col-sm-6 col-lg-4">
                <label for="passwordRepeat">
                    <fmt:message key="changeData.password.repeat.label"/>
                </label>
                <input id="passwordRepeat" name="passwordRepeat" type="password" class="form-control form-control-sm"
                       placeholder="<fmt:message key="changeData.password.repeat.placeholder"/>" required>
            </div>
        </div>

        <button type="submit" class="btn btn-primary">
            <fmt:message key="changeData.password.submit"/>
        </button>
    </csrf:form>
    <hr>
</div>

<%@ include file="../jspf/footer.jsp" %>
