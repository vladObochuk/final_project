package model.database.exception;

import exception.ApplicationException;

import java.sql.SQLException;

public class RepositoryException extends ApplicationException {

    public RepositoryException(SQLException cause){
        super(cause);
    }

    public int getErrorCode(){
        SQLException cause = (SQLException) getCause();
        return cause.getErrorCode();
    }
}
