package controller.command.manager.impl.moderator;

import controller.command.impl.moderator.ListQuestionsCommand;
import controller.command.impl.moderator.ModerateQuestionCommand;
import controller.command.manager.CommandManager;


public class ModeratorCommandManager extends CommandManager {

    private ModeratorCommandManager() {
        commandMap.put(ModeratorAction.VIEW_QUESTIONS_TO_MODERATE, new ListQuestionsCommand());
        commandMap.put(ModeratorAction.MODERATE_QUESTION, new ModerateQuestionCommand());
    }

    private static class Holder{
        static final ModeratorCommandManager INSTANCE = new ModeratorCommandManager();
    }

    public static ModeratorCommandManager getInstance(){
        return Holder.INSTANCE;
    }
}
