package validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import validation.exception.ValidationException;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
public class ValidatorTest {
    @Parameterized.Parameter
    public int value;

    @Parameterized.Parameter(1)
    public String text;

    @Parameterized.Parameter(2)
    public boolean exceptionExpected;

    @Parameterized.Parameter(3)
    public String exceptionMessage;

    TestEntity testEntity;

    @Parameterized.Parameters(name = "{index}: TestEntity({0}, {1}) -> throws={2}, message={3}")
    public static Collection<Object[]> data(){
        return Arrays.asList(new Object[][]{
                {8, "ok", false, ""},
                {-2, "ok", true, TestData.MinConstrain},
                {140, "ok", true, TestData.MaxConstrain},
                {8, null, true, TestData.NotNullConstrain},
                {8, "", true, TestData.MinLengthConstrain},
                {8, "biggerThenMaxLength", true, TestData.MaxLengthConstrain},
                {8, "░", true, TestData.RegexConstrain},
                {8, ",", true, TestData.RegexConstrain},
                {8, "8", true, TestData.RegexConstrain}
        });
    }

    @Before
    public void setUp(){
        testEntity = new TestEntity(value, text);
    }

    @Test
    public void testValidate(){
        if (exceptionExpected){
            try {
                Validator.validate(testEntity);
                fail("Didn't throw the exception!");
            } catch (ValidationException e){
                assertEquals(exceptionMessage, e.getMessage());
            }
        } else {
            Validator.validate(testEntity);
        }
    }
}
