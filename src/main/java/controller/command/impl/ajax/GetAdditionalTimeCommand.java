package controller.command.impl.ajax;

import constants.PagePath;
import controller.command.Command;
import controller.utils.CommandUtils;
import controller.utils.HttpMethod;
import service.TimeService;
import service.factory.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GetAdditionalTimeCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws ServletException {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        TimeService timeService = serviceFactory.createTimeService();
        HttpSession session = request.getSession();
        timeService.addTime(session);
        return PagePath.EMPTY;
    }
}
