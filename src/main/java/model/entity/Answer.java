package model.entity;

import model.database.DomainObject;

public class Answer extends DomainObject {
    private Integer gameID;
    private Integer questionId;
    private Integer chosenAnswerId;

    public static Builder newBuilder(){
        return new Answer().new Builder();
    }

    public class Builder {
        public Builder gameId(Integer gameId){
            Answer.this.gameID = gameId;
            return this;
        }

        public Builder questionId(Integer questionId){
            Answer.this.questionId = questionId;
            return this;
        }

        public Builder chosenAnswerId(Integer chosenAnswerId){
            Answer.this.chosenAnswerId = chosenAnswerId;
            return this;
        }

        public Builder id(Integer id){
            Answer.this.id = id;
            return this;
        }

        public Answer build(){
            return Answer.this;
        }
    }

    public Integer getGameID() {
        return gameID;
    }

    public void setGameID(Integer gameID) {
        this.gameID = gameID;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public Integer getChosenAnswerId() {
        return chosenAnswerId;
    }

    public void setChosenAnswerId(Integer chosenAnswerId) {
        this.chosenAnswerId = chosenAnswerId;
    }
}
