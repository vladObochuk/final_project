package constants;

/**
 * Names of variables in session to read in jsp for informing users about some events
 */
public interface Alert {
    String SUCCESS = "messageSuccess";
    String INFO = "messageInfo";
    String WARN = "messageWarn";
    String ERROR = "messageError";
}
