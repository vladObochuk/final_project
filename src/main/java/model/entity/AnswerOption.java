package model.entity;

import constants.ValidatorMessages;
import model.database.DomainObject;
import validation.annotation.MaxLength;
import validation.annotation.MinLength;
import validation.annotation.NotNull;

public class AnswerOption extends DomainObject {
    @NotNull(ValidatorMessages.OPTION_TEXT_NULL)
    @MinLength(value = 1, message = ValidatorMessages.OPTION_TEXT_SMALL)
    @MaxLength(value = 50, message = ValidatorMessages.OPTION_TEXT_BIG)
    private String text;
    private Integer questionId;

    public AnswerOption(){}

    public AnswerOption(String text) {
        this.text = text;
    }

    public AnswerOption(Integer id) {
        super(id);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public boolean isTextEquals(String text){
        String thatTrimmedText = this.text.trim();
        String suppliedTrimmedText = text.trim();

        return thatTrimmedText.equalsIgnoreCase(suppliedTrimmedText);
    }
}
