package service.impl;

import exception.ApplicationException;
import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.UserDAO;
import model.entity.User;
import org.apache.catalina.CredentialHandler;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import service.UserService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestUserServiceImpl {

    @Mock
    DAOFactory daoFactory;

    @Mock
    UserDAO userDAO;

    @Mock
    CredentialHandler credentialHandler;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private UserService userService;

    private User dummyUser;

    @Before
    public void setUp() throws Exception {
        when(daoFactory.createUserDAO()).thenReturn(userDAO);
        userService = new UserServiceImpl(daoFactory);
        dummyUser = new User();
    }

    @Test
    public void testGetUserByUsername(){
        when(userDAO.get(anyString())).thenReturn(Optional.of(dummyUser));

        User user = userService.getUser("");

        assertEquals(dummyUser, user);

        verify(daoFactory, times(1)).createUserDAO();
        verify(userDAO, times(1)).get(anyString());
    }

    @Test(expected = ApplicationException.class)
    public void testGetUserByUsernameThrows(){
        when(userDAO.get(anyString())).thenReturn(Optional.empty());

        User user = userService.getUser("");
    }

    @Test
    public void testGetUserById(){
        when(userDAO.getById(anyInt())).thenReturn(Optional.of(dummyUser));

        User user = userService.getUser(0);

        assertEquals(dummyUser, user);

        verify(daoFactory, times(1)).createUserDAO();
        verify(userDAO, times(1)).getById(anyInt());
    }

    @Test(expected = ApplicationException.class)
    public void testGetUserByIdThrows(){
        when(userDAO.getById(anyInt())).thenReturn(Optional.empty());

        User user = userService.getUser(0);
    }

    @Test
    public void testGetUsersWithRole(){
        List<User> users = Arrays.asList(new User(), new User());
        User.Role role = User.Role.ADMIN;
        when(userDAO.getUsersWithRole(anyObject())).thenReturn(users);

        List<User> result = userService.getUsersWithRole(role);
        assertEquals(users, result);

        verify(daoFactory, times(1)).createUserDAO();
        verify(userDAO, times(1)).getUsersWithRole(role);
    }

    @Test
    public void testRegisterUser(){
        String passwordAfterProcessing = "password";
        when(credentialHandler.mutate(anyString())).thenReturn(passwordAfterProcessing);

        userService.registerUser(dummyUser, credentialHandler);

        assertEquals(passwordAfterProcessing, dummyUser.getPassword());

        verify(credentialHandler, times(1)).mutate(anyString());
        verify(userDAO, times(1)).insert(dummyUser);
    }

    @Test
    public void testChangePassword(){
        String passwordAfterProcessing = "password";
        when(credentialHandler.matches(anyString(), anyString())).thenReturn(true);
        when(credentialHandler.mutate(anyString())).thenReturn(passwordAfterProcessing);

        userService.changePassword(dummyUser, "", "", credentialHandler);

        assertEquals(passwordAfterProcessing, dummyUser.getPassword());
        verify(credentialHandler, times(1)).mutate(anyString());
        verify(credentialHandler, times(1)).matches(anyString(), anyString());
        verify(userDAO, times(1)).merge(dummyUser);
    }

    @Test
    public void testMergeUser(){
        userService.mergeUser(dummyUser);

        verify(userDAO, times(1)).merge(dummyUser);
    }

    @Test
    public void testDeleteUser(){
        userService.deleteUser(dummyUser.getId());

        verify(userDAO, times(1)).delete(anyInt());
    }

    @Test
    public void testModifyUser(){
        User modification = User.newBuilder().id(1).password("pass").build();

        when(userDAO.getById(anyInt())).thenReturn(Optional.of(dummyUser));

        userService.modifyUser(modification, credentialHandler);

        verify(credentialHandler, times(1)).mutate(anyString());
        verify(userDAO, times(1)).merge(anyObject());
        verify(userDAO, times(1)).getById(anyInt());

    }
}
