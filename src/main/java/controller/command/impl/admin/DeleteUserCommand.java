package controller.command.impl.admin;

import constants.Alert;
import constants.AlertMessages;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;
import controller.utils.CommandUtils;
import exception.NullRequestParameterException;
import service.UserService;
import service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class DeleteUserCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required deleteUser with GET method");
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        try {
            Integer userId = CommandUtils.retrieveIntegerValue(request,"userId");
            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            UserService userService = serviceFactory.createUserService();
            userService.deleteUser(userId);
            session.setAttribute(Alert.SUCCESS, AlertMessages.DELETE_USER_SUCCESS);
        } catch (NullRequestParameterException | NumberFormatException e){
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_POST_PARAMETER);
        }
        return request.getContextPath() + RedirectLink.ADMIN_LIST_MODERATORS;
    }
}
