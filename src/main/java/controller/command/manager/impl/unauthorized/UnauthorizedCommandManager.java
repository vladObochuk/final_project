package controller.command.manager.impl.unauthorized;

import controller.command.impl.unauthorized.RegisterCommand;
import controller.command.manager.CommandManager;

public class UnauthorizedCommandManager extends CommandManager {
    private UnauthorizedCommandManager(){
        commandMap.put(UnauthorizedAction.REGISTER, new RegisterCommand());
    }

    private static class Holder{
        static final UnauthorizedCommandManager INSTANCE = new UnauthorizedCommandManager();
    }

    public static UnauthorizedCommandManager getInstance(){
        return Holder.INSTANCE;
    }
}
