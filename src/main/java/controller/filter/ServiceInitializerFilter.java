package controller.filter;


import model.database.dao.factory.DAOFactory;
import model.database.dao.factory.impl.MySQLDAOFactory;
import org.apache.log4j.Logger;
import service.factory.ServiceFactory;
import service.factory.ServiceFactoryImpl;

import javax.servlet.*;
import java.io.IOException;

/**
 * Initialize the serviceFactory for request
 */
public class ServiceInitializerFilter implements Filter {
    private static Logger logger = Logger.getLogger(ServiceInitializerFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // NOP
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            DAOFactory daoFactory = new MySQLDAOFactory();
            ServiceFactory serviceFactory = new ServiceFactoryImpl(daoFactory);
            request.setAttribute("model.database.dao.factory.DAOFactory", daoFactory);
            logger.trace("DAOFactory initialized");
            request.setAttribute("service.factory.ServiceFactory", serviceFactory);
            logger.trace("ServiceFactory initialized");
        } catch (Exception e) {
            logger.trace("Error initializing service", e);
            throw e;
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // NOP
    }
}
