package service;


import model.entity.AnswerOption;
import model.entity.Question;
import model.entity.User;

import java.util.List;
import java.util.Map;

public interface QuestionService {

    /**
     * Save question with all related objects
     *
     * @param question      not {@code null}
     * @param rightAnswer   not {@code null} correct answers to question
     * @param answerOptions nullable list of options
     */
    public void addQuestion(Question question, AnswerOption rightAnswer, List<AnswerOption> answerOptions);

    /**
     * Save question and related objects with new values
     *
     * @param question      not {@code null} with id not {@code null}
     * @param rightAnswer   not {@code null} with id not {@code null}
     * @param answerOptions nullable list of options with id not {@code null}
     */
    public void confirmQuestion(Question question, AnswerOption rightAnswer, List<AnswerOption> answerOptions);

    /**
     * Retrieve list of questions created by user
     *
     * @param userId not {@code null} value of user id
     * @return list of questions where userId equals to specified
     */
    public List<Question> getQuestionsOfUser(Integer userId);

    /**
     * Get all question that were not moderated
     *
     * @return list of questions with isApproved {@code null}
     */
    public List<Question> getNotModeratedQuestions();

    /**
     * Retrieve question from db
     *
     * @param questionId not {@code null}
     * @return Question object with specified id
     */
    public Question getQuestionById(Integer questionId);

    /**
     * Retrieve some amount of randomly chosen questions
     *
     * @param lang   not {@code null} language of question
     * @param amount bigger then 0 desirable amount of question objects
     * @param user   not {@code null} User object
     * @return list of question with specified language that were not created by specified user,
     * and length of list is equal or less then specified amount
     */
    public List<Question> getRandomQuestions(String lang, int amount, User user);

    /**
     * Get all answer options of question
     *
     * @param questionId not {@code null} id of question
     * @return list of answerOptions where questionId equals to specified
     */
    public List<AnswerOption> getQuestionAnswers(Integer questionId);

    /**
     * Retrieve user that creates question
     *
     * @param question not {@code null} with userId not {@code null}
     * @return user object
     */
    public User getCreatorOfQuestion(Question question);

    /**
     * Retrieve answer for question
     *
     * @param question not {@code null}
     * @return AnswerOption object
     */
    public AnswerOption getQuestionAnswer(Question question);

    /**
     * Create map of AnswerOption and Double that present probability of correctness of option
     *
     * @param questionAnswers not {@code null}
     * @param question        not {@code null}
     * @return Map
     */
    public Map<AnswerOption, Double> getCorrectnessProbability(List<AnswerOption> questionAnswers, Question question);

    /**
     * Select correct answer and some random answer for question
     *
     * @param questionAnswers not {@code null}, containing one that have
     *                        questionId equals to {@code question.rightAnswerId}
     * @param question        not {@code null}
     * @return List of two elements
     */
    public List<AnswerOption> getCorrectAndRandomOption(List<AnswerOption> questionAnswers, Question question);
}
