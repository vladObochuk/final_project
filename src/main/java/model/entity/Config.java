package model.entity;

import constants.ValidatorMessages;
import model.database.DomainObject;
import validation.annotation.Max;
import validation.annotation.Min;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class Config extends DomainObject {
    @Min(value = 30, message = ValidatorMessages.CONFIG_TIME_LITTLE)
    @Max(value = 200, message = ValidatorMessages.CONFIG_TIME_ALOT)
    private int timeToReply;

    @Min(value = 3, message = ValidatorMessages.CONFIG_QUESTION_FEW)
    @Max(value = 15, message = ValidatorMessages.CONFIG_QUESTION_ALOT)
    private int questionsAmount;
    private int userId;
    private Set<HintType> hintTypes;

    public static Builder  newBuilder(){
        return new Config().new Builder();
    }

    public class Builder {
        public Builder id(Integer id){
            Config.this.id = id;
            return this;
        }

        public Builder timeToReply(Integer timeToReply){
            Config.this.timeToReply = timeToReply;
            return this;
        }

        public Builder questionsAmount(Integer questionsAmount){
            Config.this.questionsAmount = questionsAmount;
            return this;
        }

        public Builder userId(Integer userId){
            Config.this.userId = userId;
            return this;
        }

        public Builder hintTypes(Collection<HintType> hintTypes){
            Config.this.hintTypes = new HashSet<>(hintTypes);
            return this;
        }

        public Config build(){
            return Config.this;
        }
    }

    public int getTimeToReply() {
        return timeToReply;
    }

    public void setTimeToReply(int timeToReply) {
        this.timeToReply = timeToReply;
    }

    public int getQuestionsAmount() {
        return questionsAmount;
    }

    public void setQuestionsAmount(int questionsAmount) {
        this.questionsAmount = questionsAmount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Set<HintType> getHintTypes() {
        return hintTypes;
    }

    public void setHintTypes(Set<HintType> hintTypes) {
        this.hintTypes = hintTypes;
    }
}
