package model.database.dao.concrete.mysql;

import model.database.exception.RepositoryException;
import model.entity.Config;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLConfigDAOTest {
    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private Integer id = 5;
    private MySQLConfigDAO mySQLConfigDAO;

    @Before
    public void setUp() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);

        mySQLConfigDAO = new MySQLConfigDAO(connection);
    }


    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLConfigDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLConfigDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLConfigDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLConfigDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLConfigDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLConfigDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLConfigDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLConfigDAO.getInsertQuery().isEmpty());
    }


    @Test
    public void testGetLastUserConfiguration() throws SQLException{
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.getInt("user_id")).thenReturn(id);

        Optional<Config> configOptional = mySQLConfigDAO.getLastUserConfiguration(id);
        assertTrue(configOptional.isPresent());
        Config config = configOptional.get();
        assertEquals(id, (Integer) config.getUserId());

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeQuery();
        verify(preparedStatement, atLeast(1)).setObject(anyInt(), eq(id), eq(Types.INTEGER));
        verify(resultSet, times(2)).next();
        verify(preparedStatement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testGetLastUserConfigurationException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        mySQLConfigDAO.getLastUserConfiguration(id);
    }
}
