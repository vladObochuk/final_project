package model.database.dao.interfaces;

import model.database.dao.DAO;
import model.entity.HintType;

/**
 * Provides additional methods for accessing HintType objects in database
 */
public interface HintTypeDAO extends DAO<HintType> {
}
