package model.database.dao.interfaces;

import model.database.dao.DAO;
import model.entity.Answer;


/**
 * Provides additional methods for accessing Answer objects in database
 */
public interface AnswerDAO extends DAO<Answer> {
}
