package controller.utils;

import exception.ApplicationException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Provides methods to enable Cross Site Forgery security
 */
public class CSRFSecurity {

    /**
     * Generates one token per session(Only if it present security can work)
     *
     * @param session not {@code null}
     */
    public static void generateToken(HttpSession session) {
        if (session.getAttribute("csrfToken") == null) {
            try {
                SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
                byte[] randomBytes = new byte[15];
                secureRandom.nextBytes(randomBytes);
                String token = Base64.getEncoder().encodeToString(randomBytes);
                session.setAttribute("csrfToken", token);
            } catch (NoSuchAlgorithmException e) {
                throw new ApplicationException(e);
            }
        }
    }

    /**
     * Checks if request have Cross Site Forgery attack signs
     *
     * @param request not {@code null}
     * @param method  not {@code null} equals to request method
     * @return {@code true} if request is safe, otherwise {@code false}
     */
    public static boolean isSafe(HttpServletRequest request, HttpMethod method) {
        if (method != HttpMethod.GET) {
            HttpSession session = request.getSession();

            String storedToken = (String) session.getAttribute("csrfToken");
            if (storedToken == null)
                return true;
            String token = request.getParameter("csrfToken");

            return (storedToken.equals(token));
        }
        return true;
    }
}
