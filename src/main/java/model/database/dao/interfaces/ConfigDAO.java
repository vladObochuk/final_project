package model.database.dao.interfaces;

import model.database.dao.DAO;
import model.entity.Config;

import java.util.Optional;

/**
 * Provides additional methods for accessing Config objects in database
 */
public interface ConfigDAO extends DAO<Config> {

    /**
     * Retrieve configuration with biggest id
     * @param userId not {@code null} user Id which configuration need to seek
     * @return optional of configuration with biggest id. Optional is empty when configuration doesn't exist
     */
    Optional<Config> getLastUserConfiguration(int userId);
}
