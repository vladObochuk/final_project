package controller.filter;

import constants.Alert;
import model.database.dao.factory.DAOFactory;
import model.database.exception.RepositoryException;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Handle the transaction - one transaction per request
 */
public class TransactionFilter implements Filter {
    private static Logger logger = Logger.getLogger(TransactionFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // NOP
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        DAOFactory factory = (DAOFactory) request.getAttribute("model.database.dao.factory.DAOFactory");
        logger.trace("DAOFactory retrieved from request");
        if (factory != null) {
            Connection connection = factory.getConnection();
            logger.trace("Connection retrieved from DAOFactory");

            try {
                connection.setAutoCommit(false);
                logger.trace("Transaction started");

                chain.doFilter(request, response);
                logger.trace("Service finished");

                HttpSession session = httpServletRequest.getSession(false);
                if (session != null && session.getAttribute(Alert.ERROR) != null) {
                    logger.trace("Transaction rollback");
                    DbUtils.rollback(connection);
                }

                connection.setAutoCommit(true); // explicitly commit transaction
                logger.trace("Set autocommit to true");
                DbUtils.close(connection);
            } catch (SQLException e) {
                logger.error("Connection operations failed", e);
                throw new RepositoryException(e);
            } catch (Exception e) {
                logger.fatal("Exception after processing the request", e);
                DbUtils.rollbackAndCloseQuietly(connection);
                throw e;
            }
        }
    }

    @Override
    public void destroy() {
        // NOP
    }
}
