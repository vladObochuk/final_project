package controller.command.impl.moderator;

import constants.PagePath;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;
import controller.command.manager.impl.moderator.ModeratorAction;
import controller.utils.CommandUtils;
import model.entity.Question;
import service.QuestionService;
import service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListQuestionsCommand extends AbstractCommand {

    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        QuestionService questionService = serviceFactory.createQuestionService();
        List<Question> questions = questionService.getNotModeratedQuestions();
        request.setAttribute("questions", questions);
        request.setAttribute("detailURL", "/moderator.do");
        request.setAttribute("detailAction", ModeratorAction.MODERATE_QUESTION);
        return PagePath.LIST_QUESTIONS;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required listQuestions with POST method");
    }
}
