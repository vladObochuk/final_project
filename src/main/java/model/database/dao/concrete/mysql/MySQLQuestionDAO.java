package model.database.dao.concrete.mysql;

import model.database.dao.AbstractDAO;
import model.database.dao.interfaces.QuestionDAO;
import model.database.exception.RepositoryException;
import model.entity.Question;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySQLQuestionDAO extends AbstractDAO<Question> implements QuestionDAO {
    private static Logger logger = Logger.getLogger(MySQLQuestionDAO.class);
    private final static String TABLE_NAME = "questions";

    public MySQLQuestionDAO(Connection connection) {
        super(connection);
    }

    /**
     * Provides sql queries to database
     */
    private interface Queries {
        String SELECT = "SELECT * FROM " + TABLE_NAME;
        String INSERT = "INSERT INTO " + TABLE_NAME + " (text, hint, lang, right_answer_id, user_id, approved) VALUES (?, ?, ?, ?, ?, ?)";
        String MERGE = "INSERT INTO " + TABLE_NAME + " (id, text, hint, lang, right_answer_id, user_id approved) VALUES (?, ?, ?, ?, ?, ?, ?)" +
                "ON DUPLICATE KEY UPDATE text = ?, hint = ?, lang = ?, right_answer_id = ?, user_id = ?, approved = ?";
        String SELECT_QUESTIONS_NULL_APPROVED = SELECT + " WHERE approved IS NULL";
        String SELECT_USER_QUESTIONS = SELECT + " WHERE user_id = ?";
        String SELECT_RANDOM_QUESTIONS = SELECT + " WHERE approved = ? AND lang = ? AND user_id <> ? ORDER BY RAND() LIMIT ?";
    }

    @Override
    protected List<Question> mapEntities(ResultSet resultSet) throws SQLException {
        List<Question> questions = new ArrayList<>();
        while (resultSet.next()) {
            Question question = new Question();
            question.setId(resultSet.getInt("id"));
            question.setText(resultSet.getString("text"));
            question.setHint(resultSet.getString("hint"));
            question.setLanguage(resultSet.getString("lang"));
            question.setRightAnswerId(resultSet.getInt("right_answer_id"));
            question.setUserId(resultSet.getInt("user_id"));
            question.setApproved(resultSet.getBoolean("approved"));
            if (resultSet.wasNull())
                question.setApproved(null);
            questions.add(question);
        }
        return questions;
    }

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getSelectQuery() {
        return Queries.SELECT;
    }


    @Override
    protected String getMergeQuery() {
        return Queries.MERGE;
    }

    @Override
    protected void prepareStatementForMerge(PreparedStatement statement, Question elem) throws SQLException {
        statement.setInt(1, elem.getId());
        statement.setString(2, elem.getText());
        statement.setString(3, elem.getHint());
        statement.setString(4, elem.getLanguage());
        statement.setObject(5, elem.getRightAnswerId(), Types.INTEGER);
        statement.setObject(6, elem.getUserId(), Types.INTEGER);
        statement.setObject(7, elem.getApproved(), Types.BOOLEAN);

        statement.setString(8, elem.getText());
        statement.setString(9, elem.getHint());
        statement.setString(10, elem.getLanguage());
        statement.setObject(11, elem.getRightAnswerId(), Types.INTEGER);
        statement.setObject(12, elem.getUserId(), Types.INTEGER);
        statement.setObject(13, elem.getApproved(), Types.BOOLEAN);
    }

    @Override
    protected String getInsertQuery() {
        return Queries.INSERT;
    }

    @Override
    protected void prepareStatementForInsert(PreparedStatement statement, Question elem) throws SQLException {
        statement.setString(1, elem.getText());
        statement.setString(2, elem.getHint());
        statement.setString(3, elem.getLanguage());
        statement.setObject(4, elem.getRightAnswerId(), Types.INTEGER);
        statement.setObject(5, elem.getUserId(), Types.INTEGER);
        statement.setObject(6, elem.getApproved(), Types.BOOLEAN);
    }

    @Override
    public List<Question> getUserQuestions(Integer userId) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.SELECT_USER_QUESTIONS)) {
            statement.setObject(1, userId);
            ResultSet resultSet = statement.executeQuery();
            return mapEntities(resultSet);
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while retrieving questions of user with id=" + userId;
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<Question> getQuestionsNullApproved() {
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(Queries.SELECT_QUESTIONS_NULL_APPROVED);
            return mapEntities(resultSet);
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while retrieving questions with approved=NULL";
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }

    @Override
    public List<Question> getRandomQuestionsWith(boolean isApproved, String lang, int amount, Integer userId) {
        try (PreparedStatement statement = connection.prepareStatement(Queries.SELECT_RANDOM_QUESTIONS)) {
            statement.setObject(1, isApproved, Types.BOOLEAN);
            statement.setString(2, lang);
            statement.setObject(3, userId, Types.INTEGER);
            statement.setObject(4, amount, Types.INTEGER);
            ResultSet resultSet = statement.executeQuery();
            return mapEntities(resultSet);
        } catch (SQLException e) {
            final String LOG_MESSAGE = "Error occurred while retrieving random questions";
            logger.error(LOG_MESSAGE, e);
            throw new RepositoryException(e);
        }
    }
}
