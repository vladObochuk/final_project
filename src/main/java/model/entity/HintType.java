package model.entity;

import model.database.DomainObject;

public class HintType extends DomainObject {
    private String name;

    public HintType(){}

    public HintType(Integer id) {
        super(id);
    }

    public HintType(Integer id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
