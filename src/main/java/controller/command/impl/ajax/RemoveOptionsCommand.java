package controller.command.impl.ajax;

import constants.PagePath;
import controller.command.Command;
import controller.utils.CommandUtils;
import controller.utils.HttpMethod;
import model.entity.AnswerOption;
import model.entity.Question;
import service.QuestionService;
import service.factory.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class RemoveOptionsCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws ServletException {
        HttpSession session = request.getSession();
        List<AnswerOption> questionAnswers = (List<AnswerOption>) session.getAttribute("questionAnswers");
        if (questionAnswers.size() <= 2){
            return "";
        }
        Question currentQuestion = (Question) session.getAttribute("currentQuestion");
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        QuestionService questionService = serviceFactory.createQuestionService();
        questionAnswers = questionService.getCorrectAndRandomOption(questionAnswers, currentQuestion);
        session.setAttribute("questionAnswers", questionAnswers);

        return PagePath.REMOVE_OPTIONS;
    }
}
