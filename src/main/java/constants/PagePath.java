package constants;

/**
 * Paths to jsp files
 */
public interface PagePath {
    String PREFIX = "/WEB-INF/pages/";
    String SUFFIX = ".jsp";

    String WELCOME = PREFIX + "index" + SUFFIX;
    String REGISTER = PREFIX + "user/register" + SUFFIX;
    String LIST_QUESTIONS = PREFIX + "question/listQuestions" + SUFFIX;
    String ADD_QUESTION = PREFIX + "question/addQuestion" + SUFFIX;
    String QUESTION_DETAILS = PREFIX + "question/questionDetails" + SUFFIX;
    String CHANGE_DATA = PREFIX + "user/changeData" + SUFFIX;
    String CONFIG = PREFIX + "game/config" + SUFFIX;
    String START_GAME = PREFIX + "game/startgame" + SUFFIX;
    String GAME = PREFIX + "game/game" + SUFFIX;
    String MODERATOR_LIST = PREFIX + "user/moderatorList" + SUFFIX;
    String QUESTION_CONFIRMATION = PREFIX + "question/questionConfirmation" + SUFFIX;
    String EDIT_USER = PREFIX + "user/modifyUser" + SUFFIX;
    String GAME_RESULTS_PAGE = PREFIX + "game/results" + SUFFIX;
    String EMPTY = PREFIX + "ajax/empty" + SUFFIX;
    String QUESTION_HINT = PREFIX + "ajax/questionHint" + SUFFIX;
    String ANSWER_PROBABILITY = PREFIX + "ajax/answerProbability" + SUFFIX;
    String REMOVE_OPTIONS = PREFIX + "ajax/50to50" + SUFFIX;
}
