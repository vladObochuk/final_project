package service.impl;

import model.entity.Config;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import service.TimeService;

import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class TestTimeServiceImpl {

    @Mock
    HttpSession httpSession;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private TimeService timeService;

    @Before
    public void setUp() throws Exception {
        timeService = new TimeServiceImpl();
    }

    private Config getConfigDummy(){
        return Config.newBuilder()
                .timeToReply(20)
                .build();
    }

    @Test
    public void testSetupTimeForAnswer(){
        Config config = getConfigDummy();

        timeService.setupTimeForAnswer(httpSession, config);

        verify(httpSession, times(1)).setAttribute(eq("startTime"), anyObject());
        verify(httpSession, times(1)).setAttribute(eq("endTime"), anyObject());
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        verify(httpSession, times(1)).setAttribute(eq("timeLeft"), argumentCaptor.capture());

        assertEquals(config.getTimeToReply(), argumentCaptor.getValue(), 1);
    }

    @Test
    public void testAddTime(){
        Date endTime = new Date();
        when(httpSession.getAttribute("endTime")).thenReturn(endTime);
        timeService.addTime(httpSession);

        verify(httpSession, times(1)).getAttribute("endTime");
        verify(httpSession, times(1)).setAttribute(eq("endTime"), anyObject());
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        verify(httpSession, times(1)).setAttribute(eq("timeLeft"), argumentCaptor.capture());
        assertEquals(30, argumentCaptor.getValue(), 1);
    }

    @Test
    public void testRemoveSessionAttributes(){
        timeService.removeSessionAttributes(httpSession);

        verify(httpSession, atLeast(3)).removeAttribute(anyString());
    }

    @Test
    public void testTimePassed(){
        Calendar c = Calendar.getInstance();
        c.set(1990, 1, 25);
        Date date = c.getTime();
        when(httpSession.getAttribute("endTime")).thenReturn(date);

        boolean result = timeService.isTimeToAnswerPassed(httpSession);
        assertTrue(result);

        verify(httpSession, times(1)).getAttribute("endTime");
    }

    @Test
    public void testTimeReminded(){
        Calendar c = Calendar.getInstance();
        c.set(2155, 1, 25);
        Date date = c.getTime();
        when(httpSession.getAttribute("endTime")).thenReturn(date);

        boolean result = timeService.isTimeToAnswerPassed(httpSession);
        assertFalse(result);

        verify(httpSession, times(1)).getAttribute("endTime");
    }

}
