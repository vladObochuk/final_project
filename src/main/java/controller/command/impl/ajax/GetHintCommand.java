package controller.command.impl.ajax;

import constants.PagePath;
import controller.command.Command;
import controller.utils.HttpMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetHintCommand implements Command {
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws ServletException {
        return PagePath.QUESTION_HINT;
    }
}
