package constants;

import controller.command.manager.impl.admin.AdminAction;
import controller.command.manager.impl.moderator.ModeratorAction;
import controller.command.manager.impl.unauthorized.UnauthorizedAction;
import controller.command.manager.impl.user.UserAction;

public interface RedirectLink {
    String USER_CHANGE_DATA = "/user.do?action=" + UserAction.EDIT_DATA;
    String USER_LIST_QUESTIONS = "/user.do?action=" + UserAction.VIEW_ALL_QUESTIONS;
    String USER_ADD_QUESTION = "/user.do?action=" + UserAction.ADD_QUESTION;
    String MODERATOR_LIST_QUESTIONS = "/moderator.do?action=" + ModeratorAction.VIEW_QUESTIONS_TO_MODERATE;
    String ADMIN_LIST_MODERATORS = "/admin.do?action=" + AdminAction.VIEW_ALL_MODERATORS;
    String ADMIN_ADD_MODERATOR = "/admin.do?action=" + AdminAction.ADD_MODERATOR;
    String REGISTER_PAGE = "/basic.do?action=" + UnauthorizedAction.REGISTER;
    String LOGIN_PAGE = "/user.do";
    String CONFIG_PAGE = "/user.do?action=" + UserAction.CONFIGURE_GAME;
    String GAME = "/user.do?action=" + UserAction.GAME;
    String RESULTS = "/user.do?action=" + UserAction.SHOW_RESULTS;
}
