package controller.filter;

import controller.utils.CSRFSecurity;
import model.entity.User;
import org.apache.log4j.Logger;
import service.UserService;
import service.factory.ServiceFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Initialize User object, CSRF security in session
 * Must be started after service initialization
 */
public class SessionInitializerFilter implements Filter {
    private static Logger logger = Logger.getLogger(SessionInitializerFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // NOP
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpSession session = httpServletRequest.getSession();
        if (httpServletRequest.getUserPrincipal() != null) {
            User user = (User) session.getAttribute("model.entity.User");

            if (user == null) {
                logger.trace("User logged, but session doesn't have user attribute");
                ServiceFactory serviceFactory = (ServiceFactory) request.getAttribute("service.factory.ServiceFactory");
                UserService userService = serviceFactory.createUserService();
                String username = httpServletRequest.getUserPrincipal().getName();
                user = userService.getUser(username);
                logger.trace("User retrieved from db");
                session.setAttribute("model.entity.User", user);
                logger.trace("User attribute set to the session");
                session.setAttribute("pageSize", 10);
                CSRFSecurity.generateToken(session);
                logger.trace("CSRF token set to the session");
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // NOP
    }
}
