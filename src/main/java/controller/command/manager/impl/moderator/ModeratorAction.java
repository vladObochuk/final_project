package controller.command.manager.impl.moderator;

/**
 * Provides names of actions proper for moderator
 */
public interface ModeratorAction {
    String VIEW_QUESTIONS_TO_MODERATE = "listQuestions";
    String MODERATE_QUESTION = "moderateQuestion";
}
