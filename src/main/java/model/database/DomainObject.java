package model.database;

import java.io.Serializable;

/**
 * Basic parent of all entities, represents object from model.database side
 */
public abstract class DomainObject implements Serializable {
    protected Integer id;

    public DomainObject() {
    }

    public DomainObject(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isTransient() {
        return id == null;
    }

    public boolean isPersistent() {
        return !isTransient();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainObject that = (DomainObject) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "model.database.DomainObject{" +
                "id=" + id +
                '}';
    }
}
