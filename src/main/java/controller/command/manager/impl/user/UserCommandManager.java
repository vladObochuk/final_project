package controller.command.manager.impl.user;

import controller.command.impl.ajax.GetAdditionalTimeCommand;
import controller.command.impl.ajax.GetHintCommand;
import controller.command.impl.ajax.GetProbabilityCommand;
import controller.command.impl.ajax.RemoveOptionsCommand;
import controller.command.impl.user.*;
import controller.command.manager.CommandManager;

public class UserCommandManager extends CommandManager {

    private UserCommandManager(){
        commandMap.put(UserAction.VIEW_QUESTION_DETAILS, new ShowQuestionDetailsCommand());
        commandMap.put(UserAction.VIEW_ALL_QUESTIONS, new ListQuestionsCommand());
        commandMap.put(UserAction.EDIT_DATA, new ChangeUserdataCommand());
        commandMap.put(UserAction.CHANGE_PASSWORD, new ChangePasswordCommand());
        commandMap.put(UserAction.LOG_OUT, new SignOutCommand());
        commandMap.put(UserAction.ADD_QUESTION, new AddQuestionCommand());
        commandMap.put(UserAction.CONFIGURE_GAME, new ConfigureGameCommand());
        commandMap.put(UserAction.START_GAME, new StartGameCommand());
        commandMap.put(UserAction.GAME, new GameCommand());
        commandMap.put(UserAction.SHOW_RESULTS, new ShowGameResultsCommand());
        commandMap.put(UserAction.GET_ADDITIONAL_TIME, new GetAdditionalTimeCommand());
        commandMap.put(UserAction.REMOVE_OPTIONS, new RemoveOptionsCommand());
        commandMap.put(UserAction.GET_PROBABILITY, new GetProbabilityCommand());
        commandMap.put(UserAction.GET_HINT, new GetHintCommand());
    }

    private static class Holder{
        static final UserCommandManager INSTANCE = new UserCommandManager();
    }

    public static UserCommandManager getInstance(){
        return Holder.INSTANCE;
    }
}
