<%@ include file="../jspf/header.jspf" %>

<c:url value="/user.do" var="getTimeURL">
    <c:param name="action" value="getTime"/>
</c:url>

<c:url value="/user.do" var="getProbabilityURL">
    <c:param name="action" value="getProbability"/>
</c:url>

<c:url value="/user.do" var="deleteOptionsURL">
    <c:param name="action" value="deleteOptions"/>
</c:url>

<c:url value="/user.do" var="showHintURL">
    <c:param name="action" value="showHint"/>
</c:url>

<h2 class="text-center">
    <fmt:message key="question"/> ${sessionScope.questionNumber}/${sessionScope.config.questionsAmount}
</h2>

<div class="float-right">
    <table class="table table-hover text-center">
        <thead>
        <tr>
            <th>
                <fmt:message key="you"/>
            </th>
            <th>
                <fmt:message key="opponents"/>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th><span class="badge badge-primary">${playerPoints}</span></th>
            <th><span class="badge badge-danger"> ${opponentPoints} </span></th>
        </tr>
        </tbody>
    </table>
    <div id="time">
    </div>
    <div class="progress">
        <div id="time-progress" class="progress-bar progress-bar-animated" role="progressbar"></div>
    </div>

</div>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col col-10">
                <h5>
                    ${sessionScope.questionNumber}. <c:out value="${sessionScope.currentQuestion.text}"/>
                </h5>
            </div>
            <div class="col col-2 text-right">
                <fmt:message key="question.by"/> <c:out value="${questionCreator.username}"/>
            </div>
        </div>
    </div>


    <div class="card-body">
        <csrf:form action="" method="post">
            <c:choose>
                <c:when test="${fn:length(sessionScope.questionAnswers) eq 1}">
                    <div class="form-group">
                        <label for="answer">
                            <fmt:message key="answer.write"/>
                        </label>
                        <input type="text" name="answer" class="form-control" id="answer">
                    </div>
                </c:when>

                <c:otherwise>
                    <ul id="answerOptions" class="list-group list-group-flush">
                        <c:forEach items="${sessionScope.questionAnswers}" var="questionAnswer" varStatus="loop">
                            <li class="list-group-item">
                                <div class="form-check">
                                    <input id="radio${loop.index}" class="form-check-input" name="chosenAnswerId"
                                           type="radio"
                                           value="${questionAnswer.id}">
                                    <label for="radio${loop.index}" class="form-check-label">
                                        <c:out value="${questionAnswer.text}"/>
                                    </label>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>

            <br>
            <div id="hintText"></div>
            <c:if test="${not empty config.hintTypes}">

                <div class="dropdown float-right">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="hintsDropDown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <fmt:message key="help.get"/>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="hintsDropDown">
                        <c:forEach items="${config.hintTypes}" var="hintType">
                            <a id="<c:out value="${hintType.name}"/>" class="dropdown-item" href="#">
                                <fmtdt:message key="${hintType.name}" defaultVal=""/>
                            </a>
                        </c:forEach>
                    </div>
                </div>
            </c:if>


            <button type="submit" class="btn btn-lg btn-primary">
                <fmt:message key="answer"/>
            </button>
        </csrf:form>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        startTimer(${sessionScope.config.timeToReply}, ${sessionScope.timeLeft}, 'time', 'time-progress');
        $('#hint\\.probability').click(function (data) {
            $.ajax({
                url: "${getProbabilityURL}",
                success: function (data) {
                    $('#answerOptions').html(data);
                }
            })
        });
        $('#hint\\.show').click(function () {
            $.ajax({
                url: "${showHintURL}",
                success: function (data) {
                    $('#hintText').html(data);
                }
            })
        });
        $('#hint\\.deleteOptions').click(function () {
            $.ajax({
                url: "${deleteOptionsURL}",
                success: function (data) {
                    $('#answerOptions').html(data);
                }
            })
        });
        $('#hint\\.time').click(function () {
            $.ajax({
                url: "${getTimeURL}",
                success: function () {
                    location.reload();
                }
            })
        })


    });
    function startTimer(start_duration, current_duration, display, progress) {
        var timer = current_duration, minutes, seconds;
        var interval = setInterval(function () {
            if (timer < 0) {
                minutes = 0;
                seconds = 0;
            } else {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);
            }

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            if (--timer < 0) {
                clearInterval(interval);
            }

            document.getElementById(display).innerHTML = minutes + ":" + seconds;
            var process = Math.ceil(timer / start_duration * 100);
            document.getElementById(progress).style.width = process + "%";
        }, 1000);
    }
</script>

<%@ include file="../jspf/footer.jsp" %>