package model.database.dao.concrete.mysql;

import model.entity.HintType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLHintTypeDAOTest {
    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MySQLHintTypeDAO mySQLHintTypeDAO;

    private HintType source = new HintType(88, "Selected");

    @Before
    public void setUp() throws SQLException {
        mySQLHintTypeDAO = new MySQLHintTypeDAO(connection);
    }

    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLHintTypeDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLHintTypeDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLHintTypeDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLHintTypeDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLHintTypeDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLHintTypeDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLHintTypeDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLHintTypeDAO.getInsertQuery().isEmpty());
    }

    @Test
    public void testMapEntities() throws SQLException{
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("id")).thenReturn(source.getId());
        when(resultSet.getString("name")).thenReturn(source.getName());

        List<HintType> hintTypes = mySQLHintTypeDAO.mapEntities(resultSet);

        assertNotNull(hintTypes);
        assertEquals(2, hintTypes.size());
        for (HintType hintType : hintTypes) {
            assertEquals(source.getName(), hintType.getName());
            assertEquals(source.getId(), hintType.getId());
        }

        verify(resultSet, times(3)).next();
        verify(resultSet, times(2)).getInt(anyString());
        verify(resultSet, times(2)).getString(anyString());
    }

    @Test
    public void testPrepareStatementForMerge() throws SQLException{
        mySQLHintTypeDAO.prepareStatementForMerge(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setInt(anyInt(), eq(source.getId()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getName()));
    }

    @Test
    public void testPrepareStatementForInsert() throws SQLException{

        mySQLHintTypeDAO.prepareStatementForInsert(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getName()));
    }
}
