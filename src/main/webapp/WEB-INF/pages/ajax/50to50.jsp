<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:forEach items="${sessionScope.questionAnswers}" var="questionAnswer" varStatus="loop">
    <li class="list-group-item">
        <div class="form-check">
            <input id="radio${loop.index}" class="form-check-input" name="chosenAnswerId"
                   type="radio"
                   value="${questionAnswer.id}">
            <label for="radio${loop.index}" class="form-check-label">
                <c:out value="${questionAnswer.text}"/>
            </label>
        </div>
    </li>
</c:forEach>
