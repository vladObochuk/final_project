<%@ include file="../../jspf/header.jspf" %>

<div class="alert alert-danger text-center">
    <strong><fmt:message key="login.error.message"/></strong>
</div>

<form action="j_security_check" method="post" name="loginForm" class="form-small">
    <div class="form-group">
        <label for="user">
            <fmt:message key="username.label"/>
        </label>
        <input id="user" type="text" name="j_username" size="30" class="form-control"
               placeholder="<fmt:message key="username.placeholder"/>" required>
    </div>


    <div class="form-group">
        <label for="password">
            <fmt:message key="password.label"/>
        </label>
        <input id="password" type="password" name="j_password" size="30" class="form-control"
               placeholder="<fmt:message key="password.placeholder"/>" required>
    </div>

    <button type="submit" class="btn btn-lg btn-primary btn-block">
        <fmt:message key="button.login"/>
    </button>
</form>

<%@ include file="../../jspf/footer.jsp" %>