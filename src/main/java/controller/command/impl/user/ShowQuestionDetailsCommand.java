package controller.command.impl.user;

import constants.Alert;
import constants.AlertMessages;
import constants.PagePath;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;
import controller.utils.CommandUtils;
import exception.NullRequestParameterException;
import model.entity.AnswerOption;
import model.entity.Question;
import service.QuestionService;
import service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class ShowQuestionDetailsCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        QuestionService questionService = serviceFactory.createQuestionService();

        try {
            Integer id = CommandUtils.retrieveIntegerValue(request, "questionId");
            Question question = questionService.getQuestionById(id);
            List<AnswerOption> answerOptions = questionService.getQuestionAnswers(question.getId());

            request.setAttribute("question", question);
            request.setAttribute("answerOptions", answerOptions);
        } catch (NumberFormatException | NullRequestParameterException e){
            HttpSession session = request.getSession();
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_GET_PARAMETER);
        }
        return PagePath.QUESTION_DETAILS;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Requested showQuestionDetails with POST method");
    }
}
