package service;

import model.entity.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface GameService {
    List<Integer> TIME_OPTIONS = Stream.iterate(30, i -> i + 15)
            .limit(8)
            .collect(Collectors.toList());

    List<Integer> QUESTION_AMOUNT_OPTIONS = Stream.iterate(4, i -> i + 1)
            .limit(5)
            .collect(Collectors.toList());

    Config DEFAULT_CONFIG = Config.newBuilder().timeToReply(60).questionsAmount(5).build();

    /**
     * Retrieve all entities of Hint type from db
     *
     * @return {@code List} of hints
     */
    List<HintType> getAllAvailableHints();

    /**
     * Retrieve last configuration or if it is not exist returns default
     *
     * @param user user which configuration retrieve
     * @return not {@code null} entity of Config type
     */
    Config getLastConfigurationOrDefault(User user);

    /**
     * Save Configuration
     *
     * @param config not {@code null}
     */
    void addConfiguration(Config config);

    /**
     * Save answer
     *
     * @param answer not {@code null}
     */
    void addAnswer(Answer answer);

    /**
     * Save game
     *
     * @param game not {@code null}
     */
    void addGame(Game game);
}
