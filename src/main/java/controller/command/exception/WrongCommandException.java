package controller.command.exception;

import exception.ApplicationException;

public class WrongCommandException extends ApplicationException {
    public WrongCommandException(String message) {
        super(message);
    }
}
