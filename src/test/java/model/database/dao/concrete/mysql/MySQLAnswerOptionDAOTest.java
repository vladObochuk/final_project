package model.database.dao.concrete.mysql;

import model.database.exception.RepositoryException;
import model.entity.AnswerOption;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLAnswerOptionDAOTest {
    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MySQLAnswerOptionDAO mySQLAnswerOptionDAO;

    private AnswerOption source;

    @Before
    public void setUp() throws SQLException {
        mySQLAnswerOptionDAO = new MySQLAnswerOptionDAO(connection);
        source = new AnswerOption();
        source.setId(88);
        source.setQuestionId(5);
        source.setText("hello world");
    }

    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLAnswerOptionDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLAnswerOptionDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLAnswerOptionDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLAnswerOptionDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLAnswerOptionDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLAnswerOptionDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLAnswerOptionDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLAnswerOptionDAO.getInsertQuery().isEmpty());
    }

    @Test
    public void testMapEntities() throws SQLException{
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("id")).thenReturn(source.getId());
        when(resultSet.getString("text")).thenReturn(source.getText());
        when(resultSet.getInt("question_id")).thenReturn(source.getQuestionId());

        List<AnswerOption> answerOptions = mySQLAnswerOptionDAO.mapEntities(resultSet);

        assertNotNull(answerOptions);
        assertEquals(2, answerOptions.size());
        for (AnswerOption answerOption : answerOptions) {
            assertEquals(source.getQuestionId(), answerOption.getQuestionId());
            assertEquals(source.getText(), answerOption.getText());
            assertEquals(source.getId(), answerOption.getId());
        }

        verify(resultSet, times(3)).next();
        verify(resultSet, times(4)).getInt(anyString());
        verify(resultSet, times(2)).getString(anyString());
    }

    @Test
    public void testPrepareStatementForMerge() throws SQLException{

        mySQLAnswerOptionDAO.prepareStatementForMerge(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setInt(anyInt(), eq(source.getId()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getText()));
        verify(preparedStatement, atLeast(2))
                .setObject(anyInt(), eq(source.getQuestionId()), eq(Types.INTEGER));
    }

    @Test
    public void testPrepareStatementForInsert() throws SQLException{
        mySQLAnswerOptionDAO.prepareStatementForInsert(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getText()));
        verify(preparedStatement, atLeast(1))
                .setObject(anyInt(), eq(source.getQuestionId()), eq(Types.INTEGER));
    }

    @Test
    public void testGetQuestionAnswers() throws SQLException{
        Integer id = 185;
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("question_id")).thenReturn(id);

        List<AnswerOption> answerOptions = mySQLAnswerOptionDAO.getQuestionAnswers(id);
        assertEquals(1, answerOptions.size());
        assertEquals(id, answerOptions.get(0).getQuestionId());

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeQuery();
        verify(preparedStatement, atLeast(1)).setObject(anyInt(), eq(id));
        verify(resultSet, times(2)).next();
        verify(preparedStatement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testQuestionAnswersException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        mySQLAnswerOptionDAO.getQuestionAnswers(185);
    }
}
