package service.factory;

import model.database.dao.factory.DAOFactory;
import service.GameService;
import service.QuestionService;
import service.TimeService;
import service.UserService;
import service.impl.GameServiceImpl;
import service.impl.QuestionServiceImpl;
import service.impl.TimeServiceImpl;
import service.impl.UserServiceImpl;

public class ServiceFactoryImpl implements ServiceFactory {
    private final DAOFactory DAO_FACTORY;

    public ServiceFactoryImpl(DAOFactory DAO_FACTORY) {
        this.DAO_FACTORY = DAO_FACTORY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GameService createGameService() {
        return new GameServiceImpl(DAO_FACTORY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserService createUserService() {
        return new UserServiceImpl(DAO_FACTORY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QuestionService createQuestionService() {
        return new QuestionServiceImpl(DAO_FACTORY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeService createTimeService() {
        return new TimeServiceImpl();
    }
}
