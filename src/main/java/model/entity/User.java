package model.entity;

import constants.ValidatorMessages;
import model.database.DomainObject;
import validation.annotation.MaxLength;
import validation.annotation.MinLength;
import validation.annotation.NotNull;
import validation.annotation.Regex;

public class User extends DomainObject {
    @NotNull(ValidatorMessages.USERNAME_NULL)
    @MinLength(value = 3, message = ValidatorMessages.USERNAME_SMALL)
    @MaxLength(value = 30, message = ValidatorMessages.USERNAME_BIG)
    @Regex(value = "^(\\p{L}|\\p{N}|_)+$", message = ValidatorMessages.USERNAME_WRONG)
    private String username;

    @NotNull(ValidatorMessages.PASSWORD_NULL)
    @MinLength(value = 8, message = ValidatorMessages.PASSWORD_SMALL)
    private String password;

    private Role role;

    public User(){}

    public User(String username, String password, Role role){
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public enum Role {
        ADMIN, ORDINARY, MODERATOR
    }

    public static Builder newBuilder(){
        return new User().new Builder();
    }

    public class Builder {
        public Builder id(Integer id){
            User.this.id = id;
            return this;
        }

        public Builder username(String username){
            User.this.username = username;
            return this;
        }

        public Builder password(String password){
            User.this.password = password;
            return this;
        }

        public Builder role (String role){
            User.this.setRole(role);
            return this;
        }

        public User build(){
            return User.this;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role.name();
    }

    public void setRole(String role) {
        this.role = Role.valueOf(role);
    }
}