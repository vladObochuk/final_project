package controller.command.manager.factory;

public enum CommandManagerType {
    ADMIN, MODERATOR, USER, UNAUTHORIZED
}
