package controller.command;

import controller.utils.HttpMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Provides method for processing servlet request
 */
@FunctionalInterface
public interface Command {
    String process(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws ServletException;
}
