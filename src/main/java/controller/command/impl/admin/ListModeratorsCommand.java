package controller.command.impl.admin;

import constants.PagePath;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;
import controller.utils.CommandUtils;
import model.entity.User;
import service.UserService;
import service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListModeratorsCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        UserService userService = serviceFactory.createUserService();
        List<User> moderators = userService.getUsersWithRole(User.Role.MODERATOR);
        request.setAttribute("moderators", moderators);
        return PagePath.MODERATOR_LIST;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required listModerators with POST method");
    }
}
