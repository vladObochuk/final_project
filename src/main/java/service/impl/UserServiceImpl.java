package service.impl;

import exception.ApplicationException;
import exception.WrongUserCredentialException;
import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.UserDAO;
import model.entity.User;
import org.apache.catalina.CredentialHandler;
import org.apache.log4j.Logger;
import service.UserService;

import java.util.List;
import java.util.Optional;

public class UserServiceImpl implements UserService {
    private static Logger logger = Logger.getLogger(UserServiceImpl.class);

    private final DAOFactory DAO_FACTORY;

    public UserServiceImpl(DAOFactory DAO_FACTORY) {
        this.DAO_FACTORY = DAO_FACTORY;
    }

    public User getUser(String username) {
        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        Optional<User> user = userDAO.get(username);
        return user.orElseThrow(() -> new ApplicationException("Trying get user with wrong username = " + username));
    }

    @Override
    public User getUser(Integer id) {
        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        Optional<User> user = userDAO.getById(id);
        return user.orElseThrow(() -> new ApplicationException("Trying get user with wrong id = " + id));
    }

    @Override
    public List<User> getUsersWithRole(User.Role role) {
        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        return userDAO.getUsersWithRole(role);
    }

    public void registerUser(User user, CredentialHandler handler) {
        logger.debug("registerUser method - start");
        user.setPassword(handler.mutate(user.getPassword()));
        logger.trace("Password successfully hashed");

        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        userDAO.insert(user);
        logger.trace("User inserted into model.database");
        logger.debug("registerUser method - finished");
    }

    @Override
    public void changePassword(User user, String oldPassword, String newPassword, CredentialHandler credentialHandler) {
        if (credentialHandler.matches(oldPassword, user.getPassword())) {
            user.setPassword(credentialHandler.mutate(newPassword));
            UserDAO userDAO = DAO_FACTORY.createUserDAO();
            userDAO.merge(user);
        } else {
            throw new WrongUserCredentialException("Wrong user password");
        }
    }

    @Override
    public void mergeUser(User user) {
        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        userDAO.merge(user);
    }

    @Override
    public void deleteUser(Integer userId) {
        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        userDAO.delete(userId);
    }

    @Override
    public void modifyUser(User modified, CredentialHandler credentialHandler) {
        logger.trace("Modify user method");
        User original = getUser(modified.getId());
        User readjust = modify(original, modified, credentialHandler);
        mergeUser(readjust);
    }

    private User modify(User original, User modification, CredentialHandler credentialHandler) {
        if (modification.getPassword().trim().isEmpty()) {
            logger.trace("Password have not been changed");
            modification.setPassword(original.getPassword());
        } else {
            logger.trace("Change user password");
            String password = credentialHandler.mutate(modification.getPassword());
            modification.setPassword(password);
        }
        return modification;
    }
}