package controller.command.impl.user;

import constants.PagePath;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;
import controller.command.manager.impl.user.UserAction;
import controller.utils.CommandUtils;
import model.entity.Question;
import model.entity.User;
import service.QuestionService;
import service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ListQuestionsCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        User user = CommandUtils.getUser(request);
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        QuestionService questionService = serviceFactory.createQuestionService();
        List<Question> questions = questionService.getQuestionsOfUser(user.getId());
        request.setAttribute("questions", questions);
        request.setAttribute("detailURL", "/user.do");
        request.setAttribute("detailAction", UserAction.VIEW_QUESTION_DETAILS);

        return PagePath.LIST_QUESTIONS;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required listQuestions with POST method");
    }
}
