package exception;

/**
 * Created by Vlad on 25.05.2018.
 */
public class NullRequestParameterException extends ApplicationException {
    public NullRequestParameterException(String message) {
        super(message);
    }
}
