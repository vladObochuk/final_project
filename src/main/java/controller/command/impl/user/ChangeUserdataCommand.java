package controller.command.impl.user;

import constants.PagePath;
import controller.command.AbstractCommand;
import controller.command.exception.WrongCommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ChangeUserdataCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        return PagePath.CHANGE_DATA;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        throw new WrongCommandException("Required changeUserdata with POST method");
    }
}
