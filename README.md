# Описания системы
Система **"Что? Где? Когда?"**. Система состоит из **Знатоков** и **Судьи**.
**Знатоки** отвечают на **Вопросы** разного типа (с варинтами ответа и без).
**Судья** принимает ответы от **Знатоков**, опередляет правильность ответа, и
защитывает очко команде знатоков (в случае правильного ответа) или
команде противников (если **Время** вышло, или ответ не правильный). **Судья**
также может при запросе от знатоков давать **Подсказки** разного типа.
Сервисы:
**Время** - засекает время между началом и концом вопроса в соответствии с
**Конфигурацией**.
**Подсказки**:
• может выводить вероятность правильного ответа (при выборе из
нескольких вариантов),
• либо убрать несколько неправильных вариантов (при выборе из
нескольких вариантов),
• либо дать текстовую подсказку (заранее прикреплена к вопросу)
• либо дать дополнительное время, и т.п.
**Статистика** - отображает статистику после окончания игры. Формат
определяется **Конфигурацией**
**Конфигурация** - управляет настройками системы: время, количество
игроков, тип подсказок, количество вопросов, и т.п. 

### Установка
Для работы приложения нужно чтобы на компьютере были установлены:

- Apache Tomcat 8.5
- MySQL 6.3
- Maven 2 - для сборки проекта.
 
Версии могут отличаться, но работоспособность біла проверена только при этих настройках.

Для начала нужно упаковать приложения:
```
maven package
```
###### Настроить tomcat
Настроить SSL соеденения при помощи следующего источника: https://tomcat.apache.org/tomcat-7.0-doc/ssl-howto.html
###### Настроить MySQL Server.
На машине должен быть запущен локальный сервер MySQL с пользователем и паролем указаным в [файле описания контекста](src/main/webapp/META-INF/context.xml)

Запустить на исполнения на этом сервере sql-команды из файла [init.sql](resources/init.sql)
### Запуск
Чтобы запустить приложения в Apache Tomcat, нужно скопировать ```.war``` файл этого проекта в папку развертывания(webapp) Apache Tomcat.

Если контейнер сервлетов Apache Tomcat не запущен, необходимо его запустить.

Для перехода к приложению необходимо в браузере перейти по следующей ссылке:

http://localhost:8080/final

### Пользователи

>##### Администратор
login: 'administrator',
password: 'ZIns4vJD'

>##### Модератор
login: 'moderator',
password: '08JQu3Ke';
login: 'auto',
password: 'owHAzdUT'

>##### Пользователь
login: 'user',
password: 'Y6IRFWgx'