package model.database.dao.interfaces;


import model.database.dao.DAO;
import model.entity.User;

import java.util.List;
import java.util.Optional;

/**
 * Provides additional methods for accessing User objects in database
 */
public interface UserDAO extends DAO<User> {
    /**
     * get User with supplied username, if not exist throws an exception
     * @param username not {@code null}
     * @return user with supplied username
     */
    Optional<User> get(String username);

    /**
     * get all users with supplied role
     * @param role not null
     * @return list of users with supplied role
     */
    List<User> getUsersWithRole(User.Role role);
}
