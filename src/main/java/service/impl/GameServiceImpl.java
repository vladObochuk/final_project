package service.impl;

import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.AnswerDAO;
import model.database.dao.interfaces.ConfigDAO;
import model.database.dao.interfaces.GameDAO;
import model.database.dao.interfaces.HintTypeDAO;
import model.entity.*;
import org.apache.log4j.Logger;
import service.GameService;

import java.util.List;
import java.util.Optional;

public class GameServiceImpl implements GameService {
    private static Logger logger = Logger.getLogger(GameServiceImpl.class);

    private final DAOFactory DAO_FACTORY;

    public GameServiceImpl(DAOFactory DAO_FACTORY) {
        this.DAO_FACTORY = DAO_FACTORY;
    }

    @Override
    public List<HintType> getAllAvailableHints() {
        HintTypeDAO hintTypeDAO = DAO_FACTORY.createHintTypeDAO();
        return hintTypeDAO.getAll();
    }

    @Override
    public Config getLastConfigurationOrDefault(User user) {
        ConfigDAO configDAO = DAO_FACTORY.createConfigDAO();
        Optional<Config> configOptional = configDAO.getLastUserConfiguration(user.getId());
        return configOptional.orElse(DEFAULT_CONFIG);
    }

    @Override
    public void addConfiguration(Config config) {
        ConfigDAO configDAO = DAO_FACTORY.createConfigDAO();
        configDAO.insert(config);
    }

    @Override
    public void addAnswer(Answer answer) {
        AnswerDAO answerDAO = DAO_FACTORY.createAnswerDAO();
        answerDAO.insert(answer);
    }

    @Override
    public void addGame(Game game) {
        GameDAO gameDAO = DAO_FACTORY.createGameDAO();
        gameDAO.insert(game);
    }

}
