package model.database.dao.concrete.mysql;

import model.database.dao.ErrorCodes;


public class MySQLErrorCodes implements ErrorCodes {
    @Override
    public int TOO_BIG_FIELDLENGTH() {
        return 1074;
    }

    @Override
    public int DUP_UNIQUE() {
        return 1062;
    }
}
