package model.entity;

import constants.ValidatorMessages;
import model.database.DomainObject;
import validation.annotation.MaxLength;
import validation.annotation.MinLength;
import validation.annotation.NotNull;

public class Question extends DomainObject {
    @NotNull(ValidatorMessages.QUESTION_TEXT_NULL)
    @MinLength(value = 20, message = ValidatorMessages.QUESTION_TEXT_SMALL)
    @MaxLength(value = 1024, message = ValidatorMessages.QUESTION_TEXT_BIG)
    private String text;

    @NotNull(ValidatorMessages.QUESTION_HINT_NULL)
    @MaxLength(value = 255, message = ValidatorMessages.QUESTION_HINT_BIG)
    private String hint;

    @NotNull(ValidatorMessages.QUESTION_LANG_NULL)
    private String language;

    private Integer rightAnswerId;
    private Integer userId;
    private Boolean isApproved;

    public static Builder  newBuilder(){
        return new Question().new Builder();
    }

    public class Builder{

        public Builder id(Integer id){
            Question.this.id = id;
            return this;
        }

        public Builder text(String text){
            Question.this.text = text;
            return this;
        }

        public Builder hint(String hint){
            Question.this.hint = hint;
            return this;
        }
        public Builder lang(String lang){
            Question.this.language = lang;
            return this;
        }

        public Builder rightAnswerId(Integer rightAnswerId){
            Question.this.rightAnswerId = rightAnswerId;
            return this;
        }

        public Builder userId(Integer userId){
            Question.this.userId = userId;
            return this;
        }

        public Builder isAproved(Boolean isAproved){
            Question.this.isApproved = isAproved;
            return this;
        }

        public Question build(){
            return Question.this;
        }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    public Integer getRightAnswerId() {
        return rightAnswerId;
    }

    public void setRightAnswerId(Integer rightAnswerId) {
        this.rightAnswerId = rightAnswerId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
