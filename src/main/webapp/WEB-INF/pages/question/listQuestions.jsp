<%@ include file="../jspf/header.jspf" %>
<c:if test="${empty questions}">
    <div class="card text-center">
        <h4 class="display-3"><fmt:message key="listQuestions.empty"/></h4>
    </div>
</c:if>

<pagination:forEach items="${questions}">
    <c:url value="${detailURL}" var="questionDetails">
        <c:param name="action" value="${detailAction}"/>
        <c:param name="questionId" value="${item.id}"/>
    </c:url>
    

    <a class="link-unstyled" href="${questionDetails}">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">${varStatus.index + 1}</h5>
                <h6 class="card-subtitle mb-2 text-muted">
                    <c:choose>
                        <c:when test="${empty item.approved}">
                            <span class="text-warning"><fmt:message key="question.status.waiting"/></span>
                        </c:when>
                        <c:when test="${item.approved}">
                            <span class="text-success"><fmt:message key="question.status.confirmed"/></span>
                        </c:when>
                        <c:otherwise>
                            <span class="text-danger"><fmt:message key="question.status.rejected"/></span>
                        </c:otherwise>
                    </c:choose>
                </h6>
                <p class="card-text">

                    <fmt:message key="question.text.label"/>: <c:out value="${item.text}"/>
                </p>
            </div>
        </div>
    </a>
</pagination:forEach>

<c:if test="${not empty questions}">
    <pagination:elem items="${questions}"/>
</c:if>

<%@ include file="../jspf/footer.jsp" %>
