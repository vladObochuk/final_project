package controller.command.manager.impl.admin;

import controller.command.impl.admin.AddModeratorCommand;
import controller.command.impl.admin.DeleteUserCommand;
import controller.command.impl.admin.ListModeratorsCommand;
import controller.command.impl.admin.ModifyUserCommand;
import controller.command.manager.CommandManager;

public class AdminCommandManager extends CommandManager {

    private AdminCommandManager(){
        commandMap.put(AdminAction.ADD_MODERATOR, new AddModeratorCommand());
        commandMap.put(AdminAction.VIEW_ALL_MODERATORS, new ListModeratorsCommand());
        commandMap.put(AdminAction.DELETE_USER, new DeleteUserCommand());
        commandMap.put(AdminAction.EDIT_USER, new ModifyUserCommand());
    }

    private static class Holder{
        static final AdminCommandManager INSTANCE = new AdminCommandManager();
    }

    public static AdminCommandManager getInstance(){
        return Holder.INSTANCE;
    }
}
