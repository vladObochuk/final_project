package controller.command.impl.user;

import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import model.entity.*;
import service.GameService;
import service.QuestionService;
import service.TimeService;
import service.factory.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class GameCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        HttpSession session = request.getSession();

        // retrieve current question
        List<Question> chosenQuestions = (List<Question>) session.getAttribute("questions");
        Integer questionNumber = (Integer) session.getAttribute("questionNumber");
        questionNumber = (questionNumber == null ? 1 : questionNumber);
        session.setAttribute("questionNumber", questionNumber);

        Question current = chosenQuestions.get( questionNumber - 1);
        session.setAttribute("currentQuestion", current);

        // set up answer options for current question
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        QuestionService questionService = serviceFactory.createQuestionService();
        List<AnswerOption> questionAnswers = questionService.getQuestionAnswers(current.getId());
        session.setAttribute("questionAnswers", questionAnswers);

        User questionCreator = questionService.getCreatorOfQuestion(current);
        session.setAttribute("questionCreator", questionCreator);

        // setup time for answer on question
        TimeService timeService = serviceFactory.createTimeService();
        Config config = (Config) session.getAttribute("config");
        timeService.setupTimeForAnswer(session, config);

        return PagePath.GAME;
    }


    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        HttpSession session = request.getSession();
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        TimeService timeService = serviceFactory.createTimeService();

        boolean isCorrectAnswer;
        if (timeService.isTimeToAnswerPassed(session)){
            isCorrectAnswer = false;
        } else{
            String answer = request.getParameter("answer");
            isCorrectAnswer = (answer == null? processMultipleAnswerOptionsAnswer(request):
                    processSingleAnswerOptionAnswer(request));
        }
        timeService.removeSessionAttributes(session);

        if (isCorrectAnswer){
            CommandUtils.incrementAttribute(request, "playerPoints");
        } else {
            CommandUtils.incrementAttribute(request, "opponentPoints");
        }

        Integer questionNumber = (Integer) session.getAttribute("questionNumber");
        Config config = (Config) session.getAttribute("config");
        if (questionNumber.equals(config.getQuestionsAmount()))
            return request.getContextPath() + RedirectLink.RESULTS;

        CommandUtils.incrementAttribute(request, "questionNumber");
        return request.getContextPath() + RedirectLink.GAME;
    }

    private boolean processMultipleAnswerOptionsAnswer(HttpServletRequest request){
        Integer chosenAnswerId = CommandUtils.retrieveIntegerValue(request, "chosenAnswerId");

        HttpSession session = request.getSession();
        Game game = (Game) session.getAttribute("game");
        Question currentQuestion = (Question) session.getAttribute("currentQuestion");

        Answer thatAnswer = Answer.newBuilder()
                .gameId(game.getId())
                .questionId(currentQuestion.getId())
                .chosenAnswerId(chosenAnswerId)
                .build();

        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        GameService gameService = serviceFactory.createGameService();
        gameService.addAnswer(thatAnswer);

        return (chosenAnswerId.equals(currentQuestion.getRightAnswerId()));
    }

    private boolean processSingleAnswerOptionAnswer(HttpServletRequest request){
        String answer = request.getParameter("answer");

        HttpSession session = request.getSession();
        Game game = (Game) session.getAttribute("game");
        Question currentQuestion = (Question) session.getAttribute("currentQuestion");
        Answer thatAnswer = Answer.newBuilder()
                .gameId(game.getId())
                .questionId(currentQuestion.getId())
                .build();
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        GameService gameService = serviceFactory.createGameService();
        gameService.addAnswer(thatAnswer);

        QuestionService questionService = serviceFactory.createQuestionService();
        AnswerOption correctAnswer = questionService.getQuestionAnswer(currentQuestion);

        return correctAnswer.isTextEquals(answer);
    }
}