package model.database.dao;

import model.database.exception.RepositoryException;
import model.entity.Answer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TestAbstractDAO {
    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    Statement statement;

    @Mock
    ResultSet resultSet;

    @Mock
    ResultSet generatedKeys;

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    AbstractDAO abstractDAO;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private int rowsAffected = 1;
    private Integer id = 125;

    @Before
    public void setUp() throws SQLException {
        abstractDAO.connection = connection;

        when(connection.createStatement()).thenReturn(statement);
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(connection.prepareStatement(anyString(), eq(Statement.RETURN_GENERATED_KEYS))).thenReturn(preparedStatement);
        when(statement.executeQuery(anyString())).thenReturn(resultSet);
        when(preparedStatement.executeUpdate()).thenReturn(rowsAffected);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(preparedStatement.getGeneratedKeys()).thenReturn(generatedKeys);
        when(generatedKeys.next()).thenReturn(true);
        when(generatedKeys.getInt(1)).thenReturn(id);
    }

    @Test
    public void testInsert() throws SQLException{
        Answer answer = new Answer();
        int affected = abstractDAO.insert(answer);
        assertEquals(rowsAffected, affected);
        assertEquals(id, answer.getId());

        verify(connection, times(1)).prepareStatement(anyString(), eq(Statement.RETURN_GENERATED_KEYS));
        verify(preparedStatement, times(1)).executeUpdate();
        verify(preparedStatement, times(1)).getGeneratedKeys();
        verify(generatedKeys, times(1)).next();
        verify(generatedKeys, times(1)).getInt(1);
        verify(preparedStatement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testInsertException() throws SQLException{
        when(connection.prepareStatement(anyString(), anyInt())).thenThrow(new SQLException());

        Answer answer = new Answer();
        abstractDAO.insert(answer);

    }

    @Test
    public void testMerge() throws SQLException{
        Answer answer = new Answer();
        answer.setId(id);

        int affected = abstractDAO.merge(answer);
        assertEquals(rowsAffected, affected);
        assertEquals(id, answer.getId());

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeUpdate();
        verify(preparedStatement, times(1)).close();

    }

    @Test(expected = RepositoryException.class)
    public void testMergeException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        Answer answer = new Answer();
        answer.setId(id);
        abstractDAO.merge(answer);
    }

    @Test
    public void testDelete() throws SQLException{
        boolean result = abstractDAO.delete(id);
        assertEquals(true, result);

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeUpdate();
        verify(preparedStatement, times(1)).setInt(1, id);
        verify(preparedStatement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testDeleteError() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        abstractDAO.delete(id);
    }

    @Test
    public void testGetById() throws SQLException{
        Optional<Answer> answerOptional = abstractDAO.getById(id);
        assertFalse(answerOptional.isPresent());

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeQuery();
        verify(preparedStatement, times(1)).setInt(1, id);
        verify(preparedStatement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testGetByIdException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        abstractDAO.getById(id);
    }

    @Test
    public void testGetAll() throws SQLException{
        List<Answer> answers = abstractDAO.getAll();
        assertTrue(answers.isEmpty());

        verify(connection, times(1)).createStatement();
        verify(statement, times(1)).executeQuery(anyString());
        verify(statement, times(1)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testGetAllException() throws SQLException{
        when(connection.createStatement()).thenThrow(new SQLException());

        abstractDAO.getAll();
    }
}
