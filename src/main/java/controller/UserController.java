package controller;

import controller.command.manager.CommandManager;
import controller.command.manager.factory.CommandManagerFactory;
import controller.command.manager.factory.CommandManagerType;
import controller.utils.HttpMethod;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "gameController", urlPatterns = {"/user.do"})
public class UserController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CommandManager commandManager = CommandManagerFactory.getCommandManager(CommandManagerType.USER);
        ControlProvider.process(commandManager, req, resp, HttpMethod.GET);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CommandManager commandManager = CommandManagerFactory.getCommandManager(CommandManagerType.USER);
        ControlProvider.process(commandManager, req, resp, HttpMethod.POST);
    }
}
