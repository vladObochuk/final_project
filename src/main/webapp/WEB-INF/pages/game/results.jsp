<%@ include file="../jspf/header.jspf" %>

<c:url value="/user.do" var="gameURL">
    <c:param name="action" value="startGame"/>
</c:url>

<c:url value="/user.do" var="configUrl">
    <c:param name="action" value="configure"/>
</c:url>

<div class="card text-center">
    <h3 class="card-header">
        <c:choose>
            <c:when test="${playerPoints lt opponentPoints}">
                <fmt:message key="game.lose"/>
            </c:when>

            <c:when test="${playerPoints gt opponentPoints}">
                <fmt:message key="game.win"/>
            </c:when>

            <c:otherwise>
                <fmt:message key="game.draw"/>
            </c:otherwise>
        </c:choose>
    </h3>

    <div class="card-body">
        <table class="table table-hover text-center">
            <thead>
            <tr>
                <th>
                    <fmt:message key="you"/>
                </th>
                <th>
                    <fmt:message key="opponents"/>
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th><span class="badge badge-primary">${playerPoints}</span></th>
                <th><span class="badge badge-danger"> ${opponentPoints} </span></th>
            </tr>
            </tbody>
        </table>

        <a href="${gameURL}" class="btn btn-primary"><fmt:message key="again"/> </a>
        <a href="${configureURL}" class="btn btn-primary"><fmt:message key="config"/> </a>
    </div>
</div>


<%@ include file="../jspf/footer.jsp" %>
