package controller.filter;

import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Sets the encoding for request
 */
public class EncodingFilter implements Filter {
    private static Logger logger = Logger.getLogger(EncodingFilter.class);

    private final String DEFAULT_ENCODING = "UTF-8";
    private String encoding;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        logger.debug("INIT start");
        String initEncoding = filterConfig.getInitParameter("encoding");
        if (initEncoding == null) {
            encoding = DEFAULT_ENCODING;
        } else {
            encoding = initEncoding;
        }
        logger.trace("Encoding set to: " + encoding);
        logger.debug("INIT finished");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.debug("doFilter start");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        request.setCharacterEncoding(encoding);
        logger.trace("Set encoding for " + request.getRequestURI() + " to " + encoding);
        logger.debug("doFilter finish");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        logger.debug("DESTROY method");
    }
}
