<%@ include file="../jspf/header.jspf" %>


<csrf:form action="" method="post" htmlClass="form-medium">
    <div class="form-group">
        <div class="row col-sm-6 col-lg-4">
            <label for="lang">
                <fmt:message key="question.lang"/>
            </label>
            <select id="lang" name="lang" class="form-control">
                <option value="en">
                    <fmt:message key="lang.en"/>
                </option>
                <option value="uk">
                    <fmt:message key="lang.uk"/>
                </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="text">
            <fmt:message key="question.text.label"/>
        </label>
        <textarea id="text" placeholder="<fmt:message key="question.text.placeholder"/> "
                  name="text" cols="25" rows="6" maxlength="768" class="form-control" required></textarea>
    </div>

    <div class="form-group">
        <label for="rightAnswer">
            <fmt:message key="question.answer.label"/>
        </label>
        <input id="rightAnswer" name="rightAnswer" type="text"
               placeholder="<fmt:message key="question.answer.placeholder"/> " class="form-control" required>
    </div>


    <div class="form-group">
        <div class="input_fields_wrap">
            <button type="button" class="add_field_button btn btn-success btn-sm">
                <fmt:message key="question.appendOption.label"/>
            </button>
        </div>
    </div>

    <div class="form-group">
        <label for="hint">
            <fmt:message key="question.hint.label"/>
        </label>
        <input id="hint" name="hint" type="text"
               placeholder="<fmt:message key="question.hint.placeholder"/> " class="form-control" required>
    </div>

    <button type="submit" class="btn btn-lg btn-primary">
        <fmt:message key="question.add.submit"/>
    </button>
</csrf:form>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/addField.js"></script>

<%@ include file="../jspf/footer.jsp" %>