<%@ include file="../jspf/header.jspf" %>

<c:choose>
    <c:when test="${question.language eq 'en'}">
        <c:set var="language" value="lang.en"/>
    </c:when>

    <c:otherwise>
        <c:set var="language" value="lang.uk"/>
    </c:otherwise>
</c:choose>

<div class="jumbotron">
    <h3 class="display-4">
        <fmt:message key="question"/>, <fmt:message key="question.lang"/>
        <fmtdt:message key="${language}" defaultVal=""/>
    </h3>
    <p class="lead"><c:out value="${question.text}"/></p>
    <hr>
    <fmt:message key="question.hint.label"/>
    <p class="lead"><c:out value="${question.hint}"/></p>
    <hr>
    <fmt:message key="question.answer.label"/>
    <p class="lead">
        <c:forEach items="${answerOptions}" var="answerOption">
            <c:if test="${answerOption.id eq question.rightAnswerId}">
                <c:out value="${answerOption.text}"/>
            </c:if>
        </c:forEach>
    </p>
    <hr>
    <fmt:message key="question.answers"/>
    <div class="card-deck">
        <c:forEach items="${answerOptions}" var="answerOption">
            <div class="card">
                <div class="card-body">
                    <c:out value="${answerOption.text}"/>
                </div>
            </div>
        </c:forEach>
    </div>
</div>

<%@ include file="../jspf/footer.jsp" %>