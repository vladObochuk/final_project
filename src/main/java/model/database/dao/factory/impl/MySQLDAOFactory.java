package model.database.dao.factory.impl;

import model.database.dao.ErrorCodes;
import model.database.dao.concrete.mysql.*;
import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.*;
import model.database.datasource.DataSourceFactory;
import model.database.datasource.DataSourceType;
import model.database.exception.RepositoryException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class MySQLDAOFactory implements DAOFactory {
    private static DataSource dataSource = DataSourceFactory.getDataSource(DataSourceType.MYSQL);


    private Connection connection;

    private final MySQLAnswerDAO ANSWER_DAO;
    private final MySQLAnswerOptionDAO ANSWER_OPTION_DAO;
    private final MySQLConfigDAO CONFIG_DAO;
    private final MySQLGameDAO GAME_DAO;
    private final MySQLHintTypeDAO HINT_TYPE_DAO;
    private final MySQLQuestionDAO QUESTION_DAO;
    private final MySQLUserDAO USER_DAO;
    private final MySQLErrorCodes ERROR_CODES = new MySQLErrorCodes();

    public MySQLDAOFactory(){
        try {
            connection = dataSource.getConnection();
            ANSWER_DAO = new MySQLAnswerDAO(connection);
            ANSWER_OPTION_DAO = new MySQLAnswerOptionDAO(connection);
            CONFIG_DAO = new MySQLConfigDAO(connection);
            GAME_DAO = new MySQLGameDAO(connection);
            HINT_TYPE_DAO = new MySQLHintTypeDAO(connection);
            QUESTION_DAO = new MySQLQuestionDAO(connection);
            USER_DAO = new MySQLUserDAO(connection);

        } catch (SQLException e){
            throw new RepositoryException(e);
        }
    }


    @Override
    public Connection getConnection() {
        return connection;
    }

    @Override
    public AnswerDAO createAnswerDAO() {
        return ANSWER_DAO;
    }

    @Override
    public AnswerOptionDAO createAnswerOptionDAO() {
        return ANSWER_OPTION_DAO;
    }

    @Override
    public ConfigDAO createConfigDAO() {
        return CONFIG_DAO;
    }

    @Override
    public GameDAO createGameDAO() {
        return GAME_DAO;
    }

    @Override
    public HintTypeDAO createHintTypeDAO() {
        return HINT_TYPE_DAO;
    }

    @Override
    public QuestionDAO createQuestionDAO() {
        return QUESTION_DAO;
    }

    @Override
    public UserDAO createUserDAO() {
        return USER_DAO;
    }

    @Override
    public ErrorCodes getErrorCodes() {
        return ERROR_CODES;
    }
}