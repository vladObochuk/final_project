﻿CREATE DATABASE  IF NOT EXISTS `final_project` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `final_project`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: final_project
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer_options`
--

DROP TABLE IF EXISTS `answer_options`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ans_quest_idx` (`question_id`),
  CONSTRAINT `ans_quest` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer_options`
--

LOCK TABLES `answer_options` WRITE;
/*!40000 ALTER TABLE `answer_options` DISABLE KEYS */;
INSERT INTO `answer_options` VALUES (168,'Превед',75),(169,'для страт',76),(170,'Палач',77),(171,'Шляпник',77),(172,'Ювелір',77),(173,'Банщик',77),(174,'Вітт',78),(175,'Іов',78),(176,'Августін',78),(177,'Лазар',78),(178,'Через травми Ахілла',79),(179,'Через травми Геракла',79),(180,'По релігійним причинам',79),(181,'Дипломатичні причини',79),(182,'Цурен Правдивий',80),(183,'Альфонс Цибулка',80),(184,'Девід Гілмор',80),(185,'Ян Качмарек',80),(186,'Заборонено участь в Олімпійських іграх',81),(187,'Заборона постачання зброї',81),(188,'Закритті торгові шляхи',81),(189,'Заборонено дипломатичні відносини',81),(190,'Щоб скинути зайву вагу',82),(191,'Щоб залишити офіс пустим на цей день',82),(192,'Щоб витратити гроші і втекти',82),(193,'Змії замість волос',83),(194,'Великі зрачки',83),(195,'Судини на шкірі',83),(196,'Незнайка',84),(197,'Марко Поло',84),(198,'Маклухо-Маклай',84),(199,'Федір Конюхов',84),(200,'Шубами',85),(201,'Добре накритим столом',85),(202,'Алкоголем',85),(203,'Грошима',85),(204,'Адмірал',86),(205,'Генерал',86),(206,'Майор',86),(207,'Мічман',86),(208,'Шопінг',87),(209,'Секс',87),(210,'Слухи',87),(211,'Біг',87),(212,'Інтуїція',88),(213,'Карма',88),(214,'Доля',88),(215,'Любов',88),(216,'Preved',89),(217,'for strata',90),(218,'The executioner',91),(219,'Hat',91),(220,'Jeweler',91),(221,'The sailor',91),(222,'Wit',92),(223,'Iov',92),(224,'Avgustin',92),(225,'Lazar',92),(226,'Tsuran True',93),(227,'Alphonse Tsybulka',93),(228,'David Gilmore',93),(229,'Jan Kachmarek',93),(230,'Because of the injuries of Achilles',94),(231,'For religious reasons',94),(232,'Hurricane\'s injuries',94),(233,'Diplomatic reasons',94),(234,'Participation in the Olympic Games is prohibited',95),(235,'Prohibition of supply of weapons',95),(236,'Closing trade routes',95),(237,'Diplomatic relations are forbidden',95),(238,'To lose weight',96),(239,'To leave the office empty for this day',96),(240,'To spend money and run away',96),(241,'Snakes instead of hair',97),(242,'Big pupils',97),(243,'Vessels on the skin',97),(244,'Stranger',98),(245,'Marco Polo',98),(246,'Maclayh-Maclay',98),(247,'Fedor Konyukhov',98),(248,'Fur coats',99),(249,'Well covered table',99),(250,'Alcohol',99),(251,'Money',99),(252,'Admiral',100),(253,'General',100),(254,'Major',100),(255,'Mitchman',100),(256,'Shopping',101),(257,'Sex',101),(258,'Rumors',101),(259,'Run',101),(260,'Intuition',102),(261,'Karma',102),(262,'Fate',102),(263,'Love',102),(272,'1941',105),(273,'1932',105),(274,'1938',105),(275,'1 September 1939',106),(276,'22 June 1941',106),(277,'9 May 1945',106),(278,'8 May 1945',106),(279,'answering question',107),(280,'sleep',107),(281,'nothing',107),(282,'read words',107);
/*!40000 ALTER TABLE `answer_options` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game_id` int(11) NOT NULL,
  `chosen_answer_id` int(11) DEFAULT NULL,
  `question_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `answeroptions_answers_idx` (`chosen_answer_id`),
  KEY `questions_answers_idx` (`question_id`),
  CONSTRAINT `answeroptions_answers` FOREIGN KEY (`chosen_answer_id`) REFERENCES `answer_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `questions_answers` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configs`
--

DROP TABLE IF EXISTS `configs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time_for_reply` int(11) DEFAULT '60',
  `questions_amount` int(11) DEFAULT '8',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_configs_idx` (`user_id`),
  CONSTRAINT `users_configs` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs`
--

LOCK TABLES `configs` WRITE;
/*!40000 ALTER TABLE `configs` DISABLE KEYS */;
INSERT INTO `configs` VALUES (22,60,5,30),(23,60,5,31),(24,60,5,32),(32,60,5,41);
/*!40000 ALTER TABLE `configs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `configs_hinttypes`
--

DROP TABLE IF EXISTS `configs_hinttypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configs_hinttypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_id` int(11) NOT NULL,
  `hint_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `configs_FK_idx` (`config_id`),
  KEY `hint_types_FK_idx` (`hint_type_id`),
  CONSTRAINT `configs_FK` FOREIGN KEY (`config_id`) REFERENCES `configs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `hint_types_FK` FOREIGN KEY (`hint_type_id`) REFERENCES `hint_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configs_hinttypes`
--

LOCK TABLES `configs_hinttypes` WRITE;
/*!40000 ALTER TABLE `configs_hinttypes` DISABLE KEYS */;
/*!40000 ALTER TABLE `configs_hinttypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_Id` int(11) NOT NULL,
  `configuration_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_games_idx` (`user_Id`),
  KEY `configurations_games_idx` (`configuration_id`),
  CONSTRAINT `users_games` FOREIGN KEY (`user_Id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (56,32,24),(58,32,24);
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hint_types`
--

DROP TABLE IF EXISTS `hint_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hint_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hint_types`
--

LOCK TABLES `hint_types` WRITE;
/*!40000 ALTER TABLE `hint_types` DISABLE KEYS */;
INSERT INTO `hint_types` VALUES (3,'hint.time'),(4,'hint.show'),(5,'hint.deleteOptions'),(6,'hint.probability');
/*!40000 ALTER TABLE `hint_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `hint` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `right_answer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `approved` tinyint(4) DEFAULT NULL,
  `lang` enum('en','uk') COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_questions_idx` (`user_id`),
  CONSTRAINT `users_questions` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (75,'Перси порадять бути веселим, в Малайзії запитають «Куди ви йдете?», грузини побажають перемагати, японці констатують, що наступив новий день. А яке слово з’явилося у лютому 2006-го року для використання у аналогічних ситуаціях?','... медвед',168,32,1,'uk'),(76,'Древні римляни розводили у ставках гігантських триметрових мурен, вважаючи м\'ясо цих хижаків ніжним делікатесом. А для чого ще використовувалися мурени?','Починати з для',169,32,1,'uk'),(77,'Єкатерина 2 успішно лікувалась від викривлення хребта в представника цієї професії. Характерно, що  перед тим як вона прийшла за пристол, люди, що займалися цим, залишалися практично без роботи по спеціальності. Що це за професія?','При Єлизаветі Петровній смертельна кара була де-факто відмінена',170,32,1,'uk'),(78,'Назвіть ім\'я святого \"переслідувавшого\" Геракла, Петра 1, Наполеона Бонапарта, Ф.М. Достоєвськогоі П. І. Чайковського','\"Пляска святого ...\" - назва епілепсії, від якої страдали вказані люди.',174,32,1,'uk'),(79,'Коментатор футбольного матчу між Грецією і Росією, представляючи команду Греції і розповідаючи про двох відсутніх з різних причин гравцях, згадав двох міфологічних персонажів. Одного з цих футболістів не відпустив на матч амстердамський «Аякс», за який він грає. А чому не взяв участі в матчі другої?','Травма',178,32,1,'uk'),(80,'Цей талановитий поет написав понад п\'ятсот творів, часто виконувалися під музику. Тексти їх до нас не дійшли, відомо лише кілька рядків. Шанувальники його творчості додавали до його імені поважний епітет, недруги вважали злочином знайомство з його творчістю. Доля поета склалася трагічно - він піддався гонінням, спився і змушений був покинути батьківщину. Назвіть його ім\'я.','Іноді прізвище каже само за себе',182,32,1,'uk'),(81,'У 1926 і 1948 роках Німеччина була покарана за розв\'язання війн так само, як колись була покарана Спарта. Що це за покарання?','Покарання як для країни не досить жорстке',186,32,1,'uk'),(82,'Газета «Уїклі уорлд ньюс» провела опитування в п\'яти великих містах Америки, з\'ясовуючи, хто погодиться за 1 мільйон доларів відправитися голяка на роботу. Погодилися 84% чоловіків. Жінки, як з\'ясувалося, дещо сором\'язливі: лише 20% продемонстрували б свої принади. Правда, пояснення, можливо, міститься в словах однієї з учасниць опитування, яка роздяглася б за умови, що її попередять за кілька тижнів. А для чого їй ці кілька тижнів?','Жінки, вони такі...',190,32,1,'uk'),(83,'Вбивство, вчинене однією з НИХ, описано Конан Дойлом. Жертвами інших ЇХ різновидів стали в 1880 р. 66 австралійців. А яка характерна особливість зовнішності була у тієї, чиє ім\'я все ВОНИ носять?','Всі вони - медузи',193,32,1,'uk'),(84,'Мексиканський курорт Акапулько всесвітньо відомий. Своєю популярністю він багато в чому зобов\'язаний місцевого клімату, як не можна краще невластивому для відпочинку. Здогадавшись, що в перекладі з мови ацтеків означає слово «акапулько», назвіть знаменитого мандрівника, який побував, крім інших цікавих місць, і в місті з аналогічною назвою.','Казка',196,32,1,'uk'),(85,'Відомо, що багато російських княжі роди - татарського походження. Одного разу імператор Павло запитав графа Ростопчина:\r\n- Адже Ростопчина татарського походження?\r\n- Точно так, государ, - відповів Ростопчина.\r\n- Чому ж ви не князі?\r\n- А тому, що предок мій переселився в Росію взимку. Влітку іменитим татарам, які прибули до двору, царі шанували князівська гідність ...\r\nА чим, за словами Ростопчина, царі шанували татар, які прибули взимку?','Холодно ж',200,32,1,'uk'),(86,'Ці два слова, арабське і грецьке, в перекладі означають майже одне і те ж, хоча одне з них - географічний термін, а інше - військове звання. Що ж це за слова?','Повелитель морів перевод',204,32,1,'uk'),(87,'Англійський вчений-психолог Девід Льюїс стверджує, що ЦЕ безпечно лише для жінок, тоді як для чоловіків може стати джерелом небезпечних хвороб. Проведені дослідження показали, що лише у чверті жінок спостерігалися будь-які незначні відхилення, наприклад, серцебиття. Чоловіки ж, навпаки, вкрай негативно відреагували на це: у них почастішав пульс, стала проявлятися аритмія, різко підскочив кров\'яний тиск. Назвіть ЦЕ англійським словом, яке відносно недавно проникло і в російську мову.','Не біг)',208,32,1,'uk'),(88,'Багато хто не вірить в її існування. Однак Кант вважав, що з неї починається будь-яке людське знання. А ще кажуть, що вона підводить тільки тих, у кого вона є. Назвіть це.','.',212,32,1,'en'),(89,'The Persians are advised to be fun, in Malaysia they ask \"Where are you going?\", The Georgians want to win, the Japanese state that a new day has come. And what word appeared in February 2006 for use in similar situations?','... medved',216,32,1,'en'),(90,'Ancient Romans were bred in ponds of giant three-meter moray eels, considering the meat of these predators delicate delicacies. And what else were the mourns used for?','Start with for',217,32,1,'en'),(91,'Ekaterina 2 was successfully treated from curvature of the spine to a representative of this profession. It is characteristic that, before she came for a pintol, the people who were engaged in this, remained practically without work in the specialty. What is this profession?','When Elizabeth Petrovna, the death penalty was de facto abolished',218,32,1,'en'),(92,'Name the name of the holy \"persecutor\" Hercules, Peter 1, Napoleon Bonaparte, F.M. Dostoevsky and P. I. Tchaikovsky','\"Plyaska saint ...\" - the name of the epilepsy, from which suffered the specified people.',222,32,1,'en'),(93,'This talented poet has written over five hundred works, often performed for music. Their texts did not reach us, only a few lines are known. Fans of his work added to his name a venerable epithet, the enemies believed crime familiarity with his creativity. The fate of the poet was tragic - he succumbed to persecution, slept and was forced to leave his homeland. Name his name.','Sometimes the name speaks for itself',226,32,1,'en'),(94,'Commenting on the football match between Greece and Russia, representing the Greek team and telling two missing players for various reasons, mentioned two mythological characters. One of these players did not let the Amsterdam Ajax play for him. And why did not take part in the second match?\r\n','Trauma',230,32,1,'en'),(95,'In 1926 and 1948, Germany was punished for resolving wars just as Sparta was once punished. What is this punishment?','Punishment for the country is not strict enough',234,32,1,'en'),(96,'Газета «Уїклі уорлд ньюс» провела опитування в п\'яти великих містах Америки, з\'ясовуючи, хто погодиться за 1 мільйон доларів відправитися голяка на роботу. Погодилися 84% чоловіків. Жінки, як з\'ясувалося, дещо сором\'язливі: лише 20% продемонстрували б свої принади. Правда, пояснення, можливо, міститься в словах однієї з учасниць опитування, яка роздяглася б за умови, що її попередять за кілька тижнів. А для чого їй ці кілька тижнів?','Women are they ...',238,32,1,'en'),(97,'The murder committed by one of the NIHs is described by Conan Doyle. The victims of other their varieties were in 1880 66 Australians. And what characteristic feature of appearance was in the one whose name ALL they Wear?','All of them are jellyfish',241,32,1,'en'),(98,'The Mexican resort of Acapulco is world-famous. His popularity is largely due to the local climate, as it is not better for an unproductive holiday. Having realized that the translation of the Aztec language means the word acapulco, name the famous traveler who visited, in addition to other interesting places, and in a city with a similar name.','Tale',244,32,1,'en'),(99,'It is known that many n princely tribes - Tatar origin. Once Emperor Paul asked Count Rustopchin: \"Is Rostopchyna of Tatar origin?\" \"Just so, sir,\" replied Rustopchina. - Why are not you princes? - But because my ancestor moved to Russia in the winter. In the summer, eminent Tatars who arrived in the courtyard, the kings revered the princely dignity ... But what, according to Rostopchin, did the kings revere the Tatars who arrived in the winter?','It\'s cold',248,32,1,'en'),(100,'These two words, Arabic and Greek, in translation mean almost the same, although one of them is a geographical term and the other is a military title. What is this word?\r\n','The Lord of the Seas Translation',252,32,1,'en'),(101,'English psychologist David Lewis argues that it is safe for women only, whereas men can be the source of dangerous illnesses. Studies have shown that only a quarter of women experienced any slight deviations, such as palpitations. Men, on the contrary, responded extremely negatively to this: they had a pulse more frequent, arrhythmia began to manifest, blood pressure jumped sharply. Name it the English word, which relatively recently penetrated into the n language.','It`s not run',256,32,1,'en'),(102,'Many do not believe in its existence. However, Kant believed that any human knowledge would begin with it. And they say that it brings only those who have it. Name this.\r\n','not fate',260,32,1,'en'),(105,'Коли в Гітлера кішка Штульхард народила перше котиня?','Все вигадка',272,41,NULL,'uk'),(106,'When did the second world war start','War started with attack on Polish',275,41,1,'en'),(107,'What are you doing now?','don\'t sleep now!',279,41,0,'en');
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(288) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (8,'admin','50a9b0473732bb4ae8f2e647797e8e1b67b7a512b5a2e5cb3a542ff9a3e3b132$10000$c3b027e45dca105a3bdd1c708375d13a1c228e26c303e514db71c54cc70d7914','ADMIN'),(30,'administrator','c8cdc05db87429bc57e06ecabd320840a07791a4fb99ff056719e951c87da449$10000$0c92a5269bcceaff2c95a904f4f7df087962568884c8f7fa7fcf0479446134df','ADMIN'),(31,'moderator','323583c1a92821e5b295752504e8f3ff974e1be123eb8bd3266c4cd0f147f032$10000$ce6b9f2540db26c149ecf903a3c4ff69cc8bab9485e3e3aee544677160be55e5','MODERATOR'),(32,'auto','96f39e16344f6f457bf0e30ddfd004571d7d95bf746b84731add3f6cfa3aaaf1$10000$addab0fef5961efaac8cecac7b888f4ef59361b31adf9325682ba32a4cdd452d','MODERATOR'),(41,'user','919d9412550f764ad7e08fc4bbb0680c37da0e4cd8dd7b6a305443c6b7e34d39$10000$a85757d9278a10a6d560c1a593c3495a3c3d68ab804587ab51a32f39bb6ad6d4','ORDINARY');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `final_project`.`users_AFTER_INSERT` AFTER INSERT ON `users` FOR EACH ROW
BEGIN
	INSERT INTO configs (time_for_reply, questions_amount, user_id) VALUES (60, 5, NEW.id);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-28  8:50:54
