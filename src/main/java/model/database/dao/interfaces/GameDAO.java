package model.database.dao.interfaces;

import model.database.dao.DAO;
import model.entity.Game;

/**
 * Provides additional methods for accessing Game objects in database
 */
public interface GameDAO extends DAO<Game> {
}
