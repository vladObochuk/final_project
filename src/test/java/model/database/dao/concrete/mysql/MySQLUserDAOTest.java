package model.database.dao.concrete.mysql;

import model.database.exception.RepositoryException;
import model.entity.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class MySQLUserDAOTest {

    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private MySQLUserDAO mySQLUserDAO;

    private User source = User.newBuilder()
            .id(55555)
            .password("passphrase")
            .username("user")
            .role(User.Role.MODERATOR.name())
            .build();

    @Before
    public void setUp() throws SQLException {
        mySQLUserDAO = new MySQLUserDAO(connection);
    }

    @Test
    public void testTableNameNotNull(){
        assertNotNull(mySQLUserDAO.getTableName());
    }

    @Test
    public void testTableNameNotEmpty(){
        assertFalse(mySQLUserDAO.getTableName().isEmpty());
    }

    @Test
    public void testSelectQueryNotNull(){
        assertNotNull(mySQLUserDAO.getSelectQuery());
    }

    @Test
    public void testSelectQueryNotEmpty(){
        assertFalse(mySQLUserDAO.getSelectQuery().isEmpty());
    }

    @Test
    public void testMergeQueryNotNull(){
        assertNotNull(mySQLUserDAO.getMergeQuery());
    }

    @Test
    public void testMergeQueryNotEmpty(){
        assertFalse(mySQLUserDAO.getMergeQuery().isEmpty());
    }

    @Test
    public void testInsertQueryNotNull(){
        assertNotNull(mySQLUserDAO.getInsertQuery());
    }

    @Test
    public void testInsertQueryNotEmpty(){
        assertFalse(mySQLUserDAO.getInsertQuery().isEmpty());
    }

    @Test
    public void testMapEntities() throws SQLException{

        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getInt("id")).thenReturn(source.getId());
        when(resultSet.getString("username")).thenReturn(source.getUsername());
        when(resultSet.getString("password")).thenReturn(source.getPassword());
        when(resultSet.getString("role")).thenReturn(source.getRole());

        List<User> users = mySQLUserDAO.mapEntities(resultSet);

        assertNotNull(users);
        assertEquals(2, users.size());
        for (User user: users) {
            assertEquals(source.getPassword(), user.getPassword());
            assertEquals(source.getUsername(), user.getUsername());
            assertEquals(source.getRole(), user.getRole());
            assertEquals(source.getId(), user.getId());
        }

        verify(resultSet, times(3)).next();
        verify(resultSet, times(2)).getInt(anyString());
        verify(resultSet, times(6)).getString(anyString());
    }

    @Test
    public void testPrepareStatementForMerge() throws SQLException{
        mySQLUserDAO.prepareStatementForMerge(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setInt(anyInt(), eq(source.getId()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getPassword()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getUsername()));
        verify(preparedStatement, atLeast(2))
                .setString(anyInt(), eq(source.getRole()));
    }

    @Test
    public void testPrepareStatementForInsert() throws SQLException{
        mySQLUserDAO.prepareStatementForInsert(preparedStatement, source);

        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getPassword()));
        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getUsername()));
        verify(preparedStatement, atLeast(1))
                .setString(anyInt(), eq(source.getRole()));
    }

    @Test
    public void testGet() throws SQLException{
        String username = "user";
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        when(resultSet.getString("username")).thenReturn(username);
        when(resultSet.getString("role")).thenReturn("ADMIN");

        Optional<User> userOptional = mySQLUserDAO.get(username);
        assertTrue(userOptional.isPresent());
        User user = userOptional.get();
        assertEquals(username, user.getUsername());

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeQuery();
        verify(preparedStatement, times(1)).close();
        verify(preparedStatement, atLeast(1)).setString(anyInt(), eq(username));
    }

    @Test(expected = RepositoryException.class)
    public void testGetException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        mySQLUserDAO.get("");
    }

    @Test
    public void testGetUsersWithRole() throws SQLException{
        User.Role role = User.Role.ADMIN;
        when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);
        when(preparedStatement.executeQuery()).thenReturn(resultSet);
        when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(resultSet.getString("role")).thenReturn(role.toString());

        List<User> users = mySQLUserDAO.getUsersWithRole(role);
        assertEquals(2, users.size());
        for (User user : users) {
            assertEquals(role.name(), user.getRole());
        }

        verify(connection, times(1)).prepareStatement(anyString());
        verify(preparedStatement, times(1)).executeQuery();
        verify(preparedStatement, times(1)).close();
        verify(preparedStatement, atLeast(1)).setString(anyInt(), eq(role.name()));
    }

    @Test(expected = RepositoryException.class)
    public void testGetUsersWithRoleException() throws SQLException{
        when(connection.prepareStatement(anyString())).thenThrow(new SQLException());

        mySQLUserDAO.getUsersWithRole(User.Role.ADMIN);
    }

}
