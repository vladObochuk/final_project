package controller.command.manager;

import controller.command.Command;
import controller.command.exception.WrongCommandException;
import controller.command.impl.GetWelcomePageCommand;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * Provides way of receiving {@code Command} object from {@code String} object
 */
public abstract class CommandManager {
    private static Logger logger = Logger.getLogger(CommandManager.class);

    protected Map<String, Command> commandMap = new HashMap<>();

    {
        commandMap.put(null, new GetWelcomePageCommand());      // default command
    }

    /**
     * Get {@code Command} by {@code String} sign
     *
     * @param commandName {@code String}
     * @return {@code Command} object somehow associated with supplied String
     * @throws WrongCommandException if there is not {@code Command} associated with supplied {@code String}
     */
    public Command get(String commandName) {
        if (!commandMap.containsKey(commandName)) {
            logger.warn("There is no command with name " + commandName);
            throw new WrongCommandException("There is no command with name " + commandName);
        }
        return commandMap.get(commandName);
    }

}
