<%@ tag body-content="scriptless" %>
<%@ attribute name="items" required="true" rtexprvalue="true" type="java.util.Collection" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
    <c:when test="${empty param.page}">
        <fmt:formatNumber var="page" value="1" scope="page" type="number"/>
    </c:when>

    <c:otherwise>
        <fmt:formatNumber var="page" value="${param.page}" scope="page" type="number"/>
    </c:otherwise>
</c:choose>

<c:set var="begin" value="${(page - 1) * pageSize}"/>
<c:set var="end" value="${begin + pageSize - 1}"/>

<c:forEach items="${items}" var="item" varStatus="varStatus" begin="${begin}" end="${end}">
    <c:set var="item" value="${item}" scope="request"/>
    <c:set var="varStatus" value="${varStatus}" scope="request"/>
    <jsp:doBody var="formBody"/>
    ${formBody}
</c:forEach>
