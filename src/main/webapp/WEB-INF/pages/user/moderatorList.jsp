<%@ include file="../jspf/header.jspf" %>

<c:url value="/admin.do" var="deleteUserURL">
    <c:param name="action" value="deleteUser"/>
</c:url>

<c:choose>
    <c:when test="${empty moderators}">
        <fmt:message key="moderators.empty"/>
    </c:when>
    <c:otherwise>
        <table class="table table-sm table-striped w-auto">
            <thead class="table-dark">
            <tr>
                <th style="width:10%">#</th>
                <th style="width: 60%"><fmt:message key="username.label"/></th>
                <th style="width: 15%"><fmt:message key="user.edit"/></th>
                <th style="width: 15%"><fmt:message key="user.delete"/></th>
            </tr>
            </thead>
            <tbody>
            <pagination:forEach items="${moderators}">
                <c:url value="/admin.do" var="userEditURL">
                    <c:param name="action" value="editUser"/>
                    <c:param name="id" value="${item.id}"/>
                </c:url>
                <tr>
                    <th scope="row">${varStatus.index + 1}</th>
                    <td><c:out value="${item.username}"/></td>
                    <td><a href="${userEditURL}"><span class="fa fa-edit"></span></a></td>
                    <td>
                        <csrf:form action="${deleteUserURL}" method="post" htmlClass="no-padding">
                            <input type="hidden" name="userId" value="${item.id}">
                            <button type="submit" class="btn btn-sm">
                                <span class="fa fa-remove"></span>
                            </button>
                        </csrf:form>
                    </td>
                </tr>
            </pagination:forEach>
            </tbody>
        </table>

        <pagination:elem items="${moderators}"/>
    </c:otherwise>
</c:choose>


<a href="${pageContext.request.contextPath}/admin.do?action=registerModerator" class="btn">
    Add new
</a>

<%@ include file="../jspf/footer.jsp" %>
