package controller.command;

import controller.command.exception.WrongCommandException;
import controller.utils.HttpMethod;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractCommand implements Command {

    /**
     * Process user request
     *
     * @param request  not {@code null}
     * @param response not {@code null}
     * @param method   not {@code null} equals to method in request
     * @return not {@code null} {@code String}
     * @throws ServletException
     */
    @Override
    public String process(HttpServletRequest request, HttpServletResponse response, HttpMethod method) throws ServletException {
        switch (method) {
            case GET:
                return onGet(request, response);
            case POST:
                return onPost(request, response);
            default:
                throw new WrongCommandException("Wrong method for command " + method);
        }
    }

    /**
     * Procession of request with get method
     *
     * @param request  not {@code null}
     * @param response not {@code null}
     * @return path to jsp page
     * @throws ServletException
     */
    protected abstract String onGet(HttpServletRequest request, HttpServletResponse response) throws ServletException;

    /**
     * Procession of request with post method
     *
     * @param request  not {@code null}
     * @param response not {@code null}
     * @return href to some internet page
     * @throws ServletException
     */
    protected abstract String onPost(HttpServletRequest request, HttpServletResponse response) throws ServletException;
}
