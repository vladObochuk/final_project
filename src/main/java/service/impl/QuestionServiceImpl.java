package service.impl;

import exception.ApplicationException;
import model.database.dao.factory.DAOFactory;
import model.database.dao.interfaces.AnswerOptionDAO;
import model.database.dao.interfaces.QuestionDAO;
import model.database.dao.interfaces.UserDAO;
import model.entity.AnswerOption;
import model.entity.Question;
import model.entity.User;
import org.apache.log4j.Logger;
import service.QuestionService;

import java.util.*;

public class QuestionServiceImpl implements QuestionService {
    private static Logger logger = Logger.getLogger(QuestionServiceImpl.class);

    private final DAOFactory DAO_FACTORY;

    public QuestionServiceImpl(DAOFactory DAO_FACTORY) {
        this.DAO_FACTORY = DAO_FACTORY;
    }


    @Override
    public void addQuestion(Question question, AnswerOption rightAnswer, List<AnswerOption> answerOptions) {
        logger.trace("Adding question");
        QuestionDAO questionDAO = DAO_FACTORY.createQuestionDAO();
        AnswerOptionDAO answerOptionDAO = DAO_FACTORY.createAnswerOptionDAO();

        // first insert right answer -> set it id to question.rightAnswerId -> insert question
        // -> set questionId to rightAnswer -> merge rightAnswer
        answerOptionDAO.insert(rightAnswer);
        question.setRightAnswerId(rightAnswer.getId());
        questionDAO.insert(question);
        rightAnswer.setQuestionId(question.getId());
        answerOptionDAO.merge(rightAnswer);

        answerOptions.stream()
                .filter(answerOption -> answerOption.getText() != null && !answerOption.getText().trim().isEmpty())
                .peek(answerOption -> answerOption.setQuestionId(question.getId()))
                .forEach(answerOptionDAO::insert);
    }


    @Override
    public void confirmQuestion(Question question, AnswerOption rightAnswer, List<AnswerOption> answerOptions) {
        QuestionDAO questionDAO = DAO_FACTORY.createQuestionDAO();

        questionDAO.delete(question.getId());
        addQuestion(question, rightAnswer, answerOptions);
    }

    @Override
    public List<Question> getQuestionsOfUser(Integer userId) {
        QuestionDAO questionDAO = DAO_FACTORY.createQuestionDAO();
        return questionDAO.getUserQuestions(userId);
    }

    @Override
    public List<Question> getNotModeratedQuestions() {
        QuestionDAO questionDAO = DAO_FACTORY.createQuestionDAO();
        return questionDAO.getQuestionsNullApproved();
    }

    @Override
    public Question getQuestionById(Integer questionId) {
        QuestionDAO questionDAO = DAO_FACTORY.createQuestionDAO();
        Optional<Question> question = questionDAO.getById(questionId);
        return question.orElseThrow(() -> new ApplicationException("Trying get question with wrong id = " + questionId));
    }

    @Override
    public List<Question> getRandomQuestions(String lang, int amount, User user) {
        QuestionDAO questionDAO = DAO_FACTORY.createQuestionDAO();
        return questionDAO.getRandomQuestionsWith(true, lang, amount, user.getId());
    }

    @Override
    public List<AnswerOption> getQuestionAnswers(Integer questionId) {
        AnswerOptionDAO answerOptionDAO = DAO_FACTORY.createAnswerOptionDAO();
        return answerOptionDAO.getQuestionAnswers(questionId);
    }

    @Override
    public User getCreatorOfQuestion(Question question) {
        UserDAO userDAO = DAO_FACTORY.createUserDAO();
        return userDAO.getById(question.getUserId())
                .orElseThrow(() -> new ApplicationException("There are no user with id=" + question.getUserId()));
    }

    @Override
    public AnswerOption getQuestionAnswer(Question question) {
        AnswerOptionDAO answerOptionDAO = DAO_FACTORY.createAnswerOptionDAO();
        return answerOptionDAO.getById(question.getRightAnswerId()).orElseThrow(
                () -> new ApplicationException("There is no answer option with id=" + question.getRightAnswerId())
        );
    }

    @Override
    public Map<AnswerOption, Double> getCorrectnessProbability(List<AnswerOption> questionAnswers, Question question) {
        Map<AnswerOption, Double> correctnessProbability = new HashMap<>();
        double minimumProbability = 100 / (questionAnswers.size() * 2);
        Random random = new Random();
        for (AnswerOption questionAnswer : questionAnswers) {
            double maxRandomValue = minimumProbability;
            if (questionAnswer.getId().equals(question.getRightAnswerId())) {
                maxRandomValue = maxRandomValue + minimumProbability / 2;
            }
            double probability = minimumProbability + (random.nextDouble() * maxRandomValue);
            correctnessProbability.put(questionAnswer, probability);
        }
        return correctnessProbability;
    }

    @Override
    public List<AnswerOption> getCorrectAndRandomOption(List<AnswerOption> questionAnswers, Question question) {
        List<AnswerOption> answerOptions = new ArrayList<>();
        AnswerOption correctAnswer = questionAnswers.stream()
                .filter(answerOption -> answerOption.getId().equals(question.getRightAnswerId()))
                .findFirst().get();
        answerOptions.add(correctAnswer);
        AnswerOption random = questionAnswers.stream()
                .filter(answerOption -> !answerOption.getId().equals(question.getRightAnswerId()))
                .findAny().get();
        answerOptions.add(random);
        return answerOptions;
    }
}