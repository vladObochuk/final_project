package controller.utils;

/**
 * Provides enumeration of Http methods
 */
public enum HttpMethod {
    GET, POST, DELETE, PUT, PATCH
}
