package controller.command.impl.admin;

import constants.Alert;
import constants.AlertMessages;
import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import model.database.dao.ErrorCodes;
import model.database.dao.factory.DAOFactory;
import model.database.exception.RepositoryException;
import model.entity.User;
import org.apache.catalina.CredentialHandler;
import org.apache.catalina.Globals;
import org.apache.log4j.Logger;
import service.UserService;
import service.factory.ServiceFactory;
import validation.Validator;
import validation.exception.ValidationException;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AddModeratorCommand extends AbstractCommand {
    private static Logger logger = Logger.getLogger(AddModeratorCommand.class);
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) {
        return PagePath.REGISTER;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("username");
        logger.trace("Register new moderator with username " + username);
        String password = request.getParameter("password");
        User.Role role = User.Role.MODERATOR;

        User user = new User(username, password, role);

        HttpSession session = request.getSession();
        ServletContext context = request.getServletContext();
        try {
            Validator.validate(user);

            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            UserService userService = serviceFactory.createUserService();
            CredentialHandler handler = (CredentialHandler) context.getAttribute(Globals.CREDENTIAL_HANDLER);
            userService.registerUser(user, handler);
            session.setAttribute(Alert.SUCCESS, AlertMessages.ADD_MODERATOR_SUCCESS);
        } catch (RepositoryException e){
            session.setAttribute(Alert.ERROR, getRepositoryErrorMessage(e, request));
        } catch (ValidationException e){
            session.setAttribute(Alert.ERROR, e.getMessage());
            return context.getContextPath() + RedirectLink.ADMIN_ADD_MODERATOR;
        }

        return context.getContextPath() + RedirectLink.ADMIN_LIST_MODERATORS;
    }

    private String getRepositoryErrorMessage(RepositoryException e, HttpServletRequest request){
        DAOFactory factory = (DAOFactory) request.getAttribute("model.database.dao.factory.DAOFactory");
        ErrorCodes errorCodes = factory.getErrorCodes();
        if (e.getErrorCode() == errorCodes.DUP_UNIQUE()){
            return AlertMessages.DUPLICATE_LOGIN;
        } else {
            return AlertMessages.REGISTER_ERROR;
        }
    }
}
