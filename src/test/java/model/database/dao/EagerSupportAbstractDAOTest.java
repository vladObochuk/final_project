package model.database.dao;

import model.database.exception.RepositoryException;
import model.entity.Answer;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class EagerSupportAbstractDAOTest {
    @Mock
    Connection connection;

    @Mock
    PreparedStatement preparedStatement;

    @Mock
    ResultSet resultSet;

    @Mock
    ResultSet generatedKeys;

    @Mock(answer = Answers.CALLS_REAL_METHODS)
    EagerSupportAbstractDAO eagerSupportAbstractDAO;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void setUp(){
        eagerSupportAbstractDAO.connection = connection;
    }

    @Test
    public void testInsert() throws SQLException{
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        when(preparedStatement.getGeneratedKeys()).thenReturn(generatedKeys);

        eagerSupportAbstractDAO.insert(new Answer());

        verify(connection, times(2)).prepareStatement(anyString(), anyInt());
        verify(preparedStatement, times(1)).executeBatch();
        verify(preparedStatement, times(2)).getGeneratedKeys();
        verify(preparedStatement, times(2)).close();
    }

    @Test(expected = RepositoryException.class)
    public void testInsertException() throws SQLException{
        when(connection.prepareStatement(anyString(), anyInt())).thenReturn(preparedStatement);
        when(preparedStatement.getGeneratedKeys()).thenReturn(generatedKeys);
        when(preparedStatement.executeBatch()).thenThrow(new SQLException());

        eagerSupportAbstractDAO.insert(new Answer());
    }
}
