package constants;

/**
 * Keys to ResourceBundle messages describing success or failure to do some action
 */
public interface AlertMessages {
    String PASSWORD_CHANGE_SUCCESS = "changeData.password.success";
    String WRONG_PASSWORD = "changeData.password.error.wrong";
    String PASSWORDS_DONT_MATCH = "changeData.password.error.match";

    String DELETE_USER_SUCCESS = "user.delete.success";

    String ADD_QUESTION_SUCCESS = "question.add.success";

    String MODERATE_QUESTION_SUCCESS = "question.moderate.success";

    String DUPLICATE_LOGIN = "username.duplicate";
    String REGISTER_ERROR = "user.register.error";

    String ADD_MODERATOR_SUCCESS = "user.register.moderator.success";

    String CANT_FIND_USER = "user.not.exist";
    String USER_EDIT_SUCCESS = "user.edit.success";

    String WRONG_GET_PARAMETER = "get.param.error";
    String WRONG_POST_PARAMETER = "post.param.error";

    String GAME_CONFIG_SUCCESS = "game.config.success";
}
