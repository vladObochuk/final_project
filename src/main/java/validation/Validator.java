package validation;

import exception.ApplicationException;
import org.apache.log4j.Logger;
import validation.annotation.*;
import validation.exception.ValidationException;

import java.lang.reflect.Field;
import java.util.Collection;

public class Validator {
    private static Logger logger = Logger.getLogger(Validator.class);

    /**
     * Checks if object is valid. Signs of correctness provided by annotations
     *
     * @param o not {@code null}
     */
    public static void validate(Object o) {
        Class objectClass = o.getClass();
        if (objectClass.isAssignableFrom(Collection.class)) {
            logger.trace("Validate collection");
            Collection items = (Collection) o;
            for (Object item : items) {
                validate(item);
            }
        } else {
            logger.trace("Simple object validation");
            validateObject(o);
        }

    }

    /**
     * Validate simple object
     *
     * @param o not {@code null}
     */
    private static void validateObject(Object o) {
        Class objectClass = o.getClass();
        Field[] fields = objectClass.getDeclaredFields();
        for (Field field : fields) {
            logger.trace("Validating field " + field.getName() + "of class " + objectClass.getSimpleName());
            processValidation(o, field);
        }
    }

    /**
     * Validate object field
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processValidation(Object o, Field field) {
        processNotNullValidation(o, field);
        processMinValidation(o, field);
        processMaxValidation(o, field);
        processMinLengthValidation(o, field);
        processMaxLengthValidation(o, field);
        processRegexValidation(o, field);
        logger.debug("Object valid");
    }

    /**
     * Process field validation by NotNull constraint
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processNotNullValidation(Object o, Field field) {
        if (field.isAnnotationPresent(NotNull.class)) {
            Object property = getField(o, field);
            if (property == null) {
                logger.trace("Not null validation fail");
                NotNull notNull = field.getAnnotation(NotNull.class);
                throw new ValidationException(notNull.value());
            }
        }
    }

    /**
     * Process field validation by Min constraint
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processMinValidation(Object o, Field field) {
        if (field.isAnnotationPresent(Min.class)) {
            double value = getNumberField(o, field);
            Min minAnnotation = field.getAnnotation(Min.class);
            if (value < minAnnotation.value()) {
                logger.trace("Min validation failed");
                throw new ValidationException(minAnnotation.message());
            }
        }
    }

    /**
     * Process field validation by Max constraint
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processMaxValidation(Object o, Field field) {
        if (field.isAnnotationPresent(Max.class)) {
            double value = getNumberField(o, field);
            Max maxAnnotation = field.getAnnotation(Max.class);
            if (value > maxAnnotation.value()) {
                logger.trace("Max validation failed");
                throw new ValidationException(maxAnnotation.message());
            }
        }
    }

    /**
     * Process field validation by MinLength constraint
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processMinLengthValidation(Object o, Field field) {
        if (field.isAnnotationPresent(MinLength.class)) {
            String value = getStringField(o, field);
            MinLength minLengthAnnotation = field.getAnnotation(MinLength.class);
            if (value.length() < minLengthAnnotation.value()) {
                logger.trace("Min length validation failed");
                throw new ValidationException(minLengthAnnotation.message());
            }
        }
    }

    /**
     * Process field validation by MaxLength constraint
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processMaxLengthValidation(Object o, Field field) {
        if (field.isAnnotationPresent(MaxLength.class)) {
            String value = getStringField(o, field);
            MaxLength maxLengthAnnotation = field.getAnnotation(MaxLength.class);
            if (value.length() > maxLengthAnnotation.value()) {
                logger.trace("Max length validation failed");
                throw new ValidationException(maxLengthAnnotation.message());
            }
        }
    }

    /**
     * Process field validation by Regex constraint
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     */
    private static void processRegexValidation(Object o, Field field) {
        if (field.isAnnotationPresent(Regex.class)) {
            String value = getStringField(o, field);
            Regex regexAnnotation = field.getAnnotation(Regex.class);
            if (!value.matches(regexAnnotation.value())) {
                logger.trace("Regex validation failed");
                throw new ValidationException(regexAnnotation.message());
            }
        }
    }

    /**
     * Retrieve field value from object
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     * @return value of field as {@code Object}
     */
    private static Object getField(Object o, Field field) {
        try {
            field.setAccessible(true);
            return field.get(o);
        } catch (IllegalAccessException e) {
            throw new ApplicationException(e);
        }
    }

    /**
     * Retrieve number field value
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     * @return value of field as {@code double}
     */
    private static double getNumberField(Object o, Field field) {
        try {
            Object property = getField(o, field);
            Number number = (Number) property;
            return number.doubleValue();
        } catch (ClassCastException e) {
            logger.fatal("Annotation attached to wrong data type, required number");
            throw new ApplicationException(e);
        }
    }

    /**
     * Retrieve string field value
     *
     * @param o     o not {@code null}
     * @param field o not {@code null} and belongs to o
     * @return value of field as {@code String}
     */
    private static String getStringField(Object o, Field field) {
        Object property = getField(o, field);
        return property.toString();
    }
}
