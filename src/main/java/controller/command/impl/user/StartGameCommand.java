package controller.command.impl.user;

import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import model.entity.Config;
import model.entity.Game;
import model.entity.Question;
import model.entity.User;
import service.GameService;
import service.QuestionService;
import service.TimeService;
import service.factory.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

public class StartGameCommand extends AbstractCommand {
    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        GameService gameService = serviceFactory.createGameService();

        User user = CommandUtils.getUser(request);
        Config lastConfig = gameService.getLastConfigurationOrDefault(user);

        request.setAttribute("config", lastConfig);
        return PagePath.START_GAME;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        User user = CommandUtils.getUser(request);
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        GameService gameService = serviceFactory.createGameService();
        Config config = gameService.getLastConfigurationOrDefault(user);

        Game game = Game.newBuilder()
                .userId(user.getId())
                .configurationId(config.getId())
                .build();
        gameService.addGame(game);

        String language = (String) request.getSession().getAttribute("language");
        QuestionService questionService = serviceFactory.createQuestionService();
        List<Question> chosenQuestions = questionService.getRandomQuestions(language, config.getQuestionsAmount(), user);

        HttpSession session = request.getSession();

        session.setAttribute("game", game);
        session.setAttribute("config", config);
        session.setAttribute("questions", chosenQuestions);
        session.setAttribute("playerPoints", 0);
        session.setAttribute("opponentPoints", 0);

        session.removeAttribute("questionNumber");
        TimeService timeService = serviceFactory.createTimeService();
        timeService.removeSessionAttributes(session);

        return request.getContextPath() + RedirectLink.GAME;
    }
}
