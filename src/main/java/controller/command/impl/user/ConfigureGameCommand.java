package controller.command.impl.user;

import constants.Alert;
import constants.AlertMessages;
import constants.PagePath;
import constants.RedirectLink;
import controller.command.AbstractCommand;
import controller.utils.CommandUtils;
import exception.NullRequestParameterException;
import model.entity.Config;
import model.entity.HintType;
import model.entity.User;
import service.GameService;
import service.factory.ServiceFactory;
import validation.Validator;
import validation.exception.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ConfigureGameCommand extends AbstractCommand {


    @Override
    protected String onGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
        GameService gameService = serviceFactory.createGameService();
        List<HintType> hintTypes = gameService.getAllAvailableHints();

        User user = CommandUtils.getUser(request);
        Config lastConfig = gameService.getLastConfigurationOrDefault(user);

        request.setAttribute("lastConfiguration", lastConfig);
        request.setAttribute("hints", hintTypes);
        request.setAttribute("timeOptions", GameService.TIME_OPTIONS);
        request.setAttribute("questionAmountOptions", GameService.QUESTION_AMOUNT_OPTIONS);

        return PagePath.CONFIG;
    }

    @Override
    protected String onPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        HttpSession session = request.getSession();
        try {
            Config config = retrieveConfig(request);
            Validator.validate(config);

            ServiceFactory serviceFactory = CommandUtils.getServiceFactory(request);
            GameService gameService = serviceFactory.createGameService();
            gameService.addConfiguration(config);
            session.setAttribute(Alert.SUCCESS, AlertMessages.GAME_CONFIG_SUCCESS);
        } catch (NumberFormatException | NullRequestParameterException e){
            session.setAttribute(Alert.ERROR, AlertMessages.WRONG_POST_PARAMETER);
        } catch (ValidationException e){
            session.setAttribute(Alert.ERROR, e.getMessage());
        }

        return request.getContextPath() + RedirectLink.CONFIG_PAGE;
    }

    private Config retrieveConfig(HttpServletRequest request) {
        Integer timeToAnswer = CommandUtils.retrieveIntegerValue(request, "time");
        Integer questionsAmount = CommandUtils.retrieveIntegerValue(request, "questionAmount");
        String[] hintTypes = request.getParameterValues("hintType[]");

        User user = CommandUtils.getUser(request);

        Set<HintType> hintTypeSet = null;
        if (hintTypes != null) {
            hintTypeSet = Arrays.stream(hintTypes)
                    .map(Integer::parseInt)
                    .map(HintType::new)
                    .collect(Collectors.toSet());
        }

        return Config.newBuilder()
                .questionsAmount(questionsAmount)
                .timeToReply(timeToAnswer)
                .userId(user.getId())
                .hintTypes(hintTypeSet)
                .build();
    }
}
