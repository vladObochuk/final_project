package controller.command.manager.impl.admin;

/**
 * Provides names of actions proper for administrator
 */
public interface AdminAction {
    String ADD_MODERATOR = "registerModerator";
    String VIEW_ALL_MODERATORS = "listModerators";
    String EDIT_USER = "editUser";
    String DELETE_USER = "deleteUser";
}
