package validation.exception;

import exception.ApplicationException;

public class ValidationException extends ApplicationException {

    public ValidationException(String message) {
        super(message);
    }
}
