package service;

import model.entity.Config;

import javax.servlet.http.HttpSession;

public interface TimeService {

    /**
     * Set time attributes for session
     *
     * @param session not @{@code null}
     * @param config  not {@code null}
     */
    void setupTimeForAnswer(HttpSession session, Config config);

    /**
     * Change time attributes, so finish time is move forward
     *
     * @param session not {@code null}
     */
    void addTime(HttpSession session);

    /**
     * Remove created by this service attributes
     *
     * @param session not {@code null}
     */
    public void removeSessionAttributes(HttpSession session);

    /**
     * Check if specified end time in attribute is after now
     *
     * @param session not {@code null}
     * @return {@code true} if finish time is after now and {@code false} otherwise
     */
    public boolean isTimeToAnswerPassed(HttpSession session);
}
