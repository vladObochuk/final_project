package model.database.exception;

import exception.ApplicationException;

public class UnsupportedDataSourceException extends ApplicationException {
    public UnsupportedDataSourceException(String message) {
        super(message);
    }

    public UnsupportedDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }
}
