package model.database.datasource;

import model.database.exception.UnsupportedDataSourceException;
import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Provides method for creation dataSource by it`s type
 */
public class DataSourceFactory {
    private static Logger logger = Logger.getLogger(DataSourceFactory.class);

    /**
     * Get dataSource by it's type
     *
     * @param type not {@code null}
     * @return not {@code null} dataSource object
     * @throws UnsupportedDataSourceException if dataSource with specified type is not found
     */
    public static DataSource getDataSource(DataSourceType type) {
        switch (type) {
            case MYSQL:
                return getMySQLDataSource();
            default:
                logger.fatal("Unknown type of DataSource requested " + type);
                throw new UnsupportedDataSourceException("Can`t find dataSource for " + type + " type");
        }
    }

    private static DataSource getMySQLDataSource() {
        return retrieveDataSourceFromContext("java:comp/env/jdbc/final_project");
    }

    /**
     * Retrieve dataSource from context by given name
     *
     * @param name name of dataSource in context
     * @return {@code not null} dataSource object
     * @throws UnsupportedDataSourceException if dataSource with specified name doesn't exist
     */
    private static DataSource retrieveDataSourceFromContext(String name) {
        try {
            Context context = new InitialContext();
            return (DataSource) context.lookup(name);
        } catch (NamingException e) {
            logger.fatal("Cannot get dataSource from context", e);
            throw new UnsupportedDataSourceException("Can`t retrieve dataSource for name " + name, e);
        }
    }

}
